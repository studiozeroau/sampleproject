﻿Shader "Sample/Unlit" 
{
	Properties 
	{
		_MainTex ("Base (RGB)", 2D) = "white" {}
	}

	SubShader 
	{
		Tags {"Queue" = "Geometry" "RenderType" = "Opaque"}

		Cull Off

		Pass 
		{
			Tags {"LightMode" = "ForwardBase"}

			CGPROGRAM

				#pragma vertex vert
				#pragma fragment frag
				#pragma multi_compile_fwdbase
				#pragma fragmentoption ARB_fog_exp2
				#pragma fragmentoption ARB_precision_hint_fastest
				
				#include "UnityCG.cginc"
				#include "AutoLight.cginc"
				
				sampler2D _MainTex;
				float4 _MainTex_ST;
				float4 _LightColor0;
				float _worldLightToggle;
				
				struct v2f
				{
					float4	pos			: SV_POSITION;
					float2	uv			: TEXCOORD0;
					LIGHTING_COORDS(1,2)
				};

				v2f vert (appdata_tan v)
				{
					v2f o;
					
					o.pos = UnityObjectToClipPos( v.vertex);
					o.uv = TRANSFORM_TEX (v.texcoord, _MainTex).xy;

					TRANSFER_VERTEX_TO_FRAGMENT(o);

					return o;
				}


				fixed4 frag(v2f i) : COLOR
				{
					fixed4 result = tex2D(_MainTex, i.uv);
					
					result *= _LightColor0;

					return result;
				}

			ENDCG

		}
		
	}

	FallBack "VertexLit"
}