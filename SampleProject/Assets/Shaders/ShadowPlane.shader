﻿Shader "Sample/ShadowPlane"
{
    Properties
    {
        _Alpha("Alpha", Range(0.0, 1.0)) = 0.5
    }

    SubShader
    {
        Tags{  "Queue" = "Geometry" "RenderType" = "Transparent" }

        Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
            Tags{ "LightMode" = "ForwardBase" }

            CGPROGRAM

            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile_fwdbase
            
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"

            uniform float _Alpha;

            struct v2f
            {
                float4 pos          : SV_POSITION;
                float3 uv           : TEXCOORD0;
                
                LIGHTING_COORDS(1,2)
            };

            v2f vert(appdata_base v)
            {
                v2f o;

                o.pos = UnityObjectToClipPos(v.vertex);
                o.uv = mul (unity_ObjectToWorld, v.vertex);

                TRANSFER_VERTEX_TO_FRAGMENT(o);

                return o;
            }


            fixed4 frag(v2f i) : COLOR
            {
                float attenuation = LIGHT_ATTENUATION(i);
                
                return lerp(float4(0, 0, 0, _Alpha), float4(0, 0, 0, 0), attenuation);
            }

        ENDCG

        }

    }
    
    FallBack "VertexLit"
}