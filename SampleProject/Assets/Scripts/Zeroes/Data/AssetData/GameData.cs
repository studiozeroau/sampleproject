using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GameData : Singleton<GameData>
{
    private Dictionary<int, IdentifiableAsset> _assetDatabase = new Dictionary<int, IdentifiableAsset>();
    private AssetCollectionNode _root;
    
    public void Initialize(ProjectConfiguration projectConfiguration)
    {
        _root = projectConfiguration.RootAssetCollection;
        
        List<IdentifiableAsset> assets = _root.GetAssetsRecursively();
        foreach (IdentifiableAsset asset in assets)
        {
            if(_assetDatabase.ContainsKey(asset.AssetID))
                Debug.LogWarning($"Asset Database already contains ID {asset.AssetID} key for {asset.name} ({asset.GetType()} for {_assetDatabase[asset.AssetID].name} ({_assetDatabase[asset.AssetID].GetType()})). Please check the assets!");
            else
                _assetDatabase.Add(asset.AssetID, asset);
        }
    }

    public static IdentifiableAsset GetAsset(int id)
    {
        if (Instance._assetDatabase.TryGetValue(id, out IdentifiableAsset asset))
            return asset;
        return null;
    }

    public IdentifiableAsset[] AllAssets()
    {
        return _assetDatabase.Values.ToArray();
    }

}