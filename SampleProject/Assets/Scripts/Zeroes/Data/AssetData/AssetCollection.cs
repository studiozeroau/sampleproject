using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

public abstract class AssetCollectionBase : IdentifiableAsset
{
    public abstract List<IdentifiableAsset> GetAssetsRecursively();
#if UNITY_EDITOR
    public abstract void PopulateAll();
    public abstract void Sort();
#endif
    protected abstract Type[] SearchConstraints { get; }
}

public abstract class AssetCollection<T> : AssetCollectionBase where T: IdentifiableAsset
{
    [SerializeField, InspectorExpandable] private T[] _assets = null;
    private readonly Dictionary<int, T> _assetDatabase = new Dictionary<int, T>();
    protected override Type[] SearchConstraints => null;
    
    private void Awake()
    {
        if (!Application.isPlaying)
            return;

        foreach (T identifiableAsset in _assets)
        {
            if (_assetDatabase.ContainsKey(identifiableAsset.AssetID))
                Debug.LogWarning($"Duplicate Assets in Collection for {identifiableAsset.name} ({identifiableAsset.AssetID})");
            else
                _assetDatabase.Add(identifiableAsset.AssetID, identifiableAsset);   
        }
    }

    // Get an asset based on ID
    public T GetAsset(int id)
    {
        if (_assetDatabase.ContainsKey(id))
            return _assetDatabase[id];
        
        return null;
    }

    // Get an asset of a specific type
    public U GetAsset<U>() where U : IdentifiableAsset
    {
        foreach (T identifiableAsset in _assets)
        {
            if (identifiableAsset is U asset)
                return asset;
        }
        return null;
    }
    
    public T GetRandomAsset()
    {
        return _assets.RandomElement();
    }

    // Get all assets
    public List<T> GetAllAssets()
    {
        List<T> assets = new List<T>();
        foreach(T asset in _assets)
            assets.Add(asset);
        return assets;
    }
    
    // Gets all assets of the specified type
    public List<U> GetAllAssets<U>() where U : IdentifiableAsset
    {
        List<U> assets = new List<U>();
        foreach (T identifiableAsset in _assets)
        {
            if(identifiableAsset is U asset)
                assets.Add(asset);
        }
        return assets;
    }
    
    public override List<IdentifiableAsset> GetAssetsRecursively()
    {   
        List<IdentifiableAsset> assets = new List<IdentifiableAsset>();
        foreach (T asset in _assets)
        {
            if (asset is AssetCollectionBase collection)
                assets.AddRange(collection.GetAssetsRecursively());
            else
                assets.Add(asset);
        }
        return assets;
    }
    
#if UNITY_EDITOR

    public override void PopulateAll()
    {
        List<T> assets = new List<T>();
        
        if(SearchConstraints != null)
        {
            foreach (Type type in SearchConstraints)
                PopulateWith(type);
        }
        else
        {
            PopulateWith(typeof(T));
        }

        void PopulateWith(Type type)
        {
            string[] guids = AssetDatabase.FindAssets($"t: {type}");
            for(int i = 0; i < guids.Length; i++)
            {
                string assetPath = AssetDatabase.GUIDToAssetPath(guids[i]);
                T assetObject = AssetDatabase.LoadAssetAtPath<T>(assetPath);
                if(assetObject != this)
                    assets.Add(assetObject);
                if(assetObject is AssetCollectionBase collection)
                    collection.PopulateAll();
            } 
        }

        _assets = assets.ToArray();
        EditorUtility.SetDirty(this);
    }

    public override void Sort()
    {
        List<T> assets = _assets.ToList();
        assets.Sort();
        _assets = assets.ToArray();
    }

#endif
}
