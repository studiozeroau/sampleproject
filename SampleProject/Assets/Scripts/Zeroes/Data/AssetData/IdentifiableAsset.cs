using System;
using UnityEngine;
using System.Collections;
using System.ComponentModel;
using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
#if UNITY_EDITOR
using UnityEditor;
#endif

[JsonConverter(typeof(IdentifiableAssetJsonConverter))]
public class IdentifiableAsset : ScriptableObject
{

    public const string kAssetID = "AssetID";
    
    [SerializeField, EditorReadOnly] private int _assetID = 0;
    public int AssetID => _assetID;
    
    protected virtual void OnValidate() { }

#if UNITY_EDITOR

    protected virtual void OnEnable()
    {
        AssignID();
    }
    
    public void AssignID()
    {            
        int id = AssetDatabase.AssetPathToGUID(AssetDatabase.GetAssetPath(this)).GetStableHashCode();
        if(_assetID != id)
        {
            _assetID = id;
            Debug.Log($"Assigned ID of {_assetID} for {this.name}");
            EditorUtility.SetDirty(this);
        }
    }
    
#endif
    
    public static explicit operator int(IdentifiableAsset asset) => asset._assetID;

}

// Custom JSON Serializer that looks up the appropriate asset through the game data class
public class IdentifiableAssetJsonConverter : JsonConverter<IdentifiableAsset>
{
    public override void WriteJson(JsonWriter writer, IdentifiableAsset value, JsonSerializer serializer)
    {
        JObject o = new JObject();
        o.AddFirst(new JProperty(IdentifiableAsset.kAssetID, value.AssetID));
        o.WriteTo(writer);
    }

    public override IdentifiableAsset ReadJson(JsonReader reader, Type objectType, IdentifiableAsset existingValue, bool hasExistingValue, JsonSerializer serializer)
    {
        JObject item = JObject.Load(reader);
        if (item[IdentifiableAsset.kAssetID] != null)
            return GameData.GetAsset(item[IdentifiableAsset.kAssetID].Value<int>());
        Debug.LogWarning($"Could not resolve Asset with ID {item[IdentifiableAsset.kAssetID]}");
        return null;
    }
}

public static class StringUtilities
{
    public static int GetStableHashCode(this string str)
    {
        unchecked
        {
            var hash1 = 5381;
            var hash2 = hash1;

            for (var i = 0; i < str.Length && str[i] != '\0'; i += 2)
            {
                hash1 = ((hash1 << 5) + hash1) ^ str[i];
                if (i == str.Length - 1 || str[i + 1] == '\0')
                    break;
                hash2 = ((hash2 << 5) + hash2) ^ str[i + 1];
            }

            return hash1 + (hash2 * 1566083941);
        }
    }
}