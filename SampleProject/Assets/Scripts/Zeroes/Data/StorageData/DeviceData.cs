﻿using System;

namespace Zeroes
{
    [Serializable]
    public class DeviceData : DataStore
	{
		// What is the language the App is currently set to?
        public string Language = string.Empty;

		// Does the Application need to use a Dyslexic Font variant
        public bool UseDyslexicFont = false;

        // Does the Application need to use text backgrounds?
        public bool UseTextBackgrounds = false;

        // Is Music muted?
        public bool MusicMuted = false;

        // System independent music volume setting
        public float MusicVolume = 1f;

        // Is SFX muted?
        public bool SFXMuted = false;

        // System independent SFX volume setting
        public float SFXVolume = 1f;


        #region GRAPHICS SETTINGS

        // Shadow settings
        public bool Shadows = true;

        // Current anti aliasing setting
        public bool AntiAliasing = true;

        #endregion

    }
}
