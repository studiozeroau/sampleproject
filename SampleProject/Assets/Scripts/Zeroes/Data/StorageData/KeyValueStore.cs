﻿using System;
using System.Collections.Generic;

using UnityEngine;

[Serializable]
public class KeyValueStore
{

	// Store data as a key/value dictionary
	public Dictionary<string, object> Data = new Dictionary<string, object>();
    
	#region GENERICS

	public void SetValue<T>(string key, T value)
	{
		if (Data.ContainsKey(key))
		{
			Data[key] = value;
		}
		else
		{
			Data.Add(key, value);
		}
	}

	public T GetValue<T>(string key)
	{
		return GetValue<T>(key, default(T));
	}

	public T GetValue<T>(string key, T defaultValue)
	{
		if (Data.ContainsKey(key))
		{
			object value = Data[key];
			if (value.GetType() == typeof(T))
			{
				return (T)value;
			}
			else
			{
				try
				{
					return (T)Convert.ChangeType(value, typeof(T));
				}
				catch
				{
					Debug.LogError($"Type mismatch found for key: {key}. Serialised type was {value.GetType()} when expecting {defaultValue.GetType()}");
				}
			}
		}
		return defaultValue;
	}


	#endregion
    
	
}
