﻿using Newtonsoft.Json;
using System;

namespace Zeroes
{
    [Serializable]
    public class DataStore
    {
        // Store filepath for autonomous writing
        [JsonIgnore]
        public string Filepath { private set; get; }
        
        public void SetFilepath(string filepath)
        {
            Filepath = filepath;
        }

        public virtual void Save()
        {
            DataWriter.Write(this, Filepath);
        }

    }
}