﻿using System;

using UnityEngine;

//Generic User Data class to store shared information between apps

namespace Zeroes
{
    [Serializable]
    public class UserData : DataStore
    {
		// Store session-only variables here
        [NonSerialized]
		public KeyValueStore SessionData = new KeyValueStore();
	}
}