﻿using System.IO;

using Newtonsoft.Json;

using UnityEngine;

namespace Zeroes
{
    public class DataWriter
    {

        public static string CreatePath(string filename)
        {
            return CreatePath(filename, Application.persistentDataPath);
        }

        public static string CreatePath(string filename, string path)
        {
            return $"{path}{Path.DirectorySeparatorChar}{filename}";
        }

        // Writes an object to JSON and saves it to the specified path
        public static void Write<T>(T data, string path)
        {
            string serializedData = JsonConvert.SerializeObject(data);

#if DEBUGGING
            Debug.Log($"WRITE: {serializedData}");
#endif

            Delete(path);

            File.WriteAllText(path, serializedData);
        }
        
        // Reads and returns deserialized JSON from a path and returns its object
        public static T Read<T>(string path, T defaultData)
        {
            if(File.Exists(path))
            {
                string serializedData = File.ReadAllText(path);

#if DEBUGGING
                Debug.Log($"READ: {serializedData}");
#endif

                try
                {
                    var settings = new JsonSerializerSettings
                    {
                        ObjectCreationHandling = ObjectCreationHandling.Replace
                    };

                    Zebug.Log("Returning deserialized data".Colour(Color.green));
                    T data = JsonConvert.DeserializeObject<T>(serializedData, settings);
                    return data;
                }
                catch
                {
                    Zebug.Log("Returning default data".Colour(Color.red));
                    return defaultData;
                }
            }

            Zebug.Log("Returning default data".Colour(Color.red));
            return defaultData;
        }

        // Deletes the file
        public static bool Delete(string path)
        {
            if(File.Exists(path))
            {
                File.Delete(path);
                return true;
            }
            return false;
        }
    }
}
