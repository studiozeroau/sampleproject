﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using Zeroes.Audio;
using Zeroes.Touch;
using Zeroes.Localisation;

public enum TimeType
{
    Scaled,
    Unscaled,
    FixedScaled,
    FixedUnscaled
}

namespace Zeroes.Managers
{

	public class GameManager : MonoSingleton<GameManager>
	{
		// Input management
		public TouchManager TouchManager { get; private set; }

        // Audio Management
        public AudioManager AudioManager { private set; get; }
        
        // Scene management
        public SceneLoader SceneLoader { get; private set; }

		//Initialised in a superclass.
		public ZScreen Screen { get; protected set; }

		// Localisation management
		public LocalisationManager LocalisationManager { get; protected set; }

		// Accessibility management
		public AccessibilityManager AccessibilityManager { get; protected set; }

		// Device data is the base settings a Zeroes game supports
		public DeviceData DeviceData { get; protected set; }

        // Standardised debug menu
		public DebugMenu DebugMenu { get; protected set; }

		// Pushed in from the GameManager
		// Recommended to put any persistent and non closable scenes here. Loading, settings, etc.
		public List<Type> RequiredScenesOnLaunch { get; protected set; }

        // Store the project configuration in the GameManager for future access
        public ProjectConfiguration ProjectConfiguration { private set; get; }

        public override void Awake()
        {
	        base.Awake();
	        
#if UNITY_IPHONE
            // Environment setting required for iPhone saving
            System.Environment.SetEnvironmentVariable("MONO_REFLECTION_SERIALIZER", "yes");
#endif
	        
            RequiredScenesOnLaunch = new List<Type>();

            // Initialise all the managers
            TouchManager = new TouchManager();
            SceneLoader = new SceneLoader();
			LocalisationManager = new LocalisationManager();
			AccessibilityManager = new AccessibilityManager();
			
#if DEBUGGING

            // Add an FPS counter for debugging
            gameObject.AddComponent<FPSDrawer>();

            AddDebugMenu();
#endif

        }

        public override void Start()
        {
	        base.Start();
	        
        }
        
        public virtual void Update()
        {
            TouchManager.Update();
            SceneLoader.Update();

#if DEBUGGING

			// Toggles for the debug menu
			if (DebugMenu.enabled == false)
			{
				if (Input.touches.Length == 4 || Input.GetKeyDown("escape"))
				{
					DebugMenu.enabled = true;
				}
			}
			else
			{
				if (Input.GetKeyDown("escape"))
				{
					DebugMenu.enabled = false;
				}
			}
#endif

		}

#if DEBUGGING
        // I don't really like this
        // Maybe it should actually just be a regular component?
        public virtual void OnGUI()
        {
            TouchManager.OnGUI();
        }
#endif

        public virtual void Configure(ProjectConfiguration configuration)
        {
            ProjectConfiguration = configuration;

            InitialiseData();
            
            // Initialize Audio Manager
            AudioManager = gameObject.AddComponent<AudioManager>();
            AudioManager.Configure(configuration.AudioConfiguration);
            
            // Initialize ZScreen
            Screen = new ZScreen(configuration.ReferenceResolutionWidth);
            
            // Begin async initialisation
            Initialise();
        }

        public virtual async void Initialise()
        {
	        
        }

        public virtual void InitialiseData()
        {
	        
        }

        // Add generic debug menu
        protected virtual void AddDebugMenu()
        {
            DebugMenu = gameObject.AddComponent<DebugMenu>();
            DebugMenu.enabled = false;
        }

#region GAME PAUSE/RESUME HANDLING

        //Actions for the paused states of the game
        public Action AppOnPause;
        public Action AppOnResume;

        public virtual void PauseGame()
        {
            Time.timeScale = 0;

            if (AppOnPause != null)
            {
                AppOnPause();
            }
        }
        public virtual void ResumeGame()
        {
            Time.timeScale = 1;

            if (AppOnResume != null)
            {
                AppOnResume();
            }
        }

        #endregion


        public Action<bool> OnApplicationChangedFocus;

        public void OnApplicationFocus(bool hasFocus)
        {
            if (!hasFocus)
            {
                TouchManager.CancelAll();
            }

            OnApplicationChangedFocus?.Invoke(hasFocus);
        }

    }

}
