﻿using System;

using UnityEngine;

namespace Zeroes.Managers
{
	public class AccessibilityManager
	{
        // TODO. Universal text scale
        
        public Font DyslexicFont => GameManager.Instance.ProjectConfiguration.DyslexicFont;

		// Callbacks
		public static Action<Font> OnDyslexicFontSettingChanged;
        public static Action<bool> OnTextBackgroundSettingChanged;

		// Set a new state for the Dyslexic font setting and execute all subscribed callbacks
		public void SetDyslexicFontStatus(bool state)
		{
			if (GameManager.Instance.DeviceData.UseDyslexicFont != state)
			{
				GameManager.Instance.DeviceData.UseDyslexicFont = state;
                
                if(state == true)
                {
                    OnDyslexicFontSettingChanged?.Invoke(DyslexicFont);
                }
                else
                {
                    OnDyslexicFontSettingChanged?.Invoke(null);
                }
                GameManager.Instance.DeviceData.Save();
			}
		}
        
        public void SetTextBackgroundStatus(bool state)
        {
            if(GameManager.Instance.DeviceData.UseTextBackgrounds != state)
            {
                GameManager.Instance.DeviceData.UseTextBackgrounds = state;

                OnTextBackgroundSettingChanged?.Invoke(state);

                GameManager.Instance.DeviceData.Save();
            }
        }

	}

}
