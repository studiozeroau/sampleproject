﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.SceneManagement;

namespace Zeroes.Managers
{
    public class SceneLoader
    {
        // The scene ID for the entry point of the application
        public string EntryPoint;

        //A list of all the currently active scenes in the app
        private List<GameScene> ActiveScenes = new List<GameScene>();

        public void Update()
        {
            for (int i = 0; i < ActiveScenes.Count; i++)
            {
				if (ActiveScenes[i].IsSceneReady)
				{
					ActiveScenes[i].Update();
				}
            }
        }

        //Opens a game
        public GameScene OpenScene(GameScene scene)
        {
            ActiveScenes.Add(scene);
            scene.Start();
            return scene;
        }

        //Opens multiple scenes with an optional andThen Action
        public void OpenScenes(GameScene[] scenes, Action andThen = null)
        {
            for (int i = 0; i < scenes.Length; i++)
            {
                OpenScene(scenes[i]);
            }

            if (andThen != null)
            {
                GameManager.Instance.StartCoroutine(LoadBatch(scenes, andThen));
            }
        }

        //Waits for all the scenes in the list to load
        IEnumerator LoadBatch(GameScene[] scenes, Action andThen)
        {
            for (int i = 0; i < scenes.Length; i++)
            {
                yield return YieldForSceneLoad(scenes[i]);
            }

            andThen();
        }

        //Yields until a scene is ready
        IEnumerator YieldForSceneLoad(GameScene scene)
        {
            Zebug.Log($"Loading {scene.SceneID}!", Color.white);

            yield return new WaitUntil(() => scene.IsSceneReady);

            Zebug.Log($"Loaded {scene.SceneID}!", Color.green);
        }

        // Close a game scene directly
        public void CloseScene(GameScene scene)
        {
            scene.Close();
            SceneManager.UnloadSceneAsync(scene.Scene);
            ActiveScenes.Remove(scene);

            Resources.UnloadUnusedAssets();
        }

        // Closes the GameScene with the specified sceneID
        public void CloseScene(string sceneID)
        {
            for (int i = 0; i < ActiveScenes.Count; i++)
            {
                if (ActiveScenes[i].SceneID.Equals(sceneID))
                {
                    CloseScene(ActiveScenes[i]);
                    break;
                }
            }
        }

		// Closes a scene based on the type
        public void CloseScene(Type type)
        {
            foreach (GameScene scene in ActiveScenes)
            {
                if (scene.GetType().Equals(type))
                {
                    CloseScene(scene);
                    break;
                }
            }
        }

        public void CloseScenes(Type[] scenes)
        {
            foreach (var sceneType in scenes)
            {
                CloseScene(sceneType);
            }
        }

        //Returns a GameScene by its SceneID
        public GameScene GetScene(string id)
        {
            for (int i = 0; i < ActiveScenes.Count; i++)
            {
                if (ActiveScenes[i].SceneID == id)
                {
                    return ActiveScenes[i];
                }
            }
            return null;
        }

		// Returns a GameScene by SceneID and casts to Type
        public T GetScene<T>(string id)
        {
            for (int i = 0; i < ActiveScenes.Count; i++)
            {
                if (ActiveScenes[i].SceneID == id)
                {
                    return (T) Convert.ChangeType(ActiveScenes[i], typeof(T));
                }
            }
            return default(T);
        }

		// Returns a scene based on the type
        public T GetScene<T>(Type type)
        {
            for (int i = 0; i < ActiveScenes.Count; i++)
            {
                if (ActiveScenes[i].GetType() == type)
                {
                    return (T)Convert.ChangeType(ActiveScenes[i], typeof(T));
                }
            }
            return default(T);
        }

        public GameScene GetScene(Type type)
        {
            foreach(var scene in ActiveScenes)
            {
                if(scene.GetType() == type)
                {
                    return scene;
                }
            }
            return null;
        }

        // Create a GameScene from an ID
        public static GameScene CreateFromID(string id)
        {
            Type t = Type.GetType(id);

            if (t != null)
            {
                GameScene scene = CreateFromType(t);
                if (scene != null)
                {
                    return scene;
                }
            }

            return new GameScene(id);
        }

        public static GameScene CreateFromType(Type type)
        {
            GameScene scene = Activator.CreateInstance(type) as GameScene;
            return scene;
        }
        
    }
}