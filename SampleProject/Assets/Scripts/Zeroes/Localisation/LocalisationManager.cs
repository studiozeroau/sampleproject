﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

using UnityEngine;

namespace Zeroes.Localisation
{
	public class LocalisationManager
	{
		//Callback so anything that may need to be immedietly relocalised, can be
		public static Action<string> OnLanguageChanged;

		//Store the configuration for the App
		public LocalisationConfiguration Configuration { private set; get; }

		//Current language code
		public string CurrentLanguage { private set; get; }

		// All the loaded content
		// Key = ID
		// Value = Localised String
		private Dictionary<string, string> Content;

		// Keep track of the different content sections we currently have loaded
		private List<string> LoadedContentSections;

		public LocalisationManager()
		{
			Content = new Dictionary<string, string>();
			LoadedContentSections = new List<string>();

			LocalisationConfiguration config = LocalisationUtilities.GetConfig();
			if (config != null)
			{
				Configuration = config;
			}
			else
			{
				Debug.LogWarning("No Localisation Configuration Object found. Please create one in Resources/Localisation");
			}
		}


		//Changes the currently loaded language
		public void ChangeLanguage(string language)
		{
			if (string.IsNullOrEmpty(CurrentLanguage))
			{
				CurrentLanguage = language;
			}
			else
			{
				//Change language and reload any previously loaded content sections
				if (!CurrentLanguage.Equals(language))
				{
					Content.Clear();

					CurrentLanguage = language;

					for (int i = 0; i < LoadedContentSections.Count; i++)
					{
						LoadContent(LoadedContentSections[i]);
					}

					if (OnLanguageChanged != null)
					{
						OnLanguageChanged(CurrentLanguage);
					}
				}
			}

			Debug.Log($"Changed Current Language to: {CurrentLanguage}");
		}


		#region LOADING / UNLOADING

		//Load some content into the dictionary
		public void LoadContent(string contentID)
		{
			TextAsset asset = Resources.Load($"Localisation/{CurrentLanguage}/{contentID}") as TextAsset;

			if (asset == null)
			{
				Debug.LogWarning($"Could not find content {contentID} for Language {CurrentLanguage}!");
				return;
			}

			if (!LoadedContentSections.Contains(contentID))
			{
				LoadedContentSections.Add(contentID);
			}

			Dictionary<string, string> values = JsonConvert.DeserializeObject<Dictionary<string, string>>(asset.text);

			string[] keys = values.Keys.ToArray();

			for (int i = 1; i < keys.Length; i++)
			{
				Content.Add(contentID + "." + keys[i], values[keys[i]]);
			}
		}

		//Shortcut to load all content sections if this is ever necessary to load it up front
		public void LoadAllContent()
		{
			SupportedLanguage currentLanguage = Configuration.GetSupportedLanguage(CurrentLanguage);
			if (currentLanguage != null)
			{
				for (int i = 0; i < currentLanguage.Content.Count; i++)
				{
					LoadContent(currentLanguage.Content[i].ContentName);
				}
			}
		}

		// Unload localisation content
		public void UnloadContent(string contentID)
		{
			string[] keys = Content.Keys.ToArray();

			for (int i = 0; i < keys.Length; i++)
			{
				if (keys[i].StartsWith(contentID))
				{
					Content.Remove(keys[i]);
				}
			}

			LoadedContentSections.Remove(contentID);
		}

		#endregion



		#region STRING RETRIEVAL

		// Returns a localised string, or MISSING if none exists
		public string GetLocalisedString(string id)
		{
			string localisedString;

			if (Content.TryGetValue(id, out localisedString))
			{
				return localisedString;
			}

			Debug.LogWarning($"Unable to find localised String for {id}");

			return "MISSING";
		}

		//TODO. Random string from prefix?

		#endregion

	}
}
