﻿using UnityEngine;

using System.Collections;
using System.Collections.Generic;

using Zeroes;

namespace Zeroes.Localisation
{
    
	public class LocalisationConfiguration : ScriptableObject
	{
		// An Array of all the supported languages for the App
		public List<SupportedLanguage> SupportedLanguages = new List<SupportedLanguage>();

		// Return either English, the Primary Language, or the first language
		public SupportedLanguage DefaultLanguage
		{
			get
			{
				// Return a default of English
				if (SupportedLanguages.Count == 0)
				{
					return new SupportedLanguage
					{
						LangCode = "en",
						SystemLanguage = SystemLanguage.English
					};
				}
				else
				{
					// Return primary language
					for (int i = 0; i < SupportedLanguages.Count; i++)
					{
						if (SupportedLanguages[i].IsPrimaryLanguage)
						{
							return SupportedLanguages[i];
						}
					}
				}
				// Return first if all else fails
				return SupportedLanguages[0];
			}
		}

		//Returns true if the system language is supported by this App's configuration
		public bool IsLanguageSupported(SystemLanguage systemLanguage)
		{
			for (int i = 0; i < SupportedLanguages.Count; i++)
			{
				if (SupportedLanguages[i].SystemLanguage.Equals(systemLanguage))
				{
					return true;
				}
			}
			return false;
		}

		//Returns the code in the configuration for the system language
		public string CodeForSystemLanguage(SystemLanguage systemLanguage)
		{
			for (int i = 0; i < SupportedLanguages.Count; i++)
			{
				if (SupportedLanguages[i].SystemLanguage.Equals(systemLanguage))
				{
					return SupportedLanguages[i].LangCode;
				}
			}

			return string.Empty;
		}

		// Gets a supported language by language code
		public SupportedLanguage GetSupportedLanguage(string langCode)
		{
			for (int i = 0; i < SupportedLanguages.Count; i++)
			{
				if (SupportedLanguages[i].LangCode.Equals(langCode))
				{
					return SupportedLanguages[i];
				}
			}
			return null;
		}

		// Gets a supported language via system language
		public SupportedLanguage GetSupportedLanguage(SystemLanguage systemLanguage)
		{
			for (int i = 0; i < SupportedLanguages.Count; i++)
			{
				if (SupportedLanguages[i].SystemLanguage.Equals(systemLanguage))
				{
					return SupportedLanguages[i];
				}
			}
			return null;
		}


	}

	[System.Serializable]
	public class SupportedLanguage
	{
		// Is this the primary reference language for the rest of the app
		// Setting this to TRUE will model all other SupportedLanguage instances after this
		public bool IsPrimaryLanguage;

		// A string language code for the language
		public string LangCode;

		// A hard defined system language for this supported language. Can also be 'Unknown'
		public SystemLanguage SystemLanguage;

		// The ID for the Google Spreadsheet for this bit of localisation
		public string GoogleSheetID;

		// Where do hard .tsv files for the localisation sit?
		public string SpreadsheetFilepath;

		// The content sections for the language
		public List<LanguageContent> Content;

		// Miscellaneous content?
		// Will certain languages ever need their own specific content section?
		//public List<LanguageContent> MiscContent;
	}

	[System.Serializable]
	public class LanguageContent
	{
		// What is the name of this content section. Also used for file names
		public string ContentName;

		// What is the GID (sub sheet id) for the Language's Google Sheet
		public string GoogleSheetGID;
	}
}