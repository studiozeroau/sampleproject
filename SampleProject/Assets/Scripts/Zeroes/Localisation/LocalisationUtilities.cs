﻿using System;

using UnityEngine;

namespace Zeroes.Localisation
{
	public static class LocalisationUtilities
	{
		// Get the config file
		public static LocalisationConfiguration GetConfig()
		{
			return Resources.Load("Localisation/LocalisationConfiguration") as LocalisationConfiguration;
		}

	}
}