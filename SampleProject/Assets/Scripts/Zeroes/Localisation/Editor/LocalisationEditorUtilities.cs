﻿using System;
using System.IO;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Networking;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEditor.ProjectWindowCallback;

using Zeroes.Editor;

namespace Zeroes.Localisation
{
	public static class LocalisationEditorUtilities
	{
        // Editor Icons can be found at
        // https://unitylist.com/p/5c3/Unity-editor-icons

        [MenuItem("Assets/Create/Zeroes/New Localisation Configuration", false, 0)]
		private static void CreateLocalisationConfiguration()
		{
			// Get the appropriate icon to show in the editor
			Texture2D icon = EditorGUIUtility.IconContent("ScriptableObject Icon").image as Texture2D;

            // Callback to finish creating the Asset once name input has completed
            EndNameEditActionCallback callback = ScriptableObject.CreateInstance<EndNameEditActionCallback>();
            callback.OnComplete += (int instanceID, string pathName, string resourceFile) =>
            {
                AssetDatabase.CreateAsset(ScriptableObject.CreateInstance<LocalisationConfiguration>(), pathName);
            };

			// Start the process of creating a new Scene
			ProjectWindowUtil.StartNameEditingIfProjectWindowExists(0, callback, Utilities.GetCurrentProjectWindowPath() + "/New Localisation Configuration.asset", icon, null);
		}
        
		// Get the config file
		public static LocalisationConfiguration GetConfig()
		{
			LocalisationConfiguration config = LocalisationUtilities.GetConfig();

			//If no config, show a dialog
			if (config == null)
			{
				EditorUtility.DisplayDialog("Missing Localisation Config", "You are missing a Localisation Configuration Object, please make sure you have one in the Resources folder!", "Ok");
				return null;
			}

			return config;
		}

		// Import and save a google sheet as some JSON
		public static bool ReimportFromGoogle(string key, string gid, string name, string filepath)
		{
			// The URL used to download a Google sheet
			string url = "https://docs.google.com/spreadsheets/d/{0}/export?{1}&id={2}&gid={3}";

			// Full URL for the download
			string fullURL = string.Format(url, key, "format=tsv", key, gid);

			UnityWebRequest request = UnityWebRequest.Get(fullURL);
			request.SendWebRequest();

			while (!request.isDone)
			{
				// Wait
			}

			if (request.error != null)
			{
				return false;
			}

			string json = TSVUtilities.ToJSON(request.downloadHandler.text);

			string filename = string.Format("/{0}.json", name);

			if (!Directory.Exists(filepath))
			{
				Directory.CreateDirectory(filepath);
			}

			File.WriteAllText(filepath + filename, json);

			AssetDatabase.Refresh();
			return true;
		}

		// Bulk reimports all localisation information from google sheets based on current configuration
		public static void ReimportAllFromGoogle()
		{
			LocalisationConfiguration config = GetConfig();

			// Let's figure out the number of spreadsheets we're actually importing
			int sheets = config.SupportedLanguages[0].Content.Count * config.SupportedLanguages.Count;
			int numImported = 0;

			List<string> issues = new List<string>();

			foreach (var l in config.SupportedLanguages)
			{
				string filepath = AssetDatabase.GetAssetPath(config.GetInstanceID());
				filepath = filepath.Replace(config.name + ".asset", "/");
				filepath = filepath + l.LangCode;

				foreach (var c in l.Content)
				{
					EditorUtility.DisplayProgressBar("Importing Google Sheets...", $"Importing {numImported} of {sheets} - {l.LangCode}/{l.SystemLanguage.ToString()}:{c.ContentName}", (float)numImported / (float)sheets);

					if (!LocalisationEditorUtilities.ReimportFromGoogle(l.GoogleSheetID, c.GoogleSheetGID, c.ContentName, filepath))
					{
						// Add to a list of issues so it doesn't interrupt the import procecss
						issues.Add($"- Could not import sheet for {l.LangCode}/{l.SystemLanguage.ToString()}:{c.ContentName}");
					}
					numImported++;
				}
			}

			EditorUtility.ClearProgressBar();
			if (issues.Count > 0)
			{
				string errorMsg = "";
				foreach (var i in issues)
				{
					errorMsg = errorMsg + i + "\n";
				}

				EditorUtility.DisplayDialog("Finished with errors!", $"{numImported - issues.Count}/{sheets} imported successfully:\n{errorMsg}", "Ok");
			}
			else
			{
				EditorUtility.DisplayDialog("Success!", "All language spreadsheets imported successfully!", "Ok");
			}
		}

		// Import and save a TSV file as some JSON
		public static bool ReimportFromTSV(string name, string filepath, string outputFilepath)
		{
			if (!File.Exists(filepath))
			{
				return false;
			}

			string json = TSVUtilities.ToJSON(File.ReadAllText(filepath));

			string filename = string.Format("/{0}.json", name);

			if (!Directory.Exists(outputFilepath))
			{
				Directory.CreateDirectory(outputFilepath);
			}

			File.WriteAllText(outputFilepath + filename, json);

			AssetDatabase.Refresh();

			return true;
		}

		public static void ReimportAllFromTSV()
		{
			LocalisationConfiguration config = GetConfig();

			// Let's figure out the number of spreadsheets we're actually importing
			int sheets = config.SupportedLanguages[0].Content.Count * config.SupportedLanguages.Count;
			int numImported = 0;

			List<string> issues = new List<string>();

			foreach (var l in config.SupportedLanguages)
			{
				string filepath = AssetDatabase.GetAssetPath(config.GetInstanceID());
				filepath = filepath.Replace(config.name + ".asset", "/");
				filepath = filepath + l.LangCode;

				foreach (var c in l.Content)
				{
					EditorUtility.DisplayProgressBar("Importing Spreadsheet...", $"Importing {numImported} of {sheets} - {l.LangCode}/{l.SystemLanguage.ToString()}:{c.ContentName}", (float)numImported / (float)sheets);

					if (!LocalisationEditorUtilities.ReimportFromTSV(c.ContentName, l.SpreadsheetFilepath + "/" + c.ContentName + ".tsv", filepath))
					{
						// Add to a list of issues so it doesn't interrupt the import procecss
						issues.Add($"- Could not import sheet for {l.LangCode}/{l.SystemLanguage.ToString()}:{c.ContentName}");
					}
					numImported++;
				}
			}

			EditorUtility.ClearProgressBar();
			if (issues.Count > 0)
			{
				string errorMsg = "";
				foreach (var i in issues)
				{
					errorMsg = errorMsg + i + "\n";
				}

				EditorUtility.DisplayDialog("Finished with errors!", $"{numImported - issues.Count}/{sheets} imported successfully:\n{errorMsg}", "Ok");
			}
			else
			{
				EditorUtility.DisplayDialog("Success!", "All language spreadsheets imported successfully!", "Ok");
			}

		}
	}
}
