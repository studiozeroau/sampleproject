﻿using UnityEngine;
using UnityEditor;
using UnityEngine.Networking;


using System.Collections;
using System.IO;

using Zeroes;

namespace Zeroes.Localisation
{

	public class LocalisationMenu
	{

		[MenuItem("Zeroes/Localisation/Import All From Google")]
		private static void GoogleReimport()
		{
			LocalisationEditorUtilities.ReimportAllFromGoogle();
		}

		[MenuItem("Zeroes/Localisation/Import All From File")]
		private static void FileReimport()
		{
			LocalisationEditorUtilities.ReimportAllFromTSV();
		}

	}
}