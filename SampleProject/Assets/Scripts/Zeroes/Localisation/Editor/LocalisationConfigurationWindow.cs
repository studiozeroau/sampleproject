﻿using UnityEngine;
using UnityEditor;

using System.Collections;
using System.Collections.Generic;

// Dedicated Localisation Manager Editor Window

namespace Zeroes.Localisation
{
	public class LocalisationConfigurationWindow : EditorWindow
	{

		private LocalisationConfiguration Config;

		private Vector2 Scroll = Vector2.zero;

		private int SelectedLanguage = 0;

		[MenuItem("Zeroes/Localisation/Language and Content Editor")]
		static void init()
		{
			LocalisationConfigurationWindow window = (LocalisationConfigurationWindow)EditorWindow.GetWindow(typeof(LocalisationConfigurationWindow), false, "Localisation Editor");
			window.Show();
		}

		public static LocalisationConfiguration GetConfig()
		{
			return Resources.Load("Localisation/LocalisationConfiguration") as LocalisationConfiguration;
		}

		private void OnFocus()
		{
			if (Config == null)
			{
				Config = GetConfig();
			}
		}



		void OnGUI()
		{
			if (Config == null)
			{
				GUILayout.Label("No Localisation Configuration. Would you like to make one?");

				if (GUILayout.Button("Create Localisation Configuration"))
				{
					//Create stuff here
					LocalisationConfiguration newConfig = ScriptableObject.CreateInstance<LocalisationConfiguration>();

					string filepath = EditorUtility.SaveFilePanel("", "", "LocalisationConfiguration", "");
					filepath = filepath.Substring(filepath.IndexOf("Assets"));

					AssetDatabase.CreateAsset(newConfig, filepath + ".asset");
					AssetDatabase.SaveAssets();
					AssetDatabase.Refresh();

					OnFocus();
				}

				return;
			}

			EditorGUI.DrawRect(new Rect(0, 70, position.width, 5), Color.grey);
			
			#region HEADER

			ReimportContentElement();

			LanguageDropdown();

			NewSupportedLanguageButton();

			#endregion

			#region LANG CONTENT

			GUILayout.Space(25);

			if (Config.SupportedLanguages.Count > 0)
			{
				Scroll = EditorGUILayout.BeginScrollView(Scroll);

				ShowCurrentLanguage();

				EditorGUILayout.EndScrollView();
			}
			else
			{
				// Add a prompt to make some languages!
			}
			
			EditorUtility.SetDirty(Config);

			#endregion
		}


		#region REIMPORT

		private void ReimportContentElement()
		{
			EditorGUILayout.BeginHorizontal();

			if (Config.SupportedLanguages.Count == 0)
			{
				EditorGUI.BeginDisabledGroup(true);
			}
			else
			{
				EditorGUI.BeginDisabledGroup(Config.SupportedLanguages[SelectedLanguage].Content.Count == 0);
			}

			if (GUILayout.Button("Reimport All From Google"))
			{
				//Reimport all the current content from google sheet information
				LocalisationEditorUtilities.ReimportAllFromGoogle();
			}

			if (GUILayout.Button("Reimport All From File"))
			{
				//Reimport all the current content form a spreadsheet on the hard drive
				LocalisationEditorUtilities.ReimportAllFromTSV();
			}

			EditorGUI.EndDisabledGroup();

			EditorGUILayout.EndHorizontal();
		}

		#endregion



		#region CURRENT LANGUAGE

		private void LanguageDropdown()
		{
			EditorGUILayout.BeginHorizontal();

			EditorGUILayout.LabelField("Supported Languages", GUILayout.MaxWidth(150));

			string[] langs = new string[Config.SupportedLanguages.Count];
			for (int i = 0; i < langs.Length; i++)
			{
				langs[i] = Config.SupportedLanguages[i].SystemLanguage.ToString();
			}

			SelectedLanguage =  EditorGUILayout.Popup(SelectedLanguage, langs, GUILayout.MinWidth(250));

			EditorGUILayout.EndHorizontal();
		}

		#endregion



		#region SUPPORTED LANGUAGE ELEMENTS

		private void ShowCurrentLanguage()
		{
			// Return and redraw if the supported language element gets deleted
			SupportedLanguageElement(Config.SupportedLanguages[SelectedLanguage]);
		}

		// Creates an element for supported language
		private bool SupportedLanguageElement(SupportedLanguage supportedLanguage)
		{
			EditorGUILayout.BeginHorizontal();

			//Dropdown for supported language
			supportedLanguage.SystemLanguage = (SystemLanguage)EditorGUILayout.EnumPopup("System Language", supportedLanguage.SystemLanguage);

			supportedLanguage.LangCode = EditorGUILayout.TextField(supportedLanguage.LangCode, GUILayout.MaxWidth(50));

			if (GUILayout.Button("-", GUILayout.MaxWidth(50)))
			{
				// Warn the user
				if (EditorUtility.DisplayDialog("Delete this language?", "Are you sure you want to delete this language?", "Yes", "No"))
				{
					// Delete this language
					Config.SupportedLanguages.RemoveAt(Config.SupportedLanguages.IndexOf(supportedLanguage));
					SelectedLanguage = 0;
					return false;
				}
			}

			EditorGUILayout.EndHorizontal();

			IsPrimaryLanguage(supportedLanguage);

			// Draw the google sheet ID
			EditorGUILayout.BeginHorizontal();

			EditorGUILayout.LabelField("Google Sheet ID", GUILayout.MaxWidth(150));

			supportedLanguage.GoogleSheetID = EditorGUILayout.TextField(supportedLanguage.GoogleSheetID, GUILayout.MinWidth(250));

			EditorGUILayout.EndHorizontal();



			// Draw a filepath on the system we can reimport this from
			EditorGUILayout.BeginHorizontal();

			EditorGUILayout.LabelField("Spreadsheet Folder", GUILayout.MaxWidth(150));

			supportedLanguage.SpreadsheetFilepath = EditorGUILayout.TextField(supportedLanguage.SpreadsheetFilepath, GUILayout.MinWidth(250));

			if (GUILayout.Button("...", GUILayout.MaxWidth(50)))
			{
				// Open a dialog to find a file?
				supportedLanguage.SpreadsheetFilepath = EditorUtility.OpenFolderPanel("", supportedLanguage.SpreadsheetFilepath, "");
			}

			EditorGUILayout.EndHorizontal();



			// Draw all the content
			foreach (var c in supportedLanguage.Content)
			{
				if (!DrawLanguageContent(supportedLanguage, c))
				{
					// Delete me!
					int index = supportedLanguage.Content.IndexOf(c);
					supportedLanguage.Content.RemoveAt(index);

					// If this was primary language, update others to match
					if (supportedLanguage.IsPrimaryLanguage)
					{
						foreach (var l in Config.SupportedLanguages)
						{
							if (!l.Equals(supportedLanguage))
							{
								l.Content.RemoveAt(index);
							}
						}
					}
					return false;
				}
			}

			// Make a button to add more content sections
			NewLanguageContentButton(supportedLanguage);

			return true;
		}

		// Primary language = the model for all other languages
		private void IsPrimaryLanguage(SupportedLanguage supportedLanguage)
		{
			EditorGUILayout.BeginHorizontal();

			EditorGUILayout.LabelField("Is Primary Language?", GUILayout.MaxWidth(150));

			bool didChange = supportedLanguage.IsPrimaryLanguage;
			supportedLanguage.IsPrimaryLanguage = EditorGUILayout.Toggle(supportedLanguage.IsPrimaryLanguage);
			if (didChange != supportedLanguage.IsPrimaryLanguage)
			{
				
				// If this is now the primary language, we need to change ALL others
				if (supportedLanguage.IsPrimaryLanguage)
				{
					// Not enough language to change, so don't bother
					if (Config.SupportedLanguages.Count <= 1)
					{
						return;
					}

					// Provide a warning to the user
					if (!EditorUtility.DisplayDialog("Primary language changed!", "Changing the primary language will erase ALL content configurations on other languages and model them after this one. Are you sure you'd like to continue?", "Yes", "No"))
					{
						supportedLanguage.IsPrimaryLanguage = false;
						return;
					}

					// Update all other languages to be modelled after the primary
					foreach (var l in Config.SupportedLanguages)
					{
						if (!l.Equals(supportedLanguage))
						{
							l.IsPrimaryLanguage = false;

							// We need to clear all content and update the names to match
							l.Content = new List<LanguageContent>();
							for (int i = 0; i < supportedLanguage.Content.Count; i++)
							{
								l.Content.Add(new LanguageContent()
								{ 
									ContentName = supportedLanguage.Content[i].ContentName
								});
							}
						}
					}
				}
			}

			EditorGUILayout.EndHorizontal();
		}


		// Creates a button for adding a new supported language
		private void NewSupportedLanguageButton()
		{
			EditorGUILayout.BeginHorizontal();

			EditorGUILayout.LabelField("Add New Language");

			// Create a new supported language
			if (GUILayout.Button("+", GUILayout.MaxWidth(50)))
			{
				SupportedLanguage newLang = new SupportedLanguage()
				{
					SystemLanguage = SystemLanguage.Unknown,
					LangCode = "",
					Content = new List<LanguageContent>(),
					IsPrimaryLanguage = Config.SupportedLanguages.Count == 0
				};

				// If this was not the primary language, add all items from the primary language
				if (!newLang.IsPrimaryLanguage)
				{
					SupportedLanguage primaryLangage = GetPrimaryLanguage();
					if (primaryLangage != null)
					{
						foreach (var c in primaryLangage.Content)
						{
							newLang.Content.Add(new LanguageContent()
							{
								ContentName = c.ContentName
							});
						}
					}
				}

				Config.SupportedLanguages.Add(newLang);

				// Force the interface to display the new language
				SelectedLanguage = Config.SupportedLanguages.Count - 1;
			}

			EditorGUILayout.EndHorizontal();
		}

		#endregion



		#region LANGUAGE CONTENT ELEMENT

		// Draw a whole section of language content
		private bool DrawLanguageContent(SupportedLanguage supportedLanguage, LanguageContent languageContent)
		{
			// Draw the name of this content section
			EditorGUILayout.BeginHorizontal();

			GUILayout.Space(15);

			// Add some text for the Name of the group and disable if not primary language
			EditorGUI.BeginDisabledGroup(!supportedLanguage.IsPrimaryLanguage);
			EditorGUILayout.LabelField("Name", GUILayout.MaxWidth(150));
			languageContent.ContentName = EditorGUILayout.TextField(languageContent.ContentName, GUILayout.MinWidth(250));
			EditorGUI.EndDisabledGroup();

			// If this is the primary language, then update all other langs to match
			if (supportedLanguage.IsPrimaryLanguage)
			{
				foreach (var l in Config.SupportedLanguages)
				{
					if (!l.Equals(supportedLanguage))
					{
						l.Content[supportedLanguage.Content.IndexOf(languageContent)].ContentName = languageContent.ContentName;
					}
				}
			}

			// Add a button to remove this content and disable if not primary language
			EditorGUI.BeginDisabledGroup(!supportedLanguage.IsPrimaryLanguage);
			if (GUILayout.Button("-", GUILayout.MaxWidth(50)))
			{
				return false;
			}
			EditorGUI.EndDisabledGroup();


			EditorGUILayout.EndHorizontal();


			// Draw a google sheet GID to import this sheet from
			EditorGUILayout.BeginHorizontal();

			GUILayout.Space(15);

			EditorGUILayout.LabelField("Google Sheet GID", GUILayout.MaxWidth(150));

			languageContent.GoogleSheetGID = EditorGUILayout.TextField(languageContent.GoogleSheetGID, GUILayout.MinWidth(250));

			EditorGUILayout.EndHorizontal();


			EditorGUILayout.BeginHorizontal();

			GUILayout.Space(15);

			if (GUILayout.Button("Reimport from Google"))
			{
				// Reimport the sheet from google
				EditorUtility.DisplayProgressBar("Importing Google Sheet...", $"Importing {supportedLanguage.LangCode}/{supportedLanguage.SystemLanguage.ToString()}:{languageContent.ContentName}", 0);

				string filepath = AssetDatabase.GetAssetPath(Config.GetInstanceID());
				filepath = filepath.Replace(Config.name + ".asset", "/");
				filepath = filepath + supportedLanguage.LangCode;

				if (LocalisationEditorUtilities.ReimportFromGoogle(supportedLanguage.GoogleSheetID, languageContent.GoogleSheetGID, languageContent.ContentName, filepath))
				{
					EditorUtility.ClearProgressBar();
					EditorUtility.DisplayDialog("Success!", "Spreadsheet imported successfully!", "Ok");
				}
				else
				{
					EditorUtility.ClearProgressBar();
					EditorUtility.DisplayDialog("Error!", "Spreadsheet was not imported successfully! Please check the ID and GID!", "Ok");
				}
			}

			if (GUILayout.Button("Reimport from file"))
			{
				// Reimport the sheet from a file
				EditorUtility.DisplayProgressBar("Importing Spreadsheet...", $"Importing {supportedLanguage.LangCode}/{supportedLanguage.SystemLanguage.ToString()}:{languageContent.ContentName}", 0);

				string filepath = AssetDatabase.GetAssetPath(Config.GetInstanceID());
				filepath = filepath.Replace(Config.name + ".asset", "/");
				filepath = filepath + supportedLanguage.LangCode;

				if (LocalisationEditorUtilities.ReimportFromTSV(languageContent.ContentName, supportedLanguage.SpreadsheetFilepath + "/" + languageContent.ContentName + ".tsv", filepath))
				{
					EditorUtility.ClearProgressBar();
					EditorUtility.DisplayDialog("Success!", "Spreadsheet imported successfully!", "Ok");
				}
				else
				{
					EditorUtility.ClearProgressBar();
					EditorUtility.DisplayDialog("Error!", "Spreadsheet was not imported successfully! Please check the filepath!", "Ok");
				}
			}

			EditorGUILayout.EndHorizontal();

			return true;
		}

		public void NewLanguageContentButton(SupportedLanguage supportedLanguage)
		{
			if (!supportedLanguage.IsPrimaryLanguage)
			{
				return;
			}

			EditorGUILayout.BeginHorizontal();

			GUILayout.Space(15);

			EditorGUILayout.LabelField("Add new language content section");

			// Create a new language content section
			if (GUILayout.Button("+", GUILayout.MaxWidth(50)))
			{
				supportedLanguage.Content.Add(new LanguageContent()
				{
					ContentName = "",
					GoogleSheetGID = ""
				});

				// If this is the primary language, we need to add this content section to the others
				if (supportedLanguage.IsPrimaryLanguage)
				{
					foreach (var l in Config.SupportedLanguages)
					{
						if (!l.Equals(supportedLanguage))
						{
							l.Content.Add(new LanguageContent()
							{
								ContentName = "",
								GoogleSheetGID = ""
							});
						}
					}
				}
			}

			EditorGUILayout.EndHorizontal();
		}

		#endregion

		private SupportedLanguage GetPrimaryLanguage()
		{
			foreach (var l in Config.SupportedLanguages)
			{
				if (l.IsPrimaryLanguage)
				{
					return l;
				}
			}
			return null;
		}

	}
}