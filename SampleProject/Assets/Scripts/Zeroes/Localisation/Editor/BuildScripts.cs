﻿using System;
using System.IO;

using UnityEditor;
using UnityEditor.Callbacks;


#if UNITY_IPHONE
using UnityEditor.iOS.Xcode;
#endif

namespace Zeroes.Localisation
{
	// Add localisation information to the build

	public static class BuildScripts
	{
		[PostProcessBuild(999)]
        public static void OnBuild(BuildTarget buildTarget, string pathToBuiltProject)
        {
#if UNITY_IOS

            // Get plist
            string plistPath = pathToBuiltProject + "/Info.plist";
            PlistDocument plist = new PlistDocument();
            plist.ReadFromString(File.ReadAllText(plistPath));

            // Get root
            PlistElementDict rootDict = plist.root;

            // Add the localisations to the plist so I don't have to keep manually adding this
            PlistElementArray langs = rootDict.CreateArray("CFBundleLocalizations");

            LocalisationConfiguration config = LocalisationUtilities.GetConfig();
            if (config != null)
            {
                foreach (var lang in config.SupportedLanguages)
                {
                    langs.AddString(lang.SystemLanguage.ToString());
                }
            }

            // Write to file
            File.WriteAllText(plistPath, plist.WriteToString());

#endif

        }
    }
}