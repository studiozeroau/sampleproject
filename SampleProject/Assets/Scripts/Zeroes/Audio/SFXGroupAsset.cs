﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SFXGroupAsset", menuName = "Zeroes/Audio/SFXGroupAsset", order = 1)]
public class SFXGroupAsset : ScriptableObject
{

    [SerializeField]
    private AudioClip[] _SFXClips = null;
    public AudioClip[] SFXClips => _SFXClips;
}
