﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEditor;

using Zeroes;

[CustomEditor(typeof(AudioConfiguration))]
public class AudioConfigurationEditor : ZEditor
{
    // SerializedProperties

    void OnEnable()
    {
    }

    // Custom inspector
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
    }
}
