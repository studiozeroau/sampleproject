﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Threading;

using UnityEngine;
using UnityEngine.Audio;

using Zeroes.Managers;
using UnityEngine.UIElements;

namespace Zeroes.Audio
{

    public struct SFXAsset
    {
        public AudioClip Clip;
        public string Group;
    }

    public class SFXVoice
    {

    }

    public class AudioManager : MonoBehaviour
    {
        // Shortcut to Device Data
        private DeviceData DeviceData => GameManager.Instance.DeviceData;

        // Some volume constants
        public const float MaxDB = 20f;
        public const float MinDB = -60f;

        public AudioListener AudioListener;

        // Global Audio Mixer
        private AudioMixer AudioMixer;

        // Cache the groups of audio so we can delegate sources
        private AudioMixerGroup MusicGroup;
        private AudioMixerGroup SFXGroup;

        // Audio Source for Music
        private AudioSource MusicSource;

        // Audio Source for SFX
        private AudioSource SFXSource;

        // The music we're currently playing
        public MusicAsset CurrentMusic { private set; get; }

        // Currently loaded Music Assets
        private Dictionary<string, MusicAsset> Music;

        // Currently loaded sound effects
        private Dictionary<string, SFXAsset> SFX;

        private Coroutine _musicLoop;

        // Master volume controls
        private const string kMasterVolume = "MasterVolume";
        public float Volume
        {
            get
            {
                return GetVolume(kMasterVolume);
            }
            set
            {
                SetVolume(kMasterVolume, value);
            }
        }

        // Music Volume controls
        private const string kMusicVolume = "MusicVolume";
        public float MusicVolume
        {
            get
            {
                return GetVolume(kMusicVolume);
            }
            set
            {
                SetVolume(kMusicVolume, value);
            }
        }
        public bool MusicMuted
        {
            get
            {
                return DeviceData.MusicMuted;
            }
            set
            {
                if(value == true)
                {
                    MusicVolume = 0;
                }
                else
                {
                    MusicVolume = DeviceData.MusicVolume;
                }
            }
        }

        // SFX Volume controls
        private const string kSFXVolume = "SFXVolume";
        public float SFXVolume
        {
            get
            {
                return GetVolume(kSFXVolume);
            }
            set
            {
                SetVolume(kSFXVolume, value);
            }
        }
        public bool SFXMuted
        {
            get
            {
                return DeviceData.SFXMuted;
            }
            set
            {
                if (value == true)
                {
                    SFXVolume = 0;
                }
                else
                {
                    SFXVolume = DeviceData.SFXVolume;
                }
            }
        }

        public void Awake()
        {
            // Create the source for the music
            MusicSource = gameObject.AddComponent<AudioSource>();
            MusicSource.playOnAwake = false;
            MusicSource.Stop();

            // Create the source for SFX
            SFXSource = gameObject.AddComponent<AudioSource>();
            SFXSource.playOnAwake = false;
            SFXSource.Stop();

            // Create an audio listener
            AudioListener = gameObject.AddComponent<AudioListener>();
        }

        public void Configure(AudioConfiguration audioConfiguration)
        {
            AudioMixer = audioConfiguration.AudioMixer;

            // Get our groups from the Audio Mixer
            MusicGroup = AudioMixer.FindMatchingGroups("Music")[0];
            SFXGroup = AudioMixer.FindMatchingGroups("SFX")[0];

            MusicSource.outputAudioMixerGroup = MusicGroup;
            SFXSource.outputAudioMixerGroup = SFXGroup;

            // Load up all the music
            Music = new Dictionary<string, MusicAsset>();
            foreach (var music in audioConfiguration.MusicAssets)
            {
                Music.Add(music.name, music);
            }

            // Load up all the SFX
            SFX = new Dictionary<string, SFXAsset>();
            foreach (var sfx in audioConfiguration.SFXAssets)
            {
                LoadSFXAsset(sfx);
            }

            // Set volumes on startup
            if(MusicMuted == false)
            {
                MusicVolume = DeviceData.MusicVolume;
            }
            else
            {
                MusicVolume = 0;
            }
            if(SFXMuted == false)
            {
                SFXVolume = DeviceData.SFXVolume;
            }
            else
            {
                SFXVolume = 0;
            }
        }

        // Loads a specific SFX asset
        public void LoadSFXAsset(SFXGroupAsset SFXGroup)
        {
            foreach (var clip in SFXGroup.SFXClips)
            {
                clip.LoadAudioData();
                SFX.Add(clip.name, new SFXAsset()
                {
                    Clip = clip,
                    Group = SFXGroup.name
                });
            }
        }

        // Unloads a specific set of SFX assets
        public void UnloadSFXAsset(string SFXGroup)
        {
            foreach (var k in SFX.Keys.ToArray())
            {
                if(SFX[k].Group.Equals(SFXGroup))
                {
                    SFX[k].Clip.UnloadAudioData();
                    SFX.Remove(k);
                }
            }
        }

        // Play music based on ID and return the asset
        // TODO. This could probably return some kind of voice
        public MusicAsset PlayMusic(string musicID)
        {
            if (Music.TryGetValue(musicID, out MusicAsset asset) == false)
            {
                throw new Exception($"Music Asset {musicID} not loaded");
            }

            CurrentMusic = asset;

            MusicSource.clip = CurrentMusic.MusicClips[0];
            MusicSource.Play();

            _musicLoop = StartCoroutine(MusicLoop());

            return asset;
        }

        public void PauseMusic(bool pause)
        {
            if(_musicLoop != null)
            {
                if (pause == true)
                {
                    MusicSource.Pause();
                    StopCoroutine(_musicLoop);
                }
                else
                {
                    MusicSource.UnPause();
                    _musicLoop = StartCoroutine(MusicLoop());
                }
            }
        }

        // Stops music running, does not require specific asset
        public void StopMusic()
        {
            MusicSource.Stop();
            StopCoroutine(_musicLoop);
        }

        // Oneshot plays an SFX
        public void PlaySFX(string sfxID, float volume = 1f)
        {
            SFXAsset asset = GetSFXClip(sfxID);

            SFXSource.PlayOneShot(asset.Clip, volume);
        }

        public void PlaySFX(AudioClip sfxClip, float volume = 1f)
        {
            PlaySFX(sfxClip.name, volume);
        }

        // TODO. Play an SFX at a location
        public void PlaySFX(string sfxID, Vector3 position)
        {
            SFXAsset asset = GetSFXClip(sfxID);
        }

        // Gets a loaded SFX asset
        public SFXAsset GetSFXClip(string sfxID)
        {
            if(SFX != null)
            {
                if (SFX.TryGetValue(sfxID, out SFXAsset sfx) == false)
                {
                    throw new Exception($"SFX Asset {sfxID} not loaded");
                }
                return sfx;
            }
            // TODO. Add a temp missing sfx
            return new SFXAsset();
        }

        private IEnumerator MusicLoop()
        {
            while(true)
            {
                while (MusicSource.time < MusicSource.clip.length)
                {
                    yield return null;
                }

                int index = CurrentMusic.MusicClips.IndexOf(MusicSource.clip);
                if (index == CurrentMusic.MusicClips.Length - 1)
                {
                    index = 0;
                }
                else
                {
                    index++;
                }

                MusicSource.clip = CurrentMusic.MusicClips[index];
                MusicSource.Play();

                yield return null;
            }
        }

        // Sets a volume
        private void SetVolume(string key, float value)
        {
            value = MinDB * (1 - value);

            AudioMixer.SetFloat(key, (int)value);
        }

        // Gets volume
        private float GetVolume(string key)
        {
            float volume = 1f;
            AudioMixer.GetFloat(key, out volume);
            return volume;
        }

        public void SetMuted(bool muted)
        {
            AudioListener.pause = muted;
        }

        public void OnApplicationPause(bool pause)
        {
            if(pause == true)
            {
                PauseMusic(true);
            }
        }

        public void OnApplicationFocus(bool focus)
        {
            if(focus == true)
            {
                PauseMusic(false);
            }
        }
    }
}