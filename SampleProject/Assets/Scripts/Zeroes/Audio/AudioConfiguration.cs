﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Audio;

[CreateAssetMenu(fileName = "AudioConfiguration", menuName = "Zeroes/Audio/AudioConfiguration", order = 1)]
public class AudioConfiguration : ScriptableObject
{

    // Master Audio Mixer for the game
    [SerializeField]
    private AudioMixer _AudioMixer = null;
    public AudioMixer AudioMixer => _AudioMixer;

    // Curve to give granular control over volume controls
    [SerializeField]
    private AnimationCurve _MusicVolumeCurve = null;
    public AnimationCurve MusicVolumeCurve => _MusicVolumeCurve;

    // Curve to give granular control over volume controls
    [SerializeField]
    private AnimationCurve _SFXVolumeCurve = null;
    public AnimationCurve SFXVolumeCurve => _SFXVolumeCurve;

    // All Music Assets for the game
    [SerializeField, InspectorExpandable]
    private MusicAsset[] _MusicAssets = null;
    public MusicAsset[] MusicAssets => _MusicAssets;
    
    // All SFX Assets for the game
    [SerializeField, InspectorExpandable]
    private SFXGroupAsset[] _SFXAssets = null;
    public SFXGroupAsset[] SFXAssets => _SFXAssets;
}
