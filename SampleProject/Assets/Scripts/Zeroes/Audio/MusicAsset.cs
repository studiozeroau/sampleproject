﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Describes a musical asset that can contain multiple parts
[CreateAssetMenu(fileName = "MusicAsset", menuName = "Zeroes/Audio/MusicAsset", order = 1)]
public class MusicAsset : ScriptableObject
{
    [SerializeField]
    private AudioClip[] _MusicClips = null;
    public AudioClip[] MusicClips => _MusicClips;
    
}
