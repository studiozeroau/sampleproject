﻿// #define DEBUG_TOUCH_AREAS

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace Zeroes.Touch
{
    public class TouchManager
    {

        private List<TouchArea> TouchAreas = new List<TouchArea>();
		private List<TouchArea3D> TouchAreas3D = new List<TouchArea3D>();

        //Global enabled bool for touch manager
        private bool _Enabled = true;
        public bool Enabled
        {
            get
            {
                return _Enabled;
            }
            set
            {
                if (_Enabled != value)
                {
                    _Enabled = value;
                    if (!_Enabled)
                    {
                        CancelAll();
                    }
                }
            }
        }


		#region MANAGEMENT

		public void AddTouchArea(TouchArea touchArea)
        {
            TouchAreas.Add(touchArea);
        }
		public void AddTouchArea(TouchArea3D touchArea)
		{
			TouchAreas3D.Add(touchArea);
		}

        public void RemoveTouchArea(TouchArea touchArea)
        {
            TouchAreas.Remove(touchArea);
            touchArea = null;
        }
		public void RemoveTouchArea(TouchArea3D touchArea)
		{
			TouchAreas3D.Add(touchArea);
			touchArea = null;
		}

		#endregion


		public void Update()
        {
            if (!Enabled)
                return;
			
            var touches = Input.touches;

            int i;

#if UNITY_EDITOR || UNITY_STANDALONE
			ProcessTouch(GetMouseTouch());//This means I can use mouse touch as input at editor time
#endif
			//Update the state of the touch for a touch area
			for (i = 0; i < touches.Length; i++)
            {
                ProcessTouch(touches[i]);
            }

            //Any other general updating that may be required
            for (i = 0; i < TouchAreas.Count; i++)
            {
                TouchAreas[i].Update();
            }
			for (i = 0; i < TouchAreas3D.Count; i++)
			{
				TouchAreas3D[i].Update();
			}

        }

#if DEBUG_TOUCH_AREAS

		private Texture2D Red = null;
		private Texture2D Purple = null;
		private Texture2D Green = null;
		private Texture2D Yellow = null;
#endif

		public void OnGUI()
		{
#if DEBUG_TOUCH_AREAS

			// Textures, so we're not making dozens per frame
			if(this.Red == null)
            {
				Red = TextureUtilities.MakeOutline(16, 16, 6, new Color(1, 0, 0));
				Purple = TextureUtilities.MakeOutline(16, 16, 6, new Color(1, 0, 1));
				Green = TextureUtilities.MakeOutline(16, 16, 6, new Color(0, 1, 0));
				Yellow = TextureUtilities.MakeOutline(16, 16, 6, new Color(1, 1, 0));
			}

			foreach (var touchArea in TouchAreas)
			{
				// Ignore inactive objects
				if(touchArea.RelativeTo != null && !touchArea.RelativeTo.gameObject.activeInHierarchy)
                {
					continue;
                }

				// Style dependent on state of the button
				GUIStyle style = new GUIStyle(GUI.skin.box);
				if (!touchArea.LayerEnabled || !touchArea.Enabled)
				{
					style.normal.background = !touchArea.LayerEnabled ? Purple : Red;
				}
				else
				{
					style.normal.background = touchArea.CanvasTouchRect.Contains(CachedTouch.position) ? Yellow : Green;
				}
				style.border = new RectOffset(8, 8, 8, 8);
				
				// Get the correct screen space rect for the UI element
				Rect rect = new Rect(touchArea.TouchRect);
				if(touchArea.RelativeTo != null)
                {
					RectTransform rectTransform = touchArea.RelativeTo;
					rect.width /= ZScreen.WidthScaleFactor;
					rect.height /= ZScreen.HeightScaleFactor;
					rect.width *= rectTransform.localScale.x;
					rect.height *= rectTransform.localScale.y;
					rect.x = rectTransform.position.x;
					rect.y = ZScreen.Height - rectTransform.position.y;
					rect.x -= rectTransform.pivot.x * rect.width;
					rect.y -= rectTransform.pivot.y * rect.height;
				}
				GUI.Box(rect, "", style);
			}
#endif
		}


		private void ProcessTouch(UnityEngine.Touch touch)
        {
			int i;

			bool newTouch2D = false;

			//Process all 2D Touch Areas
            for (i = 0; i < TouchAreas.Count; i++)
            {
                if (touch.phase == TouchPhase.Began)
                {
                    if (TouchAreas[i].FingerID.Equals(-1))
                    {
                        if (TouchAreas[i].TouchTest(touch))
                        {
                            TouchAreas[i].RegisterTouch(touch);

							newTouch2D = true;
                        }
                    }
                }
                else
                {
                    if (TouchAreas[i].FingerID.Equals(touch.fingerId))
                    {
                        TouchAreas[i].UpdateTouch(touch);
                    }
                }
            }

			if(newTouch2D == true)
            {
				return;
            }

			//Process 3D Touch Areas
			if (TouchAreas3D.Count > 0)
			{
				if (touch.phase == TouchPhase.Began)
				{
					Ray cast = Camera.main.ScreenPointToRay(touch.position);

					RaycastHit hit;
					if (Physics.Raycast(cast.origin, cast.direction, out hit, 100))
					{
						for (i = 0; i < TouchAreas3D.Count; i++)
						{
							if (TouchAreas3D[i].TouchTest(hit))
							{
								TouchAreas3D[i].RegisterTouch(touch, hit, Camera.main);
								return;
							}
						}
					}
				}
				else
				{
					for (i = 0; i < TouchAreas3D.Count; i++)
					{
						if (TouchAreas3D[i].FingerID.Equals(touch.fingerId))
						{
							TouchAreas3D[i].UpdateTouch(touch);
						}
					}
				}
			}
		}

		// Cancels all touches but the exception listed
		public void CancelTouchesExcept(TouchArea touchArea)
		{
			for (int i = 0; i < TouchAreas.Count; i++)
			{
				if (touchArea != TouchAreas[i])
				{
					TouchAreas[i].CancelTouch();
				}
			}
		}
		
		public void CancelTouches(TouchArea[] touchAreas)
		{
			for (int i = 0; i < touchAreas.Length; i++)
			{
				touchAreas[i].CancelTouch();
			}
		}

		public void CancelTouches(string layer, params TouchArea[] ignore)
		{
			List<TouchArea> touchAreas = new List<TouchArea>(TouchAreasOfLayer(layer));
			for (int i = 0; i < ignore.Length; i++)
			{
				touchAreas.Remove(ignore[i]);
			}
			CancelTouches(touchAreas.ToArray());
		}

        public void CancelAll()
        {
            for (int i = 0; i < TouchAreas.Count; i++)
            {
                TouchAreas[i].CancelTouch();
            }
		}

#region LAYER MANAGEMENT

		private List<string> IgnoredLayers = new List<string>();

		public bool IsLayerEnabled(string layer)
        {
			return !IgnoredLayers.Contains(layer);
        }

		//Enables a single layer of TouchAreas
		public void EnableLayer(string layer)
		{
			if (IgnoredLayers.Contains(layer))
			{
				IgnoredLayers.Remove(layer);

				for (int i = 0; i < TouchAreas.Count; i++)
				{
					if (TouchAreas[i].Layer.Equals(layer))
					{
						TouchAreas[i].LayerEnabled = true;
					}
				}
			}

		}

		// Enables all layers at once
		public void EnableAllLayers()
		{
			string[] ignoredLayers = IgnoredLayers.ToArray();
			foreach (string layer in ignoredLayers)
			{
				EnableLayer(layer);
			}
		}

		//Disables a single layer of TouchAreas
		public void DisableLayer(string layer)
		{
			if (!IgnoredLayers.Contains(layer))
			{
				IgnoredLayers.Add(layer);

				for (int i = 0; i < TouchAreas.Count; i++)
				{
					if (TouchAreas[i].Layer.Equals(layer))
					{
						TouchAreas[i].LayerEnabled = false;
					}
				}
			}

		}

		//Disables all layers except for one
		public void IsolateLayer(string layer)
		{
			for (int i = 0; i < TouchAreas.Count; i++)
			{
				if (!TouchAreas[i].Layer.Equals(layer) && !string.IsNullOrEmpty(TouchAreas[i].Layer))
				{
					DisableLayer(TouchAreas[i].Layer);	
				}
			}
			EnableLayer(layer);
		}

		public TouchArea[] TouchAreasOfLayer(string layer)
		{
			List<TouchArea> touchAreas = new List<TouchArea>();
			for (int i = 0; i < TouchAreas.Count; i++)
			{
				if (TouchAreas[i].Layer == layer)
				{
					touchAreas.Add(TouchAreas[i]);
				}
			}
			
			return touchAreas.ToArray();
		}

#endregion


#region EDITOR TOUCH INJECTION

		public void InsertTouch(Vector2 position, TouchPhase phase)
		{
			UnityEngine.Touch fakeTouch = new UnityEngine.Touch();
			fakeTouch.fingerId = -3;
			fakeTouch.position = position;
			fakeTouch.phase = phase;

			ProcessTouch(fakeTouch);
		}

		private UnityEngine.Touch CachedTouch = new UnityEngine.Touch()
		{
			phase = TouchPhase.Canceled
		};

		public UnityEngine.Touch GetMouseTouch()
		{
			UnityEngine.Touch mouseTouch = new UnityEngine.Touch();
			mouseTouch.fingerId = -2;
			mouseTouch.position = Input.mousePosition;
			mouseTouch.phase = CachedTouch.phase;

			switch(CachedTouch.phase)
            {
				case TouchPhase.Ended:
				case TouchPhase.Canceled:
					
					if (Input.GetMouseButtonDown(0))
					{
						mouseTouch.phase = TouchPhase.Began;
					}

					break;

				case TouchPhase.Stationary:
				case TouchPhase.Began:
				case TouchPhase.Moved:
					
					if (Input.GetMouseButtonUp(0))
					{
						mouseTouch.phase = TouchPhase.Ended;
					}
					else if (Mathf.Abs(Input.GetAxis("Mouse X")) > 0 || Mathf.Abs(Input.GetAxis("Mouse Y")) > 0)
					{
						mouseTouch.phase = TouchPhase.Moved;
					}
					else
                    {
						mouseTouch.phase = TouchPhase.Stationary;
                    }

					break;
            }

			CachedTouch = mouseTouch;

            return mouseTouch;
        }

#endregion
    }
}