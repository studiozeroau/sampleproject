﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Zeroes.Managers;

namespace Zeroes.Touch
{

	public class TouchArea : Touchable
    {
        //The rect on the screen used for defining the touch area
        public Rect TouchRect { private set; get; }

        // An internal touch rect in canvas space
        private Rect CanvasTouchRect;

        //A transform the touch area rect may be relative to, so it can be moved
        protected RectTransform RelativeTo = null;

        //Events for touch area
        public Action<TouchArea> OnTouchDown;
        public Action<TouchArea> OnTouchMove;
        public Action<TouchArea> OnTouchUp;
        public Action<TouchArea> OnTouchCancel;

        //The origin of the touch
        public Vector2 TouchOrigin { private set; get; }

        //The current touch position on the canvas
        public Vector2 TouchPosition { private set; get; }

        // The current touch position in the rect
        public Vector2 LocalTouchPosition { private set; get; }

        //The distance from the the touch has moved in the last frame
        public Vector2 MoveDistance { private set; get; }

        //The total distance the touch has moved from the origin
        public Vector2 TotalMoveDistance { private set; get; }

        //A list of touch areas that have precdence over this
        private List<TouchArea> _GivesPrecedenceTo = new List<TouchArea>();

		//Creates a touch area on the screen based on a simple rect
		public TouchArea(Rect touchRect, string layer = "Default") : base(layer)
        {
            TouchRect = touchRect;

            GameManager.Instance.TouchManager.AddTouchArea(this);
        }

        //Creates a touch area based on a ui element
		public TouchArea(RectTransform uiElement, string layer = "Default") : base(layer)
        {
            TouchRect = uiElement.rect;
            RelativeTo = uiElement;

            GameManager.Instance.TouchManager.AddTouchArea(this);
        }

        public override void Remove()
        {
            GameManager.Instance.TouchManager.RemoveTouchArea(this);
        }

        public bool TouchTest(UnityEngine.Touch touch)
        {
			if (!Enabled || !LayerEnabled)
                return false;
            
            Vector2 touchPosition = ZScreen.ScreenToCanvas(touch.position);

			bool result = Contains(touchPosition);

			if (result == true)
			{
				if (_GivesPrecedenceTo.Count > 0)
				{
					for (int i = 0; i < _GivesPrecedenceTo.Count; i++)
					{
						if (_GivesPrecedenceTo[i].Contains(touchPosition))
						{
							return false;
						}
					}
				}
			}

			return result;
        }

        public bool Contains(Vector2 position)
        {
            //This is potentially a bit messy. Unsure if there's a better approach
            if (RelativeTo != null)
            {
                if (!RelativeTo.gameObject.activeInHierarchy)
                {
                    return false;
                }

                UpdateCanvasTouchRectIfRequired();
                
                return CanvasTouchRect.Contains(position);
            }

            return TouchRect.Contains(position);
        }

        // Returns whether the position is within the horizontal space of the rect
        public bool ContainsHorizontal(Vector2 position)
        {
            UpdateCanvasTouchRectIfRequired();

            return position.x > CanvasTouchRect.x && position.x < CanvasTouchRect.x + CanvasTouchRect.width;
        }

        // Returns whether the position is within the vertical space of the rect
        public bool ContainsVertical(Vector2 position)
        {
            UpdateCanvasTouchRectIfRequired();

            return position.y > CanvasTouchRect.y && position.y < CanvasTouchRect.y + CanvasTouchRect.height;
        }

        private void UpdateCanvasTouchRectIfRequired()
        {
            // Only ever update this rect when the transform has changed
            if (RelativeTo.hasChanged == true)
            {
                Vector3 offset = RelativeTo.position;
                offset = ZScreen.ScreenToCanvas(offset);

                CanvasTouchRect = new Rect(
                    (TouchRect.x * RelativeTo.localScale.x) + offset.x,
                    (TouchRect.y * RelativeTo.localScale.y) + offset.y,
                    TouchRect.width * RelativeTo.localScale.x,
                    TouchRect.height * RelativeTo.localScale.y);

                RelativeTo.hasChanged = false;
            }
        }

		protected override void DeRegisterTouch()
        {
			base.DeRegisterTouch();

            TouchOrigin = Vector2.zero;
            TouchPosition = Vector2.zero;
            TotalMoveDistance = Vector2.zero;
            MoveDistance = Vector2.zero;
        }

        public override void CancelTouch()
		{
			if (FingerID != -1)
			{
				if (OnTouchCancel != null)
				{
					OnTouchCancel(this);
				}
				DeRegisterTouch();
			}
        }

        public override void UpdateTouch(UnityEngine.Touch touch)
        {
            Vector2 touchPosition = ZScreen.ScreenToCanvas(touch.position);
            
            LocalTouchPosition = new Vector2(touchPosition.x - CanvasTouchRect.x, touchPosition.y - CanvasTouchRect.y);

            if (touch.phase == TouchPhase.Began)
            {
                TouchOrigin = touchPosition;

                if (OnTouchDown != null)
                {
                    OnTouchDown(this);
                }
            }
            else if (touch.phase == TouchPhase.Moved)
            {
                MoveDistance = touchPosition - TouchPosition;

                TotalMoveDistance = TouchPosition - TouchOrigin;

                if (OnTouchMove != null)
                {
                    OnTouchMove(this);
                }
            }
            else if (touch.phase == TouchPhase.Ended)
            {
                if (OnTouchUp != null)
                {
                    OnTouchUp(this);
                }
                DeRegisterTouch();
            }
            else if (touch.phase == TouchPhase.Canceled)
            {
                CancelTouch();
            }

            TouchPosition = touchPosition;
        }

        public void GivePredenceTo(TouchArea touchArea)
        {
            if (!_GivesPrecedenceTo.Contains(touchArea))
            {
                _GivesPrecedenceTo.Add(touchArea);
            }
        }
        public void RemovePredence(TouchArea touchArea)
        {
            _GivesPrecedenceTo.Remove(touchArea);
        }

    }

}