﻿using System.Collections;

namespace Zeroes.Touch
{
	public class Touchable
	{
		public int FingerID { get; protected set; }

        //Is this touch area enabled
		private bool _Enabled = true;
		public bool Enabled
		{
			get
			{
				return _Enabled;
			}
			set
			{
				if (_Enabled != value)
				{
					_Enabled = value;
					if (_Enabled == false)
					{
						CancelTouch();
					}
				}
			}
		}

		//The layer this TouchArea belongs to
		public string Layer = "";
		private bool _LayerEnabled = true;
		public bool LayerEnabled
		{
			get
			{
				return _LayerEnabled;
			}
			set
			{
				if (_LayerEnabled != value)
				{
					_LayerEnabled = value;
					if (_LayerEnabled == false)
					{
						CancelTouch();
					}
				}
			}
		}

		public Touchable(string layer)
		{
			FingerID = -1;
			Layer = layer;
		}

		public virtual void Update() { }
		public virtual void UpdateTouch(UnityEngine.Touch touch) { }

		public virtual void RegisterTouch(UnityEngine.Touch touch) 
		{
            FingerID = touch.fingerId;

			UpdateTouch(touch);
		}
		protected virtual void DeRegisterTouch() 
		{
			FingerID = -1;
		}

		public virtual void CancelTouch() { }
		public virtual void Remove() { }
	}
}