﻿using System;
using System.Collections;

using UnityEngine;

using Zeroes.Managers;

namespace Zeroes.Touch
{
	public class TouchArea3D : Touchable
	{
		// Callback
		public Action<TouchArea3D> OnTouchDown;
		public Action<TouchArea3D> OnTouchMove;
		public Action<TouchArea3D> OnTouchUp;
		public Action<TouchArea3D> OnTouchCancel;

		// The area where the touch can hit
		private Collider TouchBounds;

		// The camera that determined the ray
		private Camera RaycastCamera;
		
		//World equivalents to the above
		public Vector3 TouchOrigin { private set; get; }
		public Vector3 TouchPosition { private set; get; }
		public Vector3 MoveDistance { private set; get; }
		public Vector3 TotalMoveDistance { private set; get; }

		//Track movement in world space using the object's x/y/z as the origin position and translate across its plane
		public TouchArea3D(Collider collider, string layer = "Default") : base(layer)
		{
			TouchBounds = collider;

			GameManager.Instance.TouchManager.AddTouchArea(this);
		}

		public bool TouchTest(RaycastHit raycastHit)
		{
			if (!FingerID.Equals(-1) || !Enabled)
			{
				return false;
			}

			if (raycastHit.collider.Equals(TouchBounds))
			{
				return true;
			}

			return false;
		}

		protected override void DeRegisterTouch()
		{
			base.DeRegisterTouch();

			TouchOrigin = Vector3.zero;
			TouchPosition = Vector3.zero;
			TotalMoveDistance = Vector3.zero;
			MoveDistance = Vector3.zero;
		}

		public void RegisterTouch(UnityEngine.Touch touch, RaycastHit hit, Camera raycastCamera)
		{
			TouchOrigin = hit.point;
			RaycastCamera = raycastCamera;
			RegisterTouch(touch);
		}

		public override void UpdateTouch(UnityEngine.Touch touch)
		{
			//Get distance from touch origin relative to camera and project it along the camera's forward plane
			float dist = Vector3.Project(TouchOrigin - RaycastCamera.transform.position, RaycastCamera.transform.forward).magnitude;

			//Get the touch position
			Vector3 touchPosition = RaycastCamera.ScreenToWorldPoint(new Vector3(touch.position.x, touch.position.y, dist));

			/*
			 * Leaving this here in case I ever need to debug this
			 * 
			//Draw the plane we're expecting to move along
			Debug.DrawLine(touchPosition - (RaycastCamera.transform.right * 1000), touchPosition + (RaycastCamera.transform.right * 1000), Color.yellow);
			Debug.DrawLine(touchPosition - (RaycastCamera.transform.up * 1000), touchPosition + (RaycastCamera.transform.up * 1000), Color.yellow);

			//Draw the origin
			Debug.DrawLine(RaycastCamera.transform.position, TouchOrigin, Color.green);
			Debug.DrawLine(TouchOrigin + Vector3.up, TouchOrigin - Vector3.up, Color.green);
			Debug.DrawLine(TouchOrigin + Vector3.right, TouchOrigin - Vector3.right, Color.green);

			//Draw the current touch position
			Debug.DrawLine(RaycastCamera.transform.position, touchPosition, Color.red);
			Debug.DrawLine(touchPosition + Vector3.up, touchPosition - Vector3.up, Color.red);
			Debug.DrawLine(touchPosition + Vector3.right, touchPosition - Vector3.right, Color.red);
			*/

			if (touch.phase == TouchPhase.Began)
			{
				TouchOrigin = touchPosition;

				OnTouchDown?.Invoke(this);
			}
			else if (touch.phase == TouchPhase.Moved)
			{
				MoveDistance = touchPosition - TouchPosition;
                TotalMoveDistance = TouchPosition - TouchOrigin;

				OnTouchMove?.Invoke(this);
			}
			else if (touch.phase == TouchPhase.Ended)
			{
				OnTouchUp?.Invoke(this);

				DeRegisterTouch();
			}
			else if (touch.phase == TouchPhase.Canceled)
			{
				CancelTouch();
			}

			TouchPosition = touchPosition;
		}

		public override void Remove()
		{
			GameManager.Instance.TouchManager.RemoveTouchArea(this);
		}

        public override void CancelTouch()
        {
            base.CancelTouch();

			OnTouchCancel?.Invoke(this);
        }
    }
}