﻿using System;

using UnityEngine;

using Zeroes;

namespace Zeroes.Touch
{
    public class ScrollArea : TouchArea
    {
        public delegate void ScrollAreaAction(ScrollArea scrollArea);
		
        public ScrollAreaAction OnScrollStart;
        public ScrollAreaAction OnScroll;
        public ScrollAreaAction OnScrollEnded;
        public ScrollAreaAction OnScrollCancelled;

        public Rect FullScrollBounds
        {
            get
            {
                return new Rect()
                {
                    x = ScrollBounds.x,
                    y = ScrollBounds.y,
                    width = ScrollBounds.width + TouchRect.width,
                    height = ScrollBounds.height + TouchRect.height
                };
            }
        }
        public Rect ScrollBounds { private set; get; }

        //The amount of scrolling in the last frame
        public Vector2 ScrollDistance;

		// Position for a rect transform
		public Vector2 ScrollPosition
		{
			get
			{
				return new Vector2()
				{
					x = ScrollBounds.x + ScrollAmount.x,
					y = ScrollBounds.y + ScrollAmount.y
				};
			}
		}

		// The actual scroll amount from 0 - width/height
		public Vector2 ScrollAmount;

        // Gets normalized (0-1) of the scroll amount of the scroll area
        public Vector2 ScrollAmountNormalized
        {
            get
            {
                Vector2 nrm = Vector2.zero;
                if(OutsideTop())
                {
                    nrm.y = ScrollAmount.y / FullScrollBounds.height;
                }
                else if (OutsideBottom())
                {
                    nrm.y = (ScrollAmount.y + TouchRect.height) / FullScrollBounds.height;
                }
                else
                {
                    if (ScrollBounds.height > 0)
                    {
                        nrm.y = ScrollAmount.y / ScrollBounds.height;
                    }
                }

                if(OutsideLeft())
                {
                    nrm.x = -ScrollAmount.x / ScrollBounds.width;
                }
                else if (OutsideRight())
                {
                    nrm.x = -(ScrollAmount.x - TouchRect.width)  / ScrollBounds.width;
                }
                else
                {
                    if(ScrollBounds.width > 0)
                    {
                        nrm.x = -ScrollAmount.x / ScrollBounds.width;
                    }
                }

                return nrm;
            }
        }

        public Vector2 Velocity;

        public float ScrollThreshold = 5f;

        private float Epsilon = .25f;

        public ScrollArea(Rect view, string layer = "Default") : base(view, layer)
        {
            SetupEvents();
        }

        public ScrollArea(RectTransform uiElement, string layer = "Default") : base(uiElement, layer)
        {
            SetupEvents();
        }

        public void DefineBounds(Rect bounds)
        {
            ScrollBounds = new Rect(
				bounds.x, 
				bounds.y, 
				bounds.width - TouchRect.width, 
				bounds.height - TouchRect.height
			);

            ScrollDistance.x = ScrollDistance.y = 0;
            ScrollAmount.x = ScrollAmount.y = 0;
        }

		public void DefineBounds(RectTransform bounds)
		{
            DefineBounds(new Rect(
				bounds.anchoredPosition.x,
				bounds.anchoredPosition.y,
				bounds.rect.width,
				bounds.rect.height
			));
		}

        public void ScrollTo(Vector2 scrollPosition)
		{
            CancelScroll();
            ScrollAmount = scrollPosition;
            OnScroll?.Invoke(this);
        }

		
        public override void Update()
        {
            base.Update();

            //Do NOT do any updates while this is actively being touched
            if (FingerID != -1)
                return;

            //Handle X rubber banding and velocity smoothing
            float xMoveDist = 0;
            if (Mathf.Abs(Velocity.x) > .1f)
            {
                if (!OutsideHorizontal())
                {
                    xMoveDist = (Velocity.x * 3) * Time.unscaledDeltaTime;
                    Velocity.x -= xMoveDist;
                }
                else
                {
                    Velocity.x /= 3;//TODO. Not using Delta. Is this ok??
                }

                xMoveDist = Velocity.x;
            }
            else
            {
                Velocity.x = 0;

                if (OutsideLeft())
                {
                    xMoveDist = -ScrollBounds.width;
                }
                else if (OutsideRight())
                {
                    xMoveDist = 0;
                }
                else
                {
                    //Set this to the position because the resulting maths below will end up with 0 x everything else
                    xMoveDist = ScrollAmount.x;
                }

                //Smooth damp it within epsilon
                if (Mathf.Abs(xMoveDist - ScrollAmount.x) < Epsilon)
                {
					xMoveDist = 0;
                }
                else
                {
					xMoveDist = -Time.unscaledDeltaTime * ((ScrollAmount.x - xMoveDist) / .15f);
                }
            }

            //Handle Y rubber banding and velocity smoothing
            float yMoveDist = 0;
            if (Mathf.Abs(Velocity.y) > .1f)
            {
                if (!OutsideVertical())
                {
                    yMoveDist = (Velocity.y * 3) * Time.unscaledDeltaTime;
                    Velocity.y -= yMoveDist;
                }
                else
                {
                    Velocity.y /= 3;//TODO. Not using Delta. Is this ok??
                }

                yMoveDist = Velocity.y;
            }
            else
            {
                Velocity.y = 0;

                if (OutsideBottom())
                {
                    yMoveDist = ScrollBounds.height;
                }
                else if (OutsideTop())
                {
                    yMoveDist = 0;
                }
                else
                {
                    //Set this to the position because the resulting maths below will end up with 0 x everything else
                    yMoveDist = ScrollAmount.y;
                }

                //Smooth it within epsilon
                if (Mathf.Abs(yMoveDist - ScrollAmount.y) < Epsilon)
                {
					yMoveDist = 0;
                }
                else
                {
                    yMoveDist = -Time.unscaledDeltaTime * ((ScrollAmount.y - yMoveDist) / .15f);
                }
            }
			
            if (xMoveDist == 0 && yMoveDist == 0)
            {
                return;
            }

            ScrollDistance.x = xMoveDist;
            ScrollDistance.y = yMoveDist;

            ScrollAmount += ScrollDistance;

            if (OnScroll != null)
            {
				OnScroll(this);
            }
        }

		public void CancelScroll()
		{
			ScrollDistance.x = 0;
			ScrollDistance.y = 0;

			Velocity.x = 0;
			Velocity.y = 0;
		}


        #region TOUCH AREA FUNCS

        private void SetupEvents()
        {
            OnTouchDown += TouchDown;
            OnTouchUp += TouchUp;
            OnTouchCancel += TouchCancel;
        }

        private void TouchDown(TouchArea touchArea)
        {
            Velocity.x = 0;
            Velocity.y = 0;

			OnTouchMove += CheckForThreshold;
        }

		//Make sure we're not doing any actual scrolling until the threshold is met
		//Also only call onScrollStart once
		private void CheckForThreshold(TouchArea touchArea)
		{
			if (touchArea.TotalMoveDistance.magnitude < ScrollThreshold)
			{
				return;
			}
			else
			{	
				OnTouchMove -= CheckForThreshold;
				OnTouchMove += TouchMove;

				if (OnScrollStart != null)
				{
					OnScrollStart(this);
				}

				if (OnTouchMove != null)
				{
					OnTouchMove(this);
				}
			}
		}

		//Perform the actual scroll action
        private void TouchMove(TouchArea touchArea)
        {
            float xScale = 1;
            float yScale = 1;
            float outsideScalar = 2.25f;

			//TODO. Maybe revisit the below math at some point

            //Scale X movement by how much it's outside the view rect
			if (OutsideLeft() || OutsideRight())
			{
				xScale = Mathf.Abs(ScrollAmount.x + ScrollBounds.width * .5f);
				xScale -= (ScrollBounds.width * .5f);
				xScale /= TouchRect.width / outsideScalar;
				xScale = 1 - xScale;
			}

			//Scale Y movement by how much it's outside the view rect
			if (OutsideBottom() || OutsideTop())
			{
				yScale = Mathf.Min(Mathf.Abs(ScrollAmount.y), Mathf.Abs(ScrollAmount.y - ScrollBounds.height));
				yScale /= TouchRect.height / outsideScalar;
				yScale = 1 - yScale;
			}

            ScrollDistance.x = touchArea.MoveDistance.x * xScale;
            ScrollDistance.y = touchArea.MoveDistance.y * yScale;

            Velocity = ScrollDistance;

            ScrollAmount += ScrollDistance;

            if (OnScroll != null)
            {
                OnScroll(this);
            }
        }

        private void TouchUp(TouchArea touchArea)
        {
			OnTouchMove -= CheckForThreshold;
			OnTouchMove -= TouchMove;

            ScrollDistance.x = 0;
            ScrollDistance.y = 0;

            if (OutsideHorizontal())
            {
                Velocity.x = 0;
            }
            if (OutsideVertical())
            {
                Velocity.y = 0;
            }

            if (OnScrollEnded != null)
            {
                OnScrollEnded(this);
            }
        }

        private void TouchCancel(TouchArea touchArea)
        {
			OnTouchMove -= CheckForThreshold;
			OnTouchMove -= TouchMove;

            if (OnScrollCancelled != null)
            {
                OnScrollCancelled(this);
			}
        }

        #endregion



        #region SCROLL BOUNDS TESTS

        public bool OutsideLeft()
        {
            if (ScrollAmount.x < -ScrollBounds.width)
                 return true;
            
            return false;
        }

        public bool OutsideRight()
        {
            if (ScrollAmount.x > 0)
                return true;

            return false;
        }

        public bool OutsideHorizontal()
        {
            if (OutsideLeft() || OutsideRight())
                return true;

            return false;
        }


        public bool OutsideTop()
        {
            if (ScrollAmount.y < 0)
                return true;
            
            return false;
        }

        public bool OutsideBottom()
        {
            if (ScrollAmount.y > ScrollBounds.height)
                return true;
            
            return false;
        }

        public bool OutsideVertical()
        {
            if (OutsideBottom() || OutsideTop())
                return true;

            return false;
        }

        public bool OutOfBounds()
        {
            if (OutsideHorizontal())
                return true;

            if (OutsideVertical())
                return true;

            return false;
        }

        #endregion
    }
}
