﻿using System;
using System.Collections;

using UnityEngine;

using Zeroes.Managers;

namespace Zeroes.Touch
{
	public class TouchableMesh : MonoBehaviour
	{
		public TouchArea3D TouchArea { private set; get; }

		// Callbacks
		public Action<TouchArea3D> OnTouchDown { get { return TouchArea.OnTouchDown; } set { TouchArea.OnTouchDown = value; } }
		public Action<TouchArea3D> OnTouchMove { get { return TouchArea.OnTouchMove; } set { TouchArea.OnTouchMove = value; } }
		public Action<TouchArea3D> OnTouchUp { get { return TouchArea.OnTouchUp; } set { TouchArea.OnTouchUp = value; } }
		public Action<TouchArea3D> OnTouchCancel { get { return TouchArea.OnTouchCancel; } set { TouchArea.OnTouchCancel = value; } }

		public string Layer = "Default";

		public virtual void Awake()
		{
			TouchArea = new TouchArea3D(GetComponent<Collider>(), Layer);
		}

		public virtual void OnDestroy()
		{
			TouchArea.Remove();
		}
	}
}