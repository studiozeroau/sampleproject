﻿using UnityEngine;
using UnityEngine.SceneManagement;

using System;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;

using Zeroes;
using Zeroes.Managers;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Zeroes
{
    [Serializable]
    public class InspectorScene : IEquatable<InspectorScene>
    {
#if UNITY_EDITOR
        [SerializeField] private SceneAsset _sceneAsset;
#endif
        [SerializeField] public string SceneName;

        public bool Equals(InspectorScene other)
        {
            return SceneName.Equals(other.SceneName);
        }
    }

    public class SceneRequirements : MonoBehaviour
    {
        [SerializeField, EditorReadOnly, Tooltip("Read - only Project Configuration object.There should only ever be one of these in the project, as it is the source for all Configuration / Asset Mapping information for the project")]
        private ProjectConfiguration _projectConfiguration;

        [SerializeField, Tooltip("To be loaded in sequence on Application startup. The current scene this GameObject belongs to is automatically added to the list on startup.")]
        private List<InspectorScene> Scenes = new List<InspectorScene>();

        protected GameManager App;

        public void Reset()
        {
            _projectConfiguration = ProjectConfiguration.Find();
        }

        // Initialise GameManager in Awake
        // Destroy if one already exists so the GameObject does not continue to Start
        public void Awake()
        {
            //If a GameManager doesn't exist, perform the necessary setup
            if (GameManager.Instance == null)
            {
                //Create a GameManager because we need one of these
                App = InitGameManager();
            }
            else
            {
                Destroy(this);
            }
        }

        //Perform the necessary loading
        public void Start()
        {
            StartCoroutine(Load());
        }

        //Loading is done in sequence in a coroutine to ensure all the necessary scenes are loaded in sequence
        private IEnumerator Load()
        {
			// GameManager can define essentially static screens to launch
			foreach (var t in App.RequiredScenesOnLaunch)
            {
                Scenes.Add(new InspectorScene()
                {
                    SceneName = t.ToString()
                });
			}
			App.RequiredScenesOnLaunch.Clear();

            // Add current scene to list of required scenes when required
            InspectorScene thisScene = new InspectorScene() { SceneName = gameObject.scene.name };
			if (!Scenes.Contains(thisScene))
			{
                App.SceneLoader.EntryPoint = thisScene.SceneName;
                Scenes.Add(thisScene);
			}

            // Load any of the required scenes
            foreach (var scene in Scenes)
            {
                Debug.Log(scene.SceneName);
                GameScene sceneToLoad = SceneLoader.CreateFromID(scene.SceneName);
				if (sceneToLoad != null)
				{
					App.SceneLoader.OpenScene(sceneToLoad);

					yield return new WaitUntil(() => sceneToLoad.IsSceneReady);
				}
            }

			// Always set Main as the active scene when applicable
			GameScene main = App.SceneLoader.GetScene<Main>(typeof(Main));
			if (main != null)
			{
				SceneManager.SetActiveScene(main.Scene);	
			}

            DontDestroyOnLoad(this);
        }
        
        protected virtual GameManager InitGameManager()
        {
            if(_projectConfiguration != null)
            {
                Type gmType = Type.GetType(_projectConfiguration.GameManager);
                if(gmType != null)
                {
                    GameManager gameManager = gameObject.AddComponent(gmType) as GameManager;
                    gameManager.Configure(_projectConfiguration);
                    return gameManager;
                }
            }

			return gameObject.AddComponent<GameManager>();
        }
        
	}
}