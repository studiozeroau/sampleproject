﻿using System;
using System.Collections;
using System.Linq;
using System.IO;
using System.Collections.Generic;

using UnityEditor;
using UnityEngine;
using UnityEditor.SceneManagement;

using Zeroes.Editor;

namespace Zeroes.Scene.Editor
{
	public class SceneUtilities
	{       
		// Creates a generic SceneRequirements
		[MenuItem("GameObject/Zeroes/Scene/SceneRequirements", priority = 10)]
		public static SceneRequirements CreateSceneRequirements()
		{
			GameObject newSceneRequirements = Utilities.NewGameObject();
			newSceneRequirements.name = "SceneRequirements";
			return newSceneRequirements.AddComponent<SceneRequirements>();
		}

		[MenuItem("Assets/Create/Zeroes/New Scene", false, 0)]
		private static void CreateZeroesScene()
		{
			// Get the appropriate icon to show in the editor
			Texture2D icon = EditorGUIUtility.IconContent("SceneAsset Icon").image as Texture2D;

			// Callback to finish creating the Scene once name input has completed
            EndNameEditActionCallback callback = ScriptableObject.CreateInstance<EndNameEditActionCallback>();
            callback.OnComplete += (int instanceID, string pathName, string resourceFile) =>
            {
                // Create the scene
                UnityEngine.SceneManagement.Scene newScene = EditorSceneManager.NewScene(NewSceneSetup.DefaultGameObjects, NewSceneMode.Single);

                // Add a SceneRequirements to it
                SceneRequirements requirements = SceneUtilities.CreateSceneRequirements();

                // Save the scene to the project
                EditorSceneManager.SaveScene(newScene, pathName);
            };
            
            // Start the process of creating a new Scene
            ProjectWindowUtil.StartNameEditingIfProjectWindowExists(0, callback, Utilities.GetCurrentProjectWindowPath() + "/New Scene.unity", icon, (string)null);
		}

        // Gets all the scenes in the project
        public static string[] GetAllScenesInProject(bool fullPath = false)
        {
            string[] temp = FindAllScenes();
            string[] scenes = new string[temp.Length];

            // 2 iterations. 1 to find main, 2 to find rest
            for(int i = 0; i < 2; i++)
            {
                for (int j = 0; j < temp.Length; j++)
                {
                    string sceneName = temp[j].Split('/').LastElement().Replace(".unity", "");
                    if (sceneName.Equals("Main") && i == 0)
                    {
                        scenes[0] = fullPath ? temp[j] : sceneName;
                        temp[j] = "";
                        break;
                    }
                    else
                    {
                        scenes[j <= temp.IndexOf("") ? j + 1 : j] = fullPath ? temp[j] : sceneName;
                    }
                }
            }

            return scenes;
        }

        public static string[] FindAllScenes()
        {
            string[] guids = AssetDatabase.FindAssets("t:Scene", new string[] { $"Assets/" });
            string[] scenePaths = new string[guids.Length];
            for(int i = 0; i <guids.Length; i++)
            {
                scenePaths[i] = AssetDatabase.GUIDToAssetPath(guids[i]);
            }

            // Sort alphabetically
            Array.Sort(scenePaths, (x, y) => string.Compare(x.Split('/').LastElement(), y.Split('/').LastElement()));

            return scenePaths;
        }
    }
}
