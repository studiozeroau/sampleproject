﻿using System.Collections.Generic;

using UnityEngine;
using UnityEditor;

using Zeroes;
using Zeroes.UI;

[CustomEditor(typeof(SceneRequirements))]
public class SceneRequirementsEditor : Editor
{
    private SerializedProperty _projectConfiguration;
    private SerializedProperty _scenes;

    public void OnEnable()
    {
        _projectConfiguration = serializedObject.FindProperty("_projectConfiguration");
        _scenes = serializedObject.FindProperty("Scenes");
    }

    public override void OnInspectorGUI()
    {
        GUIStyle header = new GUIStyle(EditorStyles.boldLabel);

        EditorGUILayout.Space();
        EditorGUILayout.LabelField("Project Configuration", header);

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.PropertyField(_projectConfiguration);
        if(GUILayout.Button("Find", GUILayout.MaxWidth(50)))
        {
            _projectConfiguration.objectReferenceValue = ProjectConfiguration.Find();
        }
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.Space();
        Editors.DrawSeparator();
        EditorGUILayout.Space();

        EditorGUILayout.LabelField("Startup Scenes", header);

        Editors.DrawArray(_scenes);

        serializedObject.ApplyModifiedProperties();
    }

}


[CustomPropertyDrawer(typeof(InspectorScene))]
public class InspectorSceneDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        GUI.enabled = !Application.isPlaying;
        
        SerializedProperty sceneAsset = property.FindPropertyRelative("_sceneAsset");
        SerializedProperty sceneName = property.FindPropertyRelative("SceneName");

        EditorGUI.BeginChangeCheck();
        EditorGUI.PropertyField(position, sceneAsset);
        if(EditorGUI.EndChangeCheck())
        {
            if (sceneAsset.objectReferenceValue != null)
            {
                sceneName.stringValue = (sceneAsset.objectReferenceValue as SceneAsset).name;
            }
            else
            {
                sceneName.stringValue = "";
            }
        }
    }
}