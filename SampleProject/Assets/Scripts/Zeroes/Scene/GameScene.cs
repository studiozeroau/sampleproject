﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.SceneManagement;

using Zeroes.Managers;
using Zeroes.Touch;

namespace Zeroes
{
    public class GameScene
    {

        #region CONVENIENCE METHODS FOR GAME SCENES

        public GameManager GameManager
        {
            get
            {
                return GameManager.Instance;
            }
        }

        public SceneLoader SceneLoader
        {
            get
            {
                return GameManager.SceneLoader;
            }
        }

        public TouchManager TouchManager
        {
            get
            {
                return GameManager.TouchManager;
            }
        }

		#endregion

        //The ID (Name of the Scene Asset) this Scene Script is linked to
        public string SceneID { protected set; get; }

        //Store a reference to the actual Scene in the Unity game instance
        public Scene Scene { protected set; get; }

        //Is this scene ready for use? Has it been loaded?
        public bool IsSceneReady { private set; get; }
		
		// Was this scene the entry point for the app?
		public bool IsEntryPoint { private set; get; }

		//An Action to call when the Scene is ready if applicable
		public Action OnReady;

        //Cache all Root GameObjects here
        private GameObject[] Root;

        public GameScene()
		{
			SceneID = this.GetType().Name;
            IsEntryPoint = SceneID == SceneLoader.EntryPoint;

        }

        //Overload to allow for opening a generic scene with no specific class
        public GameScene(string sceneID)
        {
			SceneID = sceneID;
            IsEntryPoint = SceneID == SceneLoader.EntryPoint;
        }

        //In any superclass of this class, set the SceneID here
        public virtual void Start()
		{
            GameManager.Instance.StartCoroutine(WaitForSceneReady());
        }

        public virtual void Close()
        {
        }

        // Sets this scene as the main scene
        public virtual void SetAsMain()
        {
            SceneManager.SetActiveScene(Scene);
        }

        //Async Load the scene
        private IEnumerator WaitForSceneReady()
        {
            if (!SceneManager.GetSceneByName(SceneID).isLoaded)
            {
                AsyncOperation asyncSceneLoad = SceneManager.LoadSceneAsync(SceneID, LoadSceneMode.Additive);
                while (!asyncSceneLoad.isDone)
                {
                    yield return null;
                }
            }

            Scene = SceneManager.GetSceneByName(SceneID);
            
            Root = Scene.GetRootGameObjects();

            SceneReady();

            OnReady?.Invoke();

            IsSceneReady = true;
        }

        //First point of entry for scripting in a GameScene
        public virtual void SceneReady() { }

        //Do the necessary updating of non MonoBehaviour elements
        public virtual void Update()
        {
        }

        //Indexer for navigating the root of the scene
        public GameObject this[string id]
        {
            get
            {
                for (int i = 0; i < Root.Length; i++)
                {
                    if (Root[i].name == id)
                    {
                        return Root[i];
                    }
                }
                return null;
            }
        }
        
    }

}
