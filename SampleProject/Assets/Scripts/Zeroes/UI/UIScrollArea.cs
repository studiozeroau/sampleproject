﻿using UnityEngine;
using System.Collections;

using Zeroes.Touch;

namespace Zeroes.UI
{
    public class UIScrollArea : UITouchArea
    {

        public ScrollArea ScrollArea { private set; get; }

        // Scroll thresholds
        public float Threshold = 5f;

        // What direction does this scroll area move?
        public bool ScrollsHorizontally = true;
        public bool ScrollsVertically = true;

        // Public RectTransforms exposed to the Editor
        public RectTransform ScrollViewArea;
        public RectTransform ScrollAreaContent;

        public override void Awake()
        {
            TouchArea = new ScrollArea(ScrollViewArea, Layer);
			ScrollArea = TouchArea as ScrollArea;
            ScrollArea.DefineBounds(ScrollAreaContent);
            ScrollArea.ScrollThreshold = Threshold;
			ScrollArea.OnScroll += Scroll;
		}

        // Scroll method that defines how this scroll area moves
        protected virtual void Scroll(ScrollArea scrollArea)
        {
            Vector2 pos = ScrollAreaContent.anchoredPosition;

            if (ScrollsHorizontally)
            {
                pos.x = scrollArea.ScrollPosition.x;
            }

            if (ScrollsVertically)
            {
                pos.y = scrollArea.ScrollPosition.y;
            }

            ScrollAreaContent.anchoredPosition = pos;
        }

    }
}