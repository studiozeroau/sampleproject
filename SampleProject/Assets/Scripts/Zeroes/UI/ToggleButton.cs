﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityUI = UnityEngine.UI;

using Zeroes.Managers;
using Zeroes;
using System;

namespace Zeroes.UI
{

    public class ToggleButton : Button
    {

        // Callback
        public Action<bool> OnToggleChanged;

        // Active state properties
        private ZImage ActiveState;
        private CanvasGroup ActiveStateCanvasGroup;

        // Button blip properties
        private RectTransform Button;
        private Vector2 ButtonOrigin;

        // TODO. Expose this to the Editor, so it can have a default at editor time too
        [SerializeField]
        private bool _Toggled = false;

        // Code hook, does not trigger animation
        public bool Toggled
        {
            get
            {
                return _Toggled;
            }
            set
            {
                if(value != _Toggled)
                {
                    _Toggled = value;

                    UpdateToggleState();
                }
            }
        }

        public override void Awake()
        {
            base.Awake();

            // Get active state components
            ActiveState = gameObject.Get("Active").GetComponent<ZImage>();
            ActiveStateCanvasGroup = ActiveState.GetComponent<CanvasGroup>();

            // Get button components
            Button = gameObject.Get("Button").GetComponent<RectTransform>();
            ButtonOrigin = Button.anchoredPosition;

            // Toggle press callback
            OnRelease += () =>
            {
                _Toggled = !_Toggled;
                UpdateToggleState();

                OnToggleChanged?.Invoke(_Toggled);
            };

            // On startup, if user has specified in Editor
            if(Toggled == true)
            {
                UpdateToggleState();
            }
        }
        
        private void UpdateToggleState()
        {
            if (_Toggled == true)
            {
                Button.anchoredPosition = new Vector2(-ButtonOrigin.x, ButtonOrigin.y);
                ActiveStateCanvasGroup.alpha = 1;
                
            }
            else
            {
                Button.anchoredPosition = ButtonOrigin;
                ActiveStateCanvasGroup.alpha = 0;
                
            }
        }


        // Do nothing with this, to override the behaviour of a button
        protected override void SetSprite(Sprite sprite) { }

    }

}