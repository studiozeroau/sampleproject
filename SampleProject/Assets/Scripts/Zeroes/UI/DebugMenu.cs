﻿using UnityEngine;

using System;
using System.Collections;
using System.Collections.Generic;

using Zeroes;
using Zeroes.Managers;

public class DebugMenu : MonoBehaviour
{

	// Enum for current state of the debug menu
	private enum DebugMenuState
    {
		Options,
		Log
    }

	// The current state of the debug menu
	private DebugMenuState CurrentState = DebugMenuState.Options;

	// All the buttons we're gonna use
	private List<DebugButton> Buttons = new List<DebugButton>();

	// Style for the buttons
	private GUIStyle Style = null;
	private GUIStyle LogStyle = null;

	// How many columns of buttons are available
	public int Columns = 2;

	// How many rows of buttons are available
	public int Rows = 10;

	// Where is the logger window scrolled to?
	public Vector2 LogScrollPosition = Vector2.zero;

	public virtual void Awake()
	{
		// By default we have a button to disable this menu straight away
		AddButton("Exit", () =>
		{
			enabled = false;
		});

		AddButton("Toggle Log", () =>
		{
			if (CurrentState == DebugMenuState.Log)
			{
				CurrentState = DebugMenuState.Options;
			}
			else
			{
				CurrentState = DebugMenuState.Log;
			}
		});

		// Toggle the FPS Drawer
		AddButton("Toggle FPS Counter", () =>
		{
			FPSDrawer fpsDrawer = gameObject.GetComponent<FPSDrawer>();
			fpsDrawer.enabled = !fpsDrawer.enabled;
		});

	}

	// Disable the global touch manager when this menu is added
	public void OnEnable()
	{
		CurrentState = DebugMenuState.Options;

        GameManager.Instance.TouchManager.Enabled = false;
	}

	// Re enable the global touch manager when this menu is removed
	public void OnDisable()
	{
        GameManager.Instance.TouchManager.Enabled = true;
	}

	public virtual void OnGUI()
	{
		// Width / height of screen
		float height = Screen.height;
		float width = Screen.width;
		
		// How much of the vertical size of the screen can a button use
		float heightPercentage = 1f / Rows;
		float widthPercentage = 1f / Columns;

		// Calc button width / height
		float buttonHeight = height * heightPercentage;
		float buttonWidth = width * widthPercentage;

		// Create the log style
		if (LogStyle == null)
        {
			LogStyle = GUI.skin.label;
			LogStyle.alignment = TextAnchor.UpperLeft;
			LogStyle.fontSize = (int)(height / 75f);
			LogStyle.wordWrap = true;
			LogStyle.border = GUI.skin.button.border;
			LogStyle.normal = GUI.skin.button.normal;
			LogStyle.richText = true;
        }

		// Create the style used for the buttons
		if (Style == null)
		{
			Style = GUI.skin.button;
			Style.alignment = TextAnchor.MiddleCenter;
			Style.fontSize = Screen.height / 50;
			Style.normal.textColor = Color.white;
			Style.wordWrap = true;
		}

		if (CurrentState == DebugMenuState.Log)
		{
			// Draw the close log button
			Rect buttonRect = new Rect(0, 0, buttonWidth, buttonHeight);
			if (GUI.Button(buttonRect, Buttons[1].Title, Style))
			{
				Buttons[1].DoThis?.Invoke();
			}

			// Compile a master string of all out logging
			string log = "";
			foreach (var l in Zebug.Logs)
			{
				log += l.NewLine();
			}

			Vector2 size = LogStyle.CalcSize(new GUIContent(log));

			// Just a dumb HACK to make it darker
			GUI.Label(new Rect(0, buttonHeight, width, height - buttonHeight), "");

			// Begin the scroll view
			LogScrollPosition = GUI.BeginScrollView(new Rect(0, buttonHeight, width, height - buttonHeight), LogScrollPosition, new Rect(0, 0, width, size.y));

			// Draw all the Zebug logs we've done
			GUI.Label(new Rect(0, 0, width, Math.Max(size.y, height - buttonHeight)), log, LogStyle);

			// End the scroll view that we began above.
			GUI.EndScrollView();
		}
		else
		{
			// Add the externally configured buttons
			for (int i = 0; i < Buttons.Count; i++)
			{
				// Determine our column / row
				float column = Mathf.Floor(heightPercentage * i);
				float row = i - column * (1 / heightPercentage);

				Rect buttonRect = new Rect(buttonWidth * column, buttonHeight * row, buttonWidth, buttonHeight);
				if (GUI.Button(buttonRect, Buttons[i].Title, Style))
				{
					Buttons[i].DoThis?.Invoke();
				}
			}

		}

	}

	// Adds a button to the menu
	public virtual void AddButton(string title, Action doThis, string id = null)
	{
		Buttons.Add(new DebugButton()
		{
			Title = title,
			DoThis = doThis,
            ID = id == null ? Buttons.Count.ToString() : id
		});
	}

    // Removes a button from the list
    public virtual bool RemoveButton(string id)
    {
        foreach(var b in Buttons)
        {
            if(b.ID == id)
            {
                Buttons.Remove(b);
                return true;
            }
        }
        return false;
    }


	// Wrapper for content
	private struct DebugButton
	{
		public string Title;
		public Action DoThis;
        public string ID;
	}

}
