using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

[CustomEditor(typeof(UISkew), true)]
public class UISkewEditor : Editor
{
	private SerializedProperty _horizontalAngle;
	private SerializedProperty _verticalAngle;
	
	void OnEnable()
	{
		_horizontalAngle = serializedObject.FindProperty("_horizontalAngle");
		_verticalAngle = serializedObject.FindProperty("_verticalAngle");
	}

	public override void OnInspectorGUI()
    {
	    if(targets.Length > 1)
		    return;
	    
		const float labelWidth = 125;
		
		EditorGUILayout.BeginHorizontal();
		
		EditorGUILayout.BeginVertical();

		{
			// Horizontal angle slider
			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.LabelField("Horizontal Angle", GUILayout.MaxWidth(labelWidth));
			float hAngle = _horizontalAngle.floatValue * Mathf.Rad2Deg;
			hAngle = EditorGUILayout.Slider(hAngle, -90f, 90f, GUILayout.MinWidth(labelWidth));
			hAngle *= Mathf.Deg2Rad;
			_horizontalAngle.floatValue = hAngle;
			if (GUILayout.Button("Reset"))
			{
				_horizontalAngle.floatValue = 0;
			}
			EditorGUILayout.EndHorizontal();
		}
		
		{
			
			// Vertical angle slider
			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.LabelField("Vertical Angle", GUILayout.MaxWidth(labelWidth), GUILayout.MinWidth(labelWidth));
			float vAngle = _verticalAngle.floatValue * Mathf.Rad2Deg;
			vAngle = EditorGUILayout.Slider(vAngle, -90f, 90f, GUILayout.MinWidth(labelWidth));
			vAngle *= Mathf.Deg2Rad;
			_verticalAngle.floatValue = vAngle;
			if (GUILayout.Button("Reset"))
			{
				_verticalAngle.floatValue = 0;
			}
			EditorGUILayout.EndHorizontal();
		}
		
		EditorGUILayout.EndVertical();

		{
			// Reset button
			EditorGUILayout.BeginVertical();
			GUIStyle resetButton = new GUIStyle(GUI.skin.button)
			{
				fixedHeight = EditorGUIUtility.singleLineHeight * 2 + 5
			};
			if (GUILayout.Button(EditorGUIUtility.FindTexture("d_winbtn_mac_close_h@2x"), resetButton))
			{
				_horizontalAngle.floatValue = 0;
				_verticalAngle.floatValue = 0;
			}
			EditorGUILayout.EndVertical();
		}
	    
		EditorGUILayout.EndHorizontal();

		serializedObject.ApplyModifiedProperties();
    }
	
	// Draw the scene handles for the skew box
	public void OnSceneGUI()
	{
		UISkew uiSkew = target as UISkew;
		
		if (uiSkew == null)
			return;

		const string degrees = "°";
		
		RectTransform rectTransform = uiSkew.GetComponent<RectTransform>();
		MaskableGraphic maskableGraphic = uiSkew.MaskableGraphic;
		
		Vector2 pivot = rectTransform.pivot;
		Rect rect = rectTransform.rect;
		float scaleFactor = rectTransform.lossyScale.x;
		
		// Need to convert width and height to the editor screen from the canvas
		float fullWidth = rect.width * scaleFactor;
		float fullHeight = rect.height * scaleFactor;
		float halfWidth = fullWidth * .5f;
		float halfHeight = fullHeight * .5f;
		
		// Get position of object with pivot compensation
		Vector3 position = rectTransform.position;
		position.x += halfWidth * 2 * (.5f - pivot.x);
		position.y += halfHeight * 2 * (.5f - pivot.y);

		// Get the current angle for horizontal and vertical skew
		float hAngle = _horizontalAngle.floatValue;
		float vAngle = _verticalAngle.floatValue;

		float horizontalAngle = 0;
		float verticalAngle = 0;

		foreach (var parent in uiSkew.UISkewParents)
		{
			if (parent == uiSkew)
				continue;
			
			horizontalAngle += parent.HorizontalAngle;
			verticalAngle += parent.VerticalAngle;
		}
		
		// Handle sizes
		float handleSize = 5f * scaleFactor;
		float snap = 0.5f;
		float labelOffset = 25 * scaleFactor;
		
		// Do the skew calculations
		float hSkewWidth = Mathf.Tan(hAngle) * halfHeight;
		float vSkewHeight = Mathf.Tan(vAngle) * halfWidth;

		Matrix4x4 skewMatrix = Matrix4x4.identity;
		skewMatrix.SetColumn(0, new Vector4(1, vAngle + verticalAngle, 0, 0));
		skewMatrix.SetColumn(1, new Vector4(hAngle + horizontalAngle, 1, 0, 0));

		// Create positions for drawing the handles rect
		Vector3 topLeft = new Vector3(-halfWidth, halfHeight);
		Vector3 topRight = new Vector3(halfWidth, halfHeight);
		Vector3 bottomRight = new Vector3(halfWidth, -halfHeight);
		Vector3 bottomLeft = new Vector3(-halfWidth, -halfHeight);

		// Apply the matrix to the points
		topLeft = skewMatrix.inverse.MultiplyPoint3x4(topLeft);
		topRight = skewMatrix.inverse.MultiplyPoint3x4(topRight);
		bottomRight = skewMatrix.inverse.MultiplyPoint3x4(bottomRight);
		bottomLeft = skewMatrix.inverse.MultiplyPoint3x4(bottomLeft);

		// Add UI element position for handles
		topLeft += position;
		topRight += position;
		bottomRight += position;
		bottomLeft += position;

		// Handle positions for the UI
		Vector3 rightHandle = Vector3.Lerp(topRight, bottomRight, .5f);
		Vector3 leftHandle = Vector3.Lerp(topLeft, bottomLeft, .5f);
		Vector3 topHandle = Vector3.Lerp(topLeft, topRight, .5f);
		Vector3 bottomHandle = Vector3.Lerp(bottomLeft, bottomRight, .5f);

		// Draw a rect around the object
		Handles.DrawLine(topLeft, topRight);
		Handles.DrawLine(topRight, bottomRight);
		Handles.DrawLine(bottomRight, bottomLeft);
		Handles.DrawLine(bottomLeft, topLeft);
		
		// Draw each angle
		Handles.Label(rightHandle + new Vector3(labelOffset, labelOffset, 0), string.Concat(Math.Round(vAngle * Mathf.Rad2Deg, 2), degrees));
		Handles.Label(topHandle + new Vector3(labelOffset, labelOffset, 0), string.Concat(Math.Round(hAngle * Mathf.Rad2Deg, 2), degrees));

		// Use y axis colours here
		Handles.color = Handles.yAxisColor;

		// Draw right handle
		{
			EditorGUI.BeginChangeCheck();
			Vector3 changedRightHandle = Handles.FreeMoveHandle(rightHandle, Quaternion.identity, handleSize, Vector3.one * snap, Handles.DotHandleCap);
			if (EditorGUI.EndChangeCheck())
			{
				float change = rightHandle.y - changedRightHandle.y;
				float angle = Mathf.Atan((vSkewHeight + change) / halfWidth);
				vAngle = angle;
				if(maskableGraphic != null)
					maskableGraphic.SetAllDirty();
			}	
		}

		// Draw left handle
		{
			EditorGUI.BeginChangeCheck();
			Vector3 changedLeftHandle = Handles.FreeMoveHandle(leftHandle, Quaternion.identity, handleSize, Vector3.one * snap, Handles.DotHandleCap);
			if (EditorGUI.EndChangeCheck())
			{
				float change = leftHandle.y - changedLeftHandle.y;
				float angle = Mathf.Atan((vSkewHeight - change) / halfWidth);
				vAngle = angle;
				if(maskableGraphic != null)
					maskableGraphic.SetAllDirty();
			}	
		}
		
		// Use x axis colours here
		Handles.color = Handles.xAxisColor;

		// Draw top handle
		{
			EditorGUI.BeginChangeCheck();
			Vector3 changedTopHandle = Handles.FreeMoveHandle(topHandle, Quaternion.identity, handleSize, Vector3.one * snap, Handles.DotHandleCap);
			if (EditorGUI.EndChangeCheck())
			{
				float change = topHandle.x - changedTopHandle.x;
				float angle = Mathf.Atan((hSkewWidth + change) / halfHeight);
				hAngle = angle; 
				if(maskableGraphic != null)
					maskableGraphic.SetAllDirty();
			}
		}

		// Draw bottom handle
		{
			EditorGUI.BeginChangeCheck();
			Vector3 changedBottomHandle = Handles.FreeMoveHandle(bottomHandle, Quaternion.identity, handleSize, Vector3.one * snap, Handles.DotHandleCap);
			if (EditorGUI.EndChangeCheck())
			{
				float change = bottomHandle.x - changedBottomHandle.x;
				float angle = Mathf.Atan((hSkewWidth - change) / halfHeight);
				hAngle = angle;
				if(maskableGraphic != null)
					maskableGraphic.SetAllDirty();
			}
		}

		// Update the serialized values
		_horizontalAngle.floatValue = hAngle;
		_verticalAngle.floatValue = vAngle;

		serializedObject.ApplyModifiedProperties();
	}
}
