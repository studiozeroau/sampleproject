﻿using System;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace Zeroes.UI
{
    public class Editors
    {
		public static void DrawSeparator()
        {
			GUILayout.Box("", new GUILayoutOption[] { GUILayout.ExpandWidth(true), GUILayout.Height(2) });
		}

		/// <summary>
		/// Draws an Array Property with Zeroes editor styling
		/// </summary>
		/// <param name="property"></param>
		public static void DrawArray(SerializedProperty property)
		{
			EditorGUILayout.BeginVertical(EditorStyles.helpBox);

			Texture2D addIcon = EditorGUIUtility.FindTexture("d_CollabCreate Icon");
			Texture2D deleteIcon = EditorGUIUtility.FindTexture("d_CollabDeleted Icon");

			GUIStyle textureButton = new GUIStyle(GUI.skin.button)
			{
				border = new RectOffset(0, 0, 0, 0),
				margin = new RectOffset(2, 0, 2, 0),
				padding = new RectOffset(0, 0, 0, 0),
				fixedHeight = EditorGUIUtility.singleLineHeight,
				fixedWidth = EditorGUIUtility.singleLineHeight
			};

			// Header
			EditorGUILayout.BeginHorizontal();
			GUILayout.Space(12);
			property.isExpanded = EditorGUILayout.Foldout(property.isExpanded, property.displayName) && property.arraySize > 0;
			EditorGUI.BeginChangeCheck();
			property.arraySize = EditorGUILayout.DelayedIntField(property.arraySize, GUILayout.MaxWidth(40));
			if(EditorGUI.EndChangeCheck())
            {
				property.isExpanded = true;
            }
			if(GUILayout.Button(addIcon, textureButton))
            {
				property.InsertArrayElementAtIndex(Mathf.Clamp(property.arraySize - 1, 0, int.MaxValue));
				property.isExpanded = true;
            }
			EditorGUILayout.EndHorizontal();

			Editors.DrawSeparator();
			
			// Content
			if(property.isExpanded)
			{
				EditorGUILayout.BeginVertical();
				for (int i = 0; i < property.arraySize; i++)
				{
					EditorGUILayout.BeginHorizontal();
					GUILayout.Space(12);
					EditorGUILayout.BeginVertical();
					SerializedProperty arrayElement = property.GetArrayElementAtIndex(i);
					EditorGUI.GetPropertyHeight(arrayElement);
					EditorGUILayout.PropertyField(arrayElement);
					EditorGUILayout.EndVertical();
					if (GUILayout.Button(deleteIcon, textureButton))
					{
						if (arrayElement.propertyType == SerializedPropertyType.ObjectReference)
						{
							arrayElement.objectReferenceValue = null;
						}

						property.DeleteArrayElementAtIndex(i);
						
						if(property.arraySize == 0)
                        {
							property.isExpanded = false;
						}
						return;
					}
					EditorGUILayout.EndHorizontal();
					GUILayout.Space(2);
				}
				EditorGUILayout.EndVertical();
			}

			EditorGUILayout.EndVertical();
		}


    }
}
