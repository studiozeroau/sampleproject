﻿using System;

using UnityEditor;
using UnityEngine;

using EditorUtils = Zeroes.Editor.Utilities;

// This class contains neat editor shortcuts for creating assets
// Generally follow the model of creating a game object, then adding the necessary components to it

namespace Zeroes.UI
{
	public class Utilities
	{

		// Creates a new ZText GameObject
		[MenuItem("GameObject/Zeroes/UI/Text", priority = 1)]
		public static Text CreateText()
		{
			GameObject newText = EditorUtils.NewUIGameObject("Text");
			Text text = newText.AddComponent<Text>();
			text.text = "NEW TEXT";
            text.UseDyslexicFont = true;
            // Find and assign the text material on creation. Allows the accessibility background to render correctly
            string[] assets = AssetDatabase.FindAssets("Text Material t:Material", new string[] { "Assets/Zeroes/Materials" });
            Material textMaterial = AssetDatabase.LoadAssetAtPath<Material>(AssetDatabase.GUIDToAssetPath(assets[0]));
            text.material = textMaterial;
            Selection.activeObject = newText;
            return text;
        }

		[MenuItem("GameObject/Zeroes/UI/Image", priority = 2)]
		public static ZImage CreateImage()
		{
			GameObject newImage = EditorUtils.NewUIGameObject("Image");
			ZImage image = newImage.AddComponent<ZImage>();
			Selection.activeObject = newImage;
			return image;
		}
        
		[MenuItem("GameObject/Zeroes/UI/Raw Image", priority = 3)]
		public static ZRawImage CreateRawImage()
		{
			GameObject newRawImage = EditorUtils.NewUIGameObject("RawImage");
			ZRawImage image = newRawImage.AddComponent<ZRawImage>();
			Selection.activeObject = newRawImage;
			return image;
		}
		
		// Creates a new button
		[MenuItem("GameObject/Zeroes/UI/Button", priority = 4)]
		public static Button CreateButton()
		{
			GameObject newButton = EditorUtils.NewUIGameObject("Button");
			newButton.AddComponent<ZImage>();
			newButton.AddComponent<Button>();

			// Add a content container to the button
			RectTransform contentContainer = EditorUtils.NewUIGameObject("ContentContainer").GetComponent<RectTransform>();
			contentContainer.SetParent(newButton.transform, false);
            contentContainer.anchorMin = Vector2.zero;
            contentContainer.anchorMax = Vector2.one;
            contentContainer.offsetMin = Vector2.zero;
            contentContainer.offsetMax = Vector2.zero;

            Selection.activeObject = newButton;

            return newButton.GetComponent<Button>();
        }

        [MenuItem("GameObject/Zeroes/UI/Toggle", priority = 5)]
        public static ToggleButton CreateToggleButton()
        {
            // Create the main toggle button
            ToggleButton toggleButton = EditorUtils.NewUIGameObject("ToggleButton").AddComponent<ToggleButton>();
            ZImage toggleImage = toggleButton.GetComponent<ZImage>();
            toggleImage.sprite = AssetDatabase.GetBuiltinExtraResource<Sprite>("UI/Skin/Background.psd");
            toggleImage.type = UnityEngine.UI.Image.Type.Sliced;
            RectTransform toggleRect = toggleButton.GetComponent<RectTransform>();
            toggleRect.sizeDelta = new Vector2(200, 100);

            // Create the active state graphic
            ZImage activeState = CreateImage();
            activeState.sprite = AssetDatabase.GetBuiltinExtraResource<Sprite>("UI/Skin/UISprite.psd");
            activeState.type = UnityEngine.UI.Image.Type.Sliced;
            CanvasGroup activeCanvasGroup = activeState.gameObject.AddComponent<CanvasGroup>();
            activeCanvasGroup.alpha = 0f;
            activeState.transform.SetParent(toggleButton.transform, false);
            RectTransform activeRect = activeState.GetComponent<RectTransform>();
            activeRect.anchorMin = Vector2.zero;
            activeRect.anchorMax = Vector2.one;
            activeRect.offsetMin = Vector2.zero;
            activeRect.offsetMax = Vector2.zero;
            activeState.name = "Active";

            // Create the button blip
            ZImage button = CreateImage();
            button.sprite = AssetDatabase.GetBuiltinExtraResource<Sprite>("UI/Skin/Knob.psd");
            button.type = UnityEngine.UI.Image.Type.Sliced;
            button.transform.SetParent(toggleButton.transform);
            RectTransform buttonRect = button.GetComponent<RectTransform>();
            buttonRect.sizeDelta = new Vector2(100, 100);
            buttonRect.anchoredPosition = new Vector2(-50, 0);
            button.name = "Button";

            Selection.activeObject = toggleButton;

            return toggleButton;
        }

        // Creates a new Slider
        [MenuItem("GameObject/Zeroes/UI/Slider", priority = 10)]
        public static void CreateSlider()
        {
            // Create the slider with a default size
            GameObject newSlider = EditorUtils.NewUIGameObject("Slider");
            UnityEngine.UI.Image image = newSlider.AddComponent<UnityEngine.UI.Image>();
            newSlider.AddComponent<Slider>();
            RectTransform sliderTransform = newSlider.GetComponent<RectTransform>();
            sliderTransform.sizeDelta = new Vector2(500, 30);

            // Create a button for the slider and attach it
            Button newButton = CreateButton();
            RectTransform btnTransform = newButton.GetComponent<RectTransform>();
            btnTransform.SetParent(newSlider.transform, false);
            btnTransform.anchoredPosition = new Vector2(-250, 0);

            Selection.activeObject = newSlider;
        }

        // Creates a new text entry element
        [MenuItem("GameObject/Zeroes/UI/Input Field", priority = 10)]
        public static void CreateInputField()
        {
            // Create the input field
            GameObject textInput = EditorUtils.NewUIGameObject("InputField");
            InputField inputField = textInput.AddComponent<InputField>();
            RectTransform inputTransform = textInput.GetComponent<RectTransform>();
            inputTransform.sizeDelta = new Vector2(400, 100);
            UnityEngine.UI.Image image = textInput.AddComponent<UnityEngine.UI.Image>();
            

            // Create the actual text portion
            Text text = CreateText();
            text.text = "Enter text...";
            text.color = Color.gray;
            text.fontSize = 40;
            text.alignment = TextAnchor.MiddleCenter;
            RectTransform textTransform = text.GetComponent<RectTransform>();
            textTransform.SetParent(inputTransform, false);
            textTransform.anchorMin = Vector2.zero;
            textTransform.anchorMax = Vector2.one;
            textTransform.offsetMin = Vector2.zero;
            textTransform.offsetMax = Vector2.zero;

            Selection.activeObject = textInput;
        }

		// Creates a new touch area in the editor
		[MenuItem("GameObject/Zeroes/UI/Touch Area", priority = 10)]
		public static void CreateTouchArea()
		{
			GameObject newTouchArea = EditorUtils.NewUIGameObject("TouchArea");
			newTouchArea.AddComponent<UITouchArea>();

            Selection.activeObject = newTouchArea;
        }

		// Creates a new scroll area/view/content in the editor
		[MenuItem("GameObject/Zeroes/UI/Scroll Area", priority = 10)]
		public static void CreateScrollArea()
		{
			// Create the scroll area/view gameobject
			GameObject newScrollArea = EditorUtils.NewUIGameObject("ScrollView");

			// Create the gameobject that houses the scroll area's content
			GameObject scrollContent = EditorUtils.NewUIGameObject("ScrollContent");

			RectTransform scrollContentTransform = scrollContent.GetComponent<RectTransform>();
			scrollContentTransform.SetParent(newScrollArea.transform);

			UIScrollArea uiScrollArea = newScrollArea.AddComponent<UIScrollArea>();
			uiScrollArea.ScrollViewArea = newScrollArea.GetComponent<RectTransform>();
			uiScrollArea.ScrollAreaContent = scrollContentTransform;

            Selection.activeObject = newScrollArea;
        }

		// Creates a canvas with the ZCanvasScaler component
		[MenuItem("GameObject/Zeroes/UI/Canvas", priority = 10)]
		public static void CreateCanvas()
		{
			GameObject newCanvas = EditorUtils.NewGameObject("Canvas");
			newCanvas.AddComponent<RectTransform>();
			Canvas canvas = newCanvas.AddComponent<Canvas>();
			canvas.renderMode = RenderMode.ScreenSpaceOverlay;
			newCanvas.AddComponent<UnityEngine.UI.GraphicRaycaster>();
			CanvasScaler canvasScaler = newCanvas.AddComponent<CanvasScaler>();
			canvasScaler.uiScaleMode = UnityEngine.UI.CanvasScaler.ScaleMode.ScaleWithScreenSize;

            Selection.activeObject = newCanvas;
        }

	}

}
