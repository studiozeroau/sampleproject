﻿using System;
using System.Collections.Generic;
using System.Linq;

using UnityEngine;
using UnityUI = UnityEngine.UI;
using UnityEditor;

using Zeroes.UI;

[CanEditMultipleObjects] 
[CustomEditor(typeof(Text), true)]
public class TextDrawer : Editor
{
	// Default values
	private SerializedProperty m_Text;
	private SerializedProperty m_FontData;
	private SerializedProperty m_Color;
	private SerializedProperty m_Material;
	private SerializedProperty m_RaycastTarget;

	// Accessibility
	private SerializedProperty bUseAccessibilityFont;
	private SerializedProperty bUseBackground;
	private SerializedProperty BackgroundColour;
	private SerializedProperty WidthPadding;
	private SerializedProperty HeightPadding;

	// Localisation
	private SerializedProperty bRequiresLocalization;
	private SerializedProperty LocaliseID;

	private SerializedProperty HorizontalAngle;
	private SerializedProperty VerticalAngle;

	void OnEnable()
	{
		// All the default values
		m_Text = serializedObject.FindProperty("m_Text");
		m_FontData = serializedObject.FindProperty("m_FontData");
		m_Color = serializedObject.FindProperty("m_Color");
		m_Material = serializedObject.FindProperty("m_Material");
		m_RaycastTarget = serializedObject.FindProperty("m_RaycastTarget");

		// Accessibility
		bUseAccessibilityFont = serializedObject.FindProperty("UseDyslexicFont");
		bUseBackground = serializedObject.FindProperty("UseBackground");
		BackgroundColour = serializedObject.FindProperty("BackgroundColour");
		WidthPadding = serializedObject.FindProperty("WidthPadding");
		HeightPadding = serializedObject.FindProperty("HeightPadding");

		// Localisation
		bRequiresLocalization = serializedObject.FindProperty("Localised");
		LocaliseID = serializedObject.FindProperty("LocalisedID");

		// Skew properties
		HorizontalAngle = serializedObject.FindProperty("_HorizontalAngle");
		VerticalAngle = serializedObject.FindProperty("_VerticalAngle");
	}

	public override void OnInspectorGUI()
	{
		serializedObject.Update();

		DrawAccessibilityOptions();

		Editors.DrawSeparator();

		DrawLocalizationOptions();

		Editors.DrawSeparator();
		
		Editors.DrawSeparator();

		DrawDefault();

		serializedObject.ApplyModifiedProperties();
	}

	public void DrawMargin()
	{
		GUILayout.Space(10);
	}

	public void DrawAccessibilityOptions()
	{
		// Draw Header
		EditorGUILayout.Space();
		EditorGUILayout.LabelField("Accessibility Options", EditorStyles.boldLabel);
		EditorGUILayout.Space();

		// Draw content
		EditorGUILayout.BeginHorizontal();
		DrawMargin();
		EditorGUILayout.BeginVertical();

		EditorGUILayout.PropertyField(bUseAccessibilityFont);
		EditorGUILayout.PropertyField(bUseBackground);
		if(bUseBackground.boolValue == true)
		{
			EditorGUILayout.PropertyField(BackgroundColour);
			EditorGUILayout.PropertyField(WidthPadding);
			EditorGUILayout.PropertyField(HeightPadding);
		}

		EditorGUILayout.EndVertical();
		EditorGUILayout.EndHorizontal();

		EditorGUILayout.Space();
	}


	public void DrawLocalizationOptions()
	{
		// Draw this section's header
		EditorGUILayout.Space();
		EditorGUILayout.LabelField("Localization Options", EditorStyles.boldLabel);
		EditorGUILayout.Space();

		EditorGUILayout.BeginHorizontal();
		DrawMargin();
		EditorGUILayout.BeginVertical();


		EditorGUILayout.PropertyField(bRequiresLocalization);
		if (bRequiresLocalization.boolValue == true)
		{
			EditorGUILayout.PropertyField(LocaliseID);
		}

		EditorGUILayout.EndVertical();
		EditorGUILayout.EndHorizontal();

		EditorGUILayout.Space();
	}

	// Draw all old default values for the text field
	public void DrawDefault()
	{
		EditorGUILayout.PropertyField(m_Text);
		EditorGUILayout.PropertyField(m_FontData);
		EditorGUILayout.PropertyField(m_Color);
		EditorGUILayout.PropertyField(m_Material);
		EditorGUILayout.PropertyField(m_RaycastTarget);
	}
}
