﻿using System;
using System.Collections;

using UnityEngine;
using UnityEngine.UI;

using Zeroes.Managers;
using Zeroes.Touch;

namespace Zeroes.UI
{
    
    [RequireComponent(typeof(ZImage))]

	public class Button : UITouchArea
    {
        //A struct storing the different graphics for a button
        [System.Serializable]
        public struct ButtonConfiguration
        {
            public Sprite Idle;
            public Sprite Down;
            public Sprite Disabled;
        }

        public ButtonConfiguration CurrentConfig;

        protected Image ButtonImage;

        //Button actions. Don't need any specific callbacks
        public Action OnDown;
        public Action OnRelease;
        public Action OnReleaseOutside;

        //A child tranasform we may need to move when the button is down
        public RectTransform ContentContainer { private set; get; }

        private Vector2 ContentOrigin;

        //How much we offset the child transform by
        public Vector2 ContentOffset;
        
        //Enabled/disabled functionality
        public bool Enabled
        {
            get
            {
				return TouchArea.Enabled;
            }
            set
            {
				if (Enabled != value)
                {
					TouchArea.Enabled = value;

					if (Enabled)
                    {
                        IdleState();
                    }
                    else
                    {
                        DisabledState();
                    }
                }
            }
        }

        public override void Awake()
        {
			base.Awake();

            ButtonImage = GetComponent<Image>();

            var content = transform.Find("ContentContainer");
            if (content != null)
            {
                ContentContainer = content.GetComponent<RectTransform>();
                ContentOrigin = ContentContainer.anchoredPosition;
            }

			TouchArea.OnTouchDown += TouchDown;
			TouchArea.OnTouchUp += TouchUp;
			TouchArea.OnTouchMove += TouchMove;
			TouchArea.OnTouchCancel += TouchCancel;
        }

        #region TOUCH METHODS

        public void TouchDown(TouchArea touchArea)
        {
            DownState();
            
            if (OnDown != null)
            {
                OnDown();
            }
        }

        public void TouchMove(TouchArea touchArea)
        {
            if (touchArea.Contains(touchArea.TouchPosition))
            {
                DownState();
            }
            else
            {
                IdleState();
            }
        }

        public void TouchUp(TouchArea touchArea)
        {
            IdleState();

			if (touchArea.Contains(touchArea.TouchPosition))
			{
                if (OnRelease != null)
				{
                    OnRelease();
                }
            }
            else
            {
                if (OnReleaseOutside != null)
                {
                    OnReleaseOutside();
                }
            }
        }

        public void TouchCancel(TouchArea touchArea)
        {
            IdleState();
        }

        #endregion


        #region STATES

        public virtual void IdleState()
        {
            SetSprite(CurrentConfig.Idle);

            if (ContentContainer != null)
            {
                ContentContainer.anchoredPosition = ContentOrigin;
            }
        }

        public virtual void DownState()
        {
            SetSprite(CurrentConfig.Down);

            if (ContentContainer != null)
            {
                ContentContainer.anchoredPosition = ContentOrigin + ContentOffset;
            }
        }

        public virtual void DisabledState()
        {
            SetSprite(CurrentConfig.Disabled);
        }

        protected virtual void SetSprite(Sprite sprite)
        {
            if (sprite == null)
                return;
            
            if (ButtonImage.sprite != sprite)
            {
                ButtonImage.sprite = sprite;
            }
        }

        #endregion


        public void Cancel()
        {
            TouchArea.CancelTouch();
        }

    }
}