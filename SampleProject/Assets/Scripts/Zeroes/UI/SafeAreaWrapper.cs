﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Zeroes;

namespace Zeroes.UI
{
	public class SafeAreaWrapper : MonoBehaviour
	{
		[Header("Conform RectTransform to SafeArea size")]
		public bool ConformToSize = false;

		[Header("Individual Safe Area Offsets")]
		public bool ApplyOffsetTop = false;
		public bool ApplyOffsetBottom = false;
		public bool ApplyOffsetLeft = false;
		public bool ApplyOffsetRight = false;

		private void Awake()
		{
			RectTransform rect = GetComponent<RectTransform>();

			if (rect != null)
			{
				float left = 0;
				float right = 0;
				float top = 0;
				float bottom = 0;

				if (ConformToSize)
				{
					ApplyOffsetTop = true;
					ApplyOffsetBottom = true;
					ApplyOffsetLeft = true;
					ApplyOffsetRight = true;
				}

				if (ApplyOffsetTop)
				{
					top = -ZScreen.CanvasSafeAreaOffset_Top;
				}

				if (ApplyOffsetBottom)
				{
					bottom = ZScreen.CanvasSafeAreaOffset_Bottom;
				}

				if (ApplyOffsetLeft)
				{
					left = ZScreen.CanvasSafeAreaOffset_Left;
				}

				if (ApplyOffsetRight)
				{
					right = -ZScreen.CanvasSafeAreaOffset_Right;
				}

				rect.offsetMin = new Vector2(left, bottom);
				rect.offsetMax = new Vector2(right, top);
			}
		}
	}
}
