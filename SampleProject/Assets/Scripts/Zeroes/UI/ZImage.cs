﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Zeroes.UI
{
    public class ZImage : Image, ISkewable
    {
	    [SerializeField] private UISkew _uiSkew;
		
	    protected override void OnPopulateMesh(VertexHelper vh)
	    {
		    base.OnPopulateMesh(vh);
			
		    if(_uiSkew != null)
			    _uiSkew.Skew(vh);
	    }

	    public void PrepareForSkew(UISkew uiSkew)
	    {
		    _uiSkew = uiSkew;
	    }
    }
}