﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using UnityUI = UnityEngine.UI;

using Zeroes;
using Zeroes.Managers;
using Zeroes.Localisation;
using UnityEngine.UI;
using UnityEditor;

// Override Unity's base Text component to add my own custom options to it

namespace Zeroes.UI
{
	public class Text : UnityUI.Text, ISkewable
	{
		// Override this
		// TODO. Update aspects of this text field depending on options
		public override string text
		{
			get
			{
				return base.text;
			}

			set
			{
				base.text = value;
			}
		}

		#region ISkewable Implementation

		[SerializeField] private UISkew _uiSkew;

		public void PrepareForSkew(UISkew uiSkew)
		{
			_uiSkew = uiSkew;
		}
		
        #endregion

		protected override void Start()
		{
			base.Start();

			// Counteracts ExecuteInEditMode the base Class uses
			if (Application.isPlaying == false)
			{
				return;
			}

			if (UseDyslexicFont)
			{
				// Cache the current font in case a toggle is required
				CachedFont = font;

				// Use the dyslexic font loaded into memory if applicable
				if (GameManager.Instance.DeviceData.UseDyslexicFont)
				{
					UpdateDyslexicFont(GameManager.Instance.AccessibilityManager.DyslexicFont);
				}

				// Subscribe to Accessibility change events
				AccessibilityManager.OnDyslexicFontSettingChanged += UpdateDyslexicFont;
			}

			if (Localised)
			{
				text = GameManager.Instance.LocalisationManager.GetLocalisedString(LocalisedID);

				// Subscribe to Localisation change events
				LocalisationManager.OnLanguageChanged += UpdateLocalisation;
			}

			// Set up and callback for text background
			if(UseBackground)
            {
				UpdateTextBackground(GameManager.Instance.DeviceData.UseTextBackgrounds);

				AccessibilityManager.OnTextBackgroundSettingChanged += UpdateTextBackground;

			}
		}

		protected override void OnPopulateMesh(VertexHelper vh)
		{
			base.OnPopulateMesh(vh);

#if UNITY_EDITOR
			if(Application.isPlaying == false)
			{
				DrawBackground = TextTools.GetShowText();
			}
#endif
			
			// Apply a background underneath the text
			if (DrawBackground == true)
			{
				// Min/max positions for bounds
				Vector2 min = new Vector2(float.MaxValue, float.MaxValue);
				Vector2 max = new Vector2(-float.MaxValue, -float.MaxValue);

				// Get min/max positions for vertices
				List<UIVertex> stream = new List<UIVertex>();
				vh.GetUIVertexStream(stream);
				foreach (var vertex in stream)
				{
					min.x = Mathf.Min(min.x, vertex.position.x);
					min.y = Mathf.Min(min.y, vertex.position.y);
					max.x = Mathf.Max(max.x, vertex.position.x);
					max.y = Mathf.Max(max.y, vertex.position.y);
				}

				// Pad the min/max a bit for a nice bounding box
				min.x -= WidthPadding;
				min.y -= HeightPadding;
				max.x += WidthPadding;
				max.y += HeightPadding;

				// Clear the current vertex helper
				vh.Clear();

				// Add a quad for the text background
				UIVertex v1 = new UIVertex();
				UIVertex v2 = new UIVertex();
				UIVertex v3 = new UIVertex();
				UIVertex v4 = new UIVertex();

				// Set the positions of each vertice and pack in background to Z
				v1.position = new Vector3(min.x, min.y, 1);
				v2.position = new Vector3(max.x, min.y, 1);
				v3.position = new Vector3(max.x, max.y, 1);
				v4.position = new Vector3(min.x, max.y, 1);

				// Set the vertex colour to the colour chosen
				v1.color = v2.color = v3.color = v4.color = BackgroundColour;
				v1.uv0 = v2.uv0 = v3.uv0 = v4.uv0 = new Vector2(0, 0);

				// Add this to the old stream
				stream.Insert(0, v4);
				stream.Insert(0, v3);
				stream.Insert(0, v2);
				stream.Insert(0, v1);

				// Create a new list of indices for the verts
				List<int> indices = new List<int>() { 0, 2, 1, 3, 2, 0 };
				for (int i = 0; i < stream.Count - 4; i ++)
				{
					indices.Add(i + 4);
				}

				vh.AddUIVertexStream(stream, indices);
			}
			
			if(_uiSkew != null)
				_uiSkew.Skew(vh);
		}

		#region ACCESSIBILITY

        // Does this Text field require the use of an accessibility font override?
        public bool UseDyslexicFont = true;

		// Cache the old font in case a toggle is required
		private Font CachedFont = null;

		// Should this text field have a background behind it
		public bool UseBackground = false;

		[SerializeField]
		private Color BackgroundColour = new Color(0, 0, 0, .5f);

		[SerializeField, Range(0, 50)]
		private float WidthPadding = 2f;

		[SerializeField, Range(0, 50)]
		private float HeightPadding = 2f;

		private bool DrawBackground = false;

		private void UpdateDyslexicFont(Font dyslexicFont)
		{
			if (dyslexicFont == null)
			{
				font = CachedFont;
			}
			else
			{
				font = dyslexicFont;
			}
		}

		private void UpdateTextBackground(bool state)
        {
			if (DrawBackground != state)
			{
				DrawBackground = state;
				SetAllDirty();

				if(TryGetComponent<Shadow>(out Shadow shadow))
                {
					shadow.enabled = !state;
                }
			}
        }

		#endregion



		#region LOCALISATION

		// Does this text field require localisation
		public bool Localised = false;

		// The ID this text field uses
		public string LocalisedID = "";

		private void UpdateLocalisation(string language)
		{
			text = GameManager.Instance.LocalisationManager.GetLocalisedString(LocalisedID);
		}

		#endregion


		// Clean up
		protected override void OnDestroy()
		{
			AccessibilityManager.OnDyslexicFontSettingChanged -= UpdateDyslexicFont;
			AccessibilityManager.OnTextBackgroundSettingChanged -= UpdateTextBackground;
			LocalisationManager.OnLanguageChanged -= UpdateLocalisation;
		}
	}
}

#if UNITY_EDITOR

public class TextTools
{
	private const string SHOW_TEXT_KEY = "SHOW_TEXT_BACKGROUNDS";

	[MenuItem("Zeroes/Toggle Accessibility Backgrounds")]
	public static void ToggleAccessibilityBackgrounds()
	{
		SessionState.SetBool(SHOW_TEXT_KEY, !GetShowText());
		foreach(var graphic in GameObject.FindObjectsOfType<Graphic>())
        {
			graphic.SetAllDirty();
			EditorUtility.SetDirty(graphic);
        }
	}

	public static bool GetShowText()
	{
		return SessionState.GetBool(SHOW_TEXT_KEY, false);
	}
}

#endif