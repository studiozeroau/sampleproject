﻿using UnityEngine;
using UnityUI = UnityEngine.UI;

using System.Collections;

using Zeroes.Managers;

public class CanvasScaler : UnityUI.CanvasScaler
{
	
	// Always make sure a Canvas in Zeroes uses the specified settings in the screen class
	// This is mostly just a safety thing to ensure I'm not accidentally breaking menus when changing things
	public new void Awake()
	{
		// Because this is an ExecuteInEditor derivative
		if (Application.isPlaying)
		{
			
			m_ReferenceResolution.x = ZScreen.CanvasWidth;
			
			m_ReferenceResolution.y = ZScreen.CanvasHeight;
			
		}
	}

}

