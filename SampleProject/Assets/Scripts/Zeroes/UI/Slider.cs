﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using Zeroes.UI;
using Zeroes.Touch;

namespace Zeroes.UI
{
    public class Slider : UITouchArea
    {

        // Events
        public Action<Slider> OnValueChanged;
        public Action OnSliderReleased;
        
        // General properties for the Slider
        protected RectTransform Rect;
        private Button Button;
        private RectTransform ButtonRectTransform;
        private Vector2 TargetPosition = Vector2.zero;

        // Min/max values for the slider
        public float Min = 0;
        public float Max = 1;

        // Slider button movementsmoothing
        [Range(0f, 1f)]
        public float Smoothing = .5f;
        
        // Normalized value of the slider (0 - 1)
        public float NormalizedValue
        {
            get
            {
                float position = TargetPosition.x + Rect.sizeDelta.x * .5f;
                position /= Rect.sizeDelta.x;
                return position;
            }
            set
            {
                TargetPosition.x = -Rect.sizeDelta.x * .5f + Rect.sizeDelta.x * Mathf.Clamp(value, 0f, 1f);
                ButtonRectTransform.anchoredPosition = new Vector2(TargetPosition.x, ButtonRectTransform.anchoredPosition.y);
            }
        }

        // Value of the slider based on the min/max
        public float Value
        {
            get
            {
                return Min + (Max - Min) * NormalizedValue;
            }
            set
            {
                NormalizedValue = Mathf.Clamp(value, Min, Max) / Max;
            }
        }

        public override void Awake()
        {
            base.Awake();

            // Slider touch events
            OnTouchDown += SliderTouchDown;
            OnTouchMove += SliderOnTouchMove;
            OnTouchUp += SliderTouchUp;

            // Properties of the slider
            Rect = GetComponent<RectTransform>();

            // Properties of the button in the slider
            Button = gameObject.Get("Button").GetComponent<Button>();
            Button.OnTouchMove += SliderOnTouchMove;
            Button.OnRelease += () => OnSliderReleased?.Invoke();
            Button.OnReleaseOutside += () => OnSliderReleased?.Invoke();
            ButtonRectTransform = Button.GetComponent<RectTransform>();
            TargetPosition = ButtonRectTransform.anchoredPosition;

            // Set up precedence
            GivePrecedenceTo(Button);
        }

        // Move the slider button in Update to make it smooth
        public virtual void Update()
        {
            ButtonRectTransform.anchoredPosition = Vector2.Lerp(TargetPosition, ButtonRectTransform.anchoredPosition, Smoothing);
        }

        // Tap anywhere along the slider to snap to position
        private void SliderTouchDown(TouchArea touchArea)
        {
            TargetPosition.x = -Rect.sizeDelta.x * .5f + touchArea.LocalTouchPosition.x;

            OnValueChanged?.Invoke(this);

            Button.DownState();
        }

        // Touch move event for the slider piece and the button
        private void SliderOnTouchMove(TouchArea touchArea)
        {
            float moveDistance = touchArea.MoveDistance.x;
            if (TouchArea.ContainsHorizontal(touchArea.TouchPosition + new Vector2(moveDistance, 0)) == false)
            {
                return;
            }

            TargetPosition.x += moveDistance;
            TargetPosition.x = Mathf.Clamp(TargetPosition.x, -Rect.sizeDelta.x * .5f, Rect.sizeDelta.x * .5f);

            OnValueChanged?.Invoke(this);
        }

        // Touch up event from the slider
        private void SliderTouchUp(TouchArea touchArea)
        {
            Button.IdleState();

            // Makes it safe to do saving or anything at this stage
            OnSliderReleased?.Invoke();
        }

    }
}