using UnityEngine;
using UnityEngine.UI;

namespace Zeroes.UI
{
    public class ZRawImage : RawImage, ISkewable
    {
        [SerializeField] private UISkew _uiSkew;
		
        protected override void OnPopulateMesh(VertexHelper vh)
        {
            base.OnPopulateMesh(vh);
			
            if(_uiSkew != null)
                _uiSkew.Skew(vh);
        }

        public void PrepareForSkew(UISkew uiSkew)
        {
            _uiSkew = uiSkew;
        }
    }
}