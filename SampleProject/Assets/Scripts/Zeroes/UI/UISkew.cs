using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public interface ISkewable
{
    void PrepareForSkew(UISkew uiSkew);
}

[ExecuteInEditMode]
[DisallowMultipleComponent]
public class UISkew : MonoBehaviour
{

    [SerializeField] private float _horizontalAngle;
    [SerializeField] private float _verticalAngle;
    [SerializeField] private UISkew[] _uiSkewChildren = new UISkew[0];
    [SerializeField] private UISkew[] _uiSkewParents = new UISkew[0];
    [SerializeField] private MaskableGraphic _maskableGraphic;
    
    public float HorizontalAngle { get => _horizontalAngle; set => _horizontalAngle = value; }
    public float VerticalAngle { get => _verticalAngle; set => _verticalAngle = value; }
    public UISkew[] UISkewParents => _uiSkewParents;
    public MaskableGraphic MaskableGraphic => _maskableGraphic;
    
    private void OnTransformChildrenChanged()
    {
        _uiSkewChildren = gameObject.GetComponentsInChildren<UISkew>();
    }

    private void OnTransformParentChanged()
    {
        _uiSkewParents = gameObject.GetComponentsInParent<UISkew>();
    }

    private void Reset()
    {
        _maskableGraphic = GetComponent<MaskableGraphic>();
        
        OnTransformChildrenChanged();
        OnTransformParentChanged();
        
        // Get any skewable object that might be attached to this gameObject
        ISkewable skewable = GetComponent<ISkewable>();
        if(skewable != null)
            skewable.PrepareForSkew(this);
        
        DirtyVertices();
    }

    private void OnValidate()
    {
        if(_uiSkewChildren.Length == 0)
            OnTransformChildrenChanged();
        
        if(_uiSkewParents.Length == 0)
            OnTransformParentChanged();
        
        DirtyVertices();
    }

    private void LateUpdate()
    {
        DirtyVertices();
    }

    private void DirtyVertices()
    {
        if(_maskableGraphic != null)
            _maskableGraphic.SetVerticesDirty();

        foreach (var child in _uiSkewChildren)
        {
            if(child != this)
                child._maskableGraphic.SetVerticesDirty();
        }
    }

    public void Skew(VertexHelper vh)
    {
        float horizontalAngle = HorizontalAngle;
        float verticalAngle = VerticalAngle;

        foreach (var parent in _uiSkewParents)
        {
            if (parent == this)
                continue;
            
            horizontalAngle += parent.HorizontalAngle;
            verticalAngle += parent.VerticalAngle;
        }
        
        Matrix4x4 skewMatrix = Matrix4x4.identity;
        skewMatrix.SetColumn(0, new Vector4(1, verticalAngle, 0, 0));
        skewMatrix.SetColumn(1, new Vector4(horizontalAngle, 1, 0, 0));

        UIVertex v = new UIVertex();
        for (int i = 0; i < vh.currentVertCount; i++)
        {
            vh.PopulateUIVertex(ref v, i);
            v.position = skewMatrix.inverse.MultiplyPoint3x4(v.position);
            vh.SetUIVertex(v, i);
        }
    }
    
}
