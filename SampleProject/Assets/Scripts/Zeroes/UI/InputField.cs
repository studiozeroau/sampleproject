﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.Windows.Input;

using Zeroes.Touch;

namespace Zeroes.UI
{
    public class InputField : Button
    {
        // TODO. Input caret?
        
        // The actual text display
        private Text TextField;

        // Directly get the text of the text field
        public string Text
        {
            get
            {
                return TextField.text;
            }
            set
            {
                TextField.text = value;
            }
        }

        // The placeholder text when nothing is there
        public string Placeholder = "Enter text...";
        
        // Do we need to clear the placeholder text on activation?
        private bool ClearPlaceholder = true;
        
        // Are we currently performing input
        private bool InputActive = false;

        // A Touch Area for editor/standalone platforms to defocus the input field
        private TouchArea TouchOutsideArea;

        // Touch Screen Keyboard for mobile platforms
        private TouchScreenKeyboard Keyboard;
        public TouchScreenKeyboardType KeyboardType;

        public override void Awake()
        {
            base.Awake();

            TextField = gameObject.Get("Text").GetComponent<Text>();
            TextField.text = Placeholder;

            OnRelease += BeginInput;
        }

        public void Update()
        {
            if (Application.isEditor || Application.isMobilePlatform == false)
            {
                if (InputActive == true)
                {
                    if (Input.GetKeyDown(KeyCode.Backspace))
                    {
                        Text = Text.RemoveLast();
                    }
                    else if (Input.GetKeyDown(KeyCode.Escape))
                    {
                        EndInput();
                    }
                    else if (Input.inputString.Equals(string.Empty) == false)
                    {
                        Text = Text + Input.inputString;
                    }
                }
            }
            else
            {
                if (Keyboard != null && Keyboard.active == true)
                {
                    Text = Keyboard.text;
                }
                else
                {
                    if (InputActive == true)
                    {
                        EndInput();
                    }
                }
            }
        }

        // Begin the process for input
        public void BeginInput()
        {
            if(ClearPlaceholder == true)
            {
                Text = "";
            }

            InputActive = true;

            if(Application.isMobilePlatform)
            {
                Keyboard = TouchScreenKeyboard.Open(Text, KeyboardType);
            }
            else
            {
                // Create a separate touch area for touch cancelling
                TouchOutsideArea = new TouchArea(new Rect(-ZScreen.Width, -ZScreen.Height, ZScreen.Width * 2, ZScreen.Height * 2));
                TouchOutsideArea.OnTouchDown += (TouchArea touchArea) =>
                {
                    EndInput();
                };
            }
        }

        // Stop input
        public void EndInput()
        {
            InputActive = false;

            if(Text.Equals(string.Empty))
            {
                Text = Placeholder;
                ClearPlaceholder = true;
            }
            else
            {
                ClearPlaceholder = false;
            }
            
            // Clean up
            if (TouchOutsideArea != null)
            {
                TouchOutsideArea.Remove();
                TouchOutsideArea = null;
            }
        }

        // Clean up
        public override void OnDestroy()
        {
            base.OnDestroy();
            
            if(TouchOutsideArea != null)
            {
                TouchOutsideArea.Remove();
                TouchOutsideArea = null;
            }
        }
    }
}