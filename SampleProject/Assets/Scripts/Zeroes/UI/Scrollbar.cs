﻿using System.Collections;
using System.Collections.Generic;
using System.Xml.Schema;
using UnityEngine;

using Zeroes.Touch;

namespace Zeroes.UI
{
    public class Scrollbar : MonoBehaviour
    {
        // The scroll area this is attached to
        [SerializeField]
        private UIScrollArea ScrollArea = null;

        // The RectTransform of the bar
        private RectTransform Bar = null;

        // The bounds of the whole scrollbar area
        private RectTransform ScrollRect = null;

        public void Awake()
        {
            ScrollRect = GetComponent<RectTransform>();
            Bar = gameObject.Get("Bar").GetComponent<RectTransform>();
        }

        public void Start()
        {
            Configure();
        }

        public void Configure()
        {
            // Update the size to be relative to the content of the scroll area
            Vector2 newSize = Bar.sizeDelta;
            if (ScrollArea.ScrollsHorizontally)
            {
                newSize.x = ScrollRect.rect.width * Mathf.Clamp(ScrollArea.ScrollArea.TouchRect.width / ScrollArea.ScrollArea.FullScrollBounds.width, .01f, 1);
            }
            if (ScrollArea.ScrollsVertically)
            {
                newSize.y = ScrollRect.rect.height * Mathf.Clamp(ScrollArea.ScrollArea.TouchRect.height / ScrollArea.ScrollArea.FullScrollBounds.height, .01f, 1);
            }
            Bar.sizeDelta = newSize;
        }

        // Controls the movement of the scrollbar
        public void Update()
        {
            // Required values
            Vector2 nrm = ScrollArea.ScrollArea.ScrollAmountNormalized;
            Vector2 position = Bar.anchoredPosition;
            Vector2 scale = Vector2.one;

            // Perform horizontal scroll and scaling
            if(ScrollArea.ScrollsHorizontally)
            {
                if (nrm.x < 0)
                {
                    scale.x = 1 + nrm.x;
                }
                else if (nrm.x > 1)
                {
                    scale.x = 1 - Mathf.Abs(1 - nrm.x);
                }

                float width = Bar.rect.width * scale.x;
                position.x = -(ScrollRect.rect.width - width) * .5f + nrm.x * (ScrollRect.rect.width - width);
                position.x = Mathf.Clamp(position.x, -ScrollRect.rect.width * .5f + width * .5f, ScrollRect.rect.width * .5f - width * .5f);
            }

            // Perform vertical scroll and scaling
            if(ScrollArea.ScrollsVertically)
            {
                if (nrm.y < 0)
                {
                    scale.y = 1 + nrm.y;
                }
                else if (nrm.y > 1)
                {
                    scale.y = 1 - Mathf.Abs(1 - nrm.y);
                }
                
                float height = Bar.rect.height * scale.y;
                position.y = (ScrollRect.rect.height - height) * .5f - nrm.y * (ScrollRect.rect.height - height);
                position.y = Mathf.Clamp(position.y, -ScrollRect.rect.height * .5f + height * .5f, ScrollRect.rect.height * .5f - height * .5f);
            }

            // Apply new position and scale
            Bar.anchoredPosition = position;
            Bar.localScale = scale;
        }
    }
}