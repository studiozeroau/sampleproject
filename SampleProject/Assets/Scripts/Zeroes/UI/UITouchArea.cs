﻿using UnityEngine;

using System;
using System.Collections;

using Zeroes.Touch;

namespace Zeroes.UI
{
    [RequireComponent(typeof(RectTransform))]
	public class UITouchArea : MonoBehaviour
	{
		public RectTransform RectTransform { private set; get; }

		public TouchArea TouchArea { protected set; get; }

		// Shortcuts
		public Action<TouchArea> OnTouchDown { get { return TouchArea.OnTouchDown; } set { TouchArea.OnTouchDown = value; } }
		public Action<TouchArea> OnTouchMove { get { return TouchArea.OnTouchMove; } set { TouchArea.OnTouchMove = value; } }
		public Action<TouchArea> OnTouchUp { get { return TouchArea.OnTouchUp; } set { TouchArea.OnTouchUp = value; } }
		public Action<TouchArea> OnTouchCancel { get { return TouchArea.OnTouchCancel; } set { TouchArea.OnTouchCancel = value; } }

		public string Layer = "Default";

		//Precedence List. Nice to set this up in the Editor
		public UITouchArea[] GivesPrecedenceTo;

		public virtual void Awake()
		{
			RectTransform = GetComponent<RectTransform>();

			TouchArea = new TouchArea(this.GetComponent<RectTransform>(), Layer);
		}

		public void Start()
		{
			//Automatically give precendence to stuff set up in the editor
			if (GivesPrecedenceTo != null)
			{
				for (int i = 0; i < GivesPrecedenceTo.Length; i++)
				{
					GivePrecedenceTo(GivesPrecedenceTo[i]);
				}
			}
		}

		public virtual void GivePrecedenceTo(UITouchArea uiTouchArea)
		{
			if(uiTouchArea != null)
			{
				TouchArea.GivePredenceTo(uiTouchArea.TouchArea);
			}
			else
            {
				Debug.Log($"Touch Area is null, remove from list on {gameObject.name} in {gameObject.scene.name}");
            }
		}

		//Clean up
		public virtual void OnDestroy()
		{
			TouchArea.Remove();
		}
	}
}