﻿using System;
using System.Collections.Generic;

using UnityEngine;
using UnityEditor;

namespace Zeroes
{
	public static class Zebug
	{

		#region CUBE DRAWING

		public static void DrawCube(Vector3 center, Vector3 size, Color color)
        {
            DrawCube(center, size, Quaternion.identity, color);
        }

		public static void DrawCube(Vector3 center, Vector3 size, Quaternion rotation)
		{
			DrawCube(center, size, rotation, Color.red);
		}

		public static void DrawCube(Vector3 center, Vector3 size, Quaternion rotation, Color color)
		{
			//TODO. This can be simplified?
			
			size = size / 2;

			Vector3 V1 = new Vector3();
			Vector3 V2 = new Vector3();
			Vector3 V3 = new Vector3();
			Vector3 V4 = new Vector3();

			V1.x = size.x;
			V1.y = size.y;
			V1.z = size.z;

			V2.x = -size.x;
			V2.y = size.y;
			V2.z = size.z;

			V3.x = size.x;
			V3.y = size.y;
			V3.z = -size.z;

			V4.x = -size.x;
			V4.y = size.y;
			V4.z = -size.z;

			Vector3 V5 = new Vector3();
			Vector3 V6 = new Vector3();
			Vector3 V7 = new Vector3();
			Vector3 V8 = new Vector3();

			V5 = V1;
			V5.y = -size.y;

			V6 = V2;
			V6.y = -size.y;

			V7 = V3;
			V7.y = -size.y;

			V8 = V4;
			V8.y = -size.y;

			V1 = rotation * V1;
			V2 = rotation * V2;
			V3 = rotation * V3;
			V4 = rotation * V4;
			V5 = rotation * V5;
			V6 = rotation * V6;
			V7 = rotation * V7;
			V8 = rotation * V8;

			V1 = center + V1;
			V2 = center + V2;
			V3 = center + V3;
			V4 = center + V4;
			V5 = center + V5;
			V6 = center + V6;
			V7 = center + V7;
			V8 = center + V8;

			Debug.DrawLine(V1, V2, color);
			Debug.DrawLine(V2, V4, color);
			Debug.DrawLine(V3, V4, color);
			Debug.DrawLine(V1, V3, color);

			Debug.DrawLine(V5, V6, color);
			Debug.DrawLine(V6, V8, color);
			Debug.DrawLine(V7, V8, color);
			Debug.DrawLine(V5, V7, color);

			Debug.DrawLine(V1, V5, color);
			Debug.DrawLine(V2, V6, color);
			Debug.DrawLine(V3, V7, color);
			Debug.DrawLine(V4, V8, color);

		}

		#endregion


		public static void DrawArrow(Vector3 position, Vector3 dir, Color colour, float size = 1)
		{
			Vector3 pt1 = Quaternion.LookRotation(dir) * Quaternion.Euler(new Vector3(0, -135, 0)) * Vector3.forward;
			Vector3 pt2 = Quaternion.LookRotation(dir) * Quaternion.Euler(new Vector3(0, 135, 0)) * Vector3.forward;

			pt1 = position + pt1 * size;
			pt2 = position + pt2 * size;

			Debug.DrawLine(position, pt1, colour);
			Debug.DrawLine(position, pt2, colour);
		}

        // Overload for no colour
        public static void Log(object message)
        {
            Log(message, Color.white);
        }

        // Custom log to specify a colour
        public static void Log(object message, Color color)
        {
			string str = $"{message}".Colour(color);
#if DEBUGGING
			Logs.Add($"[{DateTime.Now.ToString("HH:mm:ss").Colour(Color.white)}] {str}");
#endif
			Debug.Log(str);
        }

		#region ZEBUG LOGGING

		public static List<string> Logs = new List<string>();

		public static void ClearLog()
        {
			Logs.Clear();
		}
		#endregion
	}
}