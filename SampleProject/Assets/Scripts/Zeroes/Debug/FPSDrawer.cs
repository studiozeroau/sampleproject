﻿using UnityEngine;
using System.Collections;

//Component that draws the current framerate using the old Unity GUI system

public class FPSDrawer : MonoBehaviour
{
	private float DeltaTime = 0.0f;

	private int FontSize = Screen.height / 40;

	private GUIStyle Style = null;
	private GUIContent Content = new GUIContent();

	private float MSec, FPS;

    void Update()
    {
        DeltaTime += (Time.unscaledDeltaTime - DeltaTime) * 0.1f;
    }

    void OnGUI()
    {
		if (Style == null)
		{
			Style = GUI.skin.box;
			Style.alignment = TextAnchor.UpperLeft;
			Style.fontSize = FontSize;
			Style.normal.textColor = Color.white;
		}

		// Calculate FPS
        MSec = DeltaTime * 1000.0f;
        FPS = 1.0f / DeltaTime;

		// Create the string to display FPS
        string text = string.Format("{0:0.0} ms ({1:0.} fps)", MSec, FPS);
		Content.text = text;

		// Figure out its size and display it
		Vector2 size = Style.CalcSize(Content);
		GUI.Box(new Rect(0, ZScreen.SafeAreaOffset_Top, size.x, size.y), Content, Style);
    }
}