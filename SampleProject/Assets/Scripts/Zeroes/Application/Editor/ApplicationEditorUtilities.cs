﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEditor;

using Zeroes.Editor;

public class ApplicationEditorUtilities : MonoBehaviour
{

    [MenuItem("Assets/Create/Zeroes/New Project Configuration", false, 0)]
    private static void CreateLocalisationConfiguration()
    {
        // Get the appropriate icon to show in the editor
        Texture2D icon = EditorGUIUtility.IconContent("ScriptableObject Icon").image as Texture2D;

        // Callback to finish creating the Asset once name input has completed
        EndNameEditActionCallback callback = ScriptableObject.CreateInstance<EndNameEditActionCallback>();
        callback.OnComplete += (int instanceID, string pathName, string resourceFile) =>
        {
            AssetDatabase.CreateAsset(ScriptableObject.CreateInstance<ProjectConfiguration>(), pathName);
        };

        // Start the process of creating a new Scene
        ProjectWindowUtil.StartNameEditingIfProjectWindowExists(0, callback, Utilities.GetCurrentProjectWindowPath() + "/New Project Configuration.asset", icon, null);
    }

}
