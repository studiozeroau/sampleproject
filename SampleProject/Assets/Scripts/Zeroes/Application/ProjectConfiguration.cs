﻿using System.Collections;
using System.Collections.Generic;

using UnityEditor;
using UnityEngine;
using UnityEngine.Audio;

public class ProjectConfiguration : ScriptableObject
{
    [Header("Application")]
    [SerializeField]
    private string _GameManager = string.Empty;
    public string GameManager => _GameManager;

    [Header("Screen")]
    [SerializeField]
    private int _ReferenceResolutionWidth = 1280;
    public int ReferenceResolutionWidth => _ReferenceResolutionWidth;

    [SerializeField]
    private int _ReferenceResolutionHeight = 720;
    public int ReferenceResolutionHeight => _ReferenceResolutionHeight;

    [Header("Accessibility")]
    [SerializeField]
    private Font _DyslexicFont = null;
    public Font DyslexicFont => _DyslexicFont;

    [Header("Audio")]
    [SerializeField, InspectorExpandable("Audio Configuration")]
    private AudioConfiguration _AudioConfiguration = null;
    public AudioConfiguration AudioConfiguration => _AudioConfiguration;

    [Header("Assets")] 
    [SerializeField, InspectorExpandable] private AssetCollectionNode _rootAssetCollection;
    public AssetCollectionNode RootAssetCollection => _rootAssetCollection;
    
    public static ProjectConfiguration Find()
    {
#if UNITY_EDITOR
        string[] assetguids = AssetDatabase.FindAssets("t: ProjectConfiguration");
        if (assetguids.Length > 0)
        {
            string assetPath = AssetDatabase.GUIDToAssetPath(assetguids[0]);
            return AssetDatabase.LoadAssetAtPath<ProjectConfiguration>(assetPath);
        }
#endif

        return null;
    }

}
