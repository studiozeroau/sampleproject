﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextureUtilities
{
    // Makes a texture from a colour with a width and height
    public static Texture2D MakeTexture(int width, int height, Color colour)
    {
        Color[] pixels = new Color[width * height];
        for (int i = 0; i < pixels.Length; ++i)
        {
            pixels[i] = colour;
        }

        Texture2D texture = new Texture2D(width, height);
        texture.SetPixels(pixels);
        texture.Apply();

        return texture;
    }
}
