﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

// Generic Object Pooling class
namespace Zeroes.Utils
{
	public class ObjectPool<T>
	{
		// The Pool is a Queue of items
		protected Queue<T> Pool;

		// Creates the item for the pool
		protected Func<T> ObjectGenerator;

		public ObjectPool(int size, Func<T> objectGenerator)
		{
			if (objectGenerator != null)
			{
				BuildPool(size, objectGenerator);
			}
		}

		protected void BuildPool(int size, Func<T> objectGenerator)
		{
			ObjectGenerator = objectGenerator;

			Pool = new Queue<T>();
			for (int i = 0; i < size; i++)
			{
				Pool.Enqueue(ObjectGenerator());
			}
		}

		// Request an item from the pool if it exists
		// If pool is empty, we add a new one to it
		public virtual T Request()
		{
			if (Pool.Count > 0)
			{
				return Pool.Dequeue();
			}
			return ObjectGenerator();
		}

		// Returns an item to the pool if the pool does not contain it already
		public virtual void Return(T obj)
		{
			if (!Pool.Contains(obj))
			{
				Pool.Enqueue(obj);
			}
		}
	}
}