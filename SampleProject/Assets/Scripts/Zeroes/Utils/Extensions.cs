﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

using System;
using System.Collections;
using System.Collections.Generic;


public static class Extensions
{

    #region GAMEOBJECT

    // Recursively set the layer of a gameobject to ensure the whole object is accounted for
    public static void SetLayer(this GameObject target, int layer)
    {
        target.layer = layer;
        foreach (Transform child in target.transform)
        {
            child.gameObject.SetLayer(layer);
        }
    }

    // Shortcut for moving an object to a scene 
    public static void SetScene(this GameObject target, Scene scene)
    {
        SceneManager.MoveGameObjectToScene(target, scene);
    }

    // Can't figbure out how to do proper [] indexing for GameObjects so this'll have to do as a nice wrapper
    public static GameObject Get(this GameObject go, string name)
    {
        Transform child = go.transform.Find(name);
        return child != null ? child.gameObject : null;
    }

    // Find a child multiple objects deep
    public static GameObject Get(this GameObject go, params string[] children)
    {
        Transform child = go.transform;
        foreach (var c in children)
        {
            child = child.transform.Find(c);
            if (child == null)
            {
                return null;
            }
        }
        return child.gameObject;
    }

    // Gets the bounds of a gameObject based on its renderer and all child renderers
    public static Bounds GetBounds(this GameObject go)
    {
        Renderer[] renderers = go.GetComponentsInChildren<Renderer>();
        if (renderers.Length == 0)
        {
            return new Bounds();
        }

        Bounds bounds = renderers[0].bounds;
        for (int i = 1; i < renderers.Length; i++)
        {
            bounds.Encapsulate(renderers[i].bounds);
        }
        return bounds;
    }

    // Gets screen rect of object
    // Original Src: https://answers.unity.com/questions/49943/is-there-an-easy-way-to-get-on-screen-render-size.html
    // I have now modified this to better work out the size
    public static Rect GUIRect(this GameObject go)
    {
        Bounds bounds = GetBounds(go);

        Camera camera = Camera.main;

        Vector3 cen = bounds.center;
        Vector3 ext = bounds.extents;
        Vector2[] extentPoints = new Vector2[8]
        {
         camera.WorldToScreenPoint(new Vector3(cen.x-ext.x, cen.y-ext.y, cen.z-ext.z)),
         camera.WorldToScreenPoint(new Vector3(cen.x+ext.x, cen.y-ext.y, cen.z-ext.z)),
         camera.WorldToScreenPoint(new Vector3(cen.x-ext.x, cen.y-ext.y, cen.z+ext.z)),
         camera.WorldToScreenPoint(new Vector3(cen.x+ext.x, cen.y-ext.y, cen.z+ext.z)),
         camera.WorldToScreenPoint(new Vector3(cen.x-ext.x, cen.y+ext.y, cen.z-ext.z)),
         camera.WorldToScreenPoint(new Vector3(cen.x+ext.x, cen.y+ext.y, cen.z-ext.z)),
         camera.WorldToScreenPoint(new Vector3(cen.x-ext.x, cen.y+ext.y, cen.z+ext.z)),
         camera.WorldToScreenPoint(new Vector3(cen.x+ext.x, cen.y+ext.y, cen.z+ext.z))
        };
        Vector2 min = extentPoints[0];
        Vector2 max = extentPoints[0];
        foreach (Vector2 v in extentPoints)
        {
            min = Vector2.Min(min, v);
            max = Vector2.Max(max, v);
        }
        return new Rect(min.x, min.y, max.x - min.x, max.y - min.y);
    }

    public static void ConformToScreen(this GameObject gameObject)
    {
        Rect rect = gameObject.GUIRect();
        if (rect.height > rect.width)
        {
            gameObject.transform.localScale *= (Camera.main.pixelHeight / rect.height);
        }
        else
        {
            gameObject.transform.localScale *= (Camera.main.pixelWidth / rect.width);
        }
    }

    #endregion

    #region TRANSFORM

    // The first child in a transform hierarchy
    public static Transform FirstChild(this Transform transform)
    {
        return transform.GetChild(0);
    }

    // The last child in a transform hierarchy
    public static Transform LastChild(this Transform transform)
    {
        return transform.GetChild(transform.childCount - 1);
    }

    #endregion

    #region VECTOR_EXTENSIONS

    //Returns an absolute value for a Vector
    public static Vector3 Abs(this Vector3 v)
    {
        v.x = Mathf.Abs(v.x);
        v.y = Mathf.Abs(v.y);
        v.z = Mathf.Abs(v.z);

        return v;
    }

    #endregion

    #region LIST_EXTENSIONS

    //Returns last index of a list
    public static int Last<T>(this List<T> list)
    {
        return list.Count - 1;
    }
    //Returns the last element of a List
    public static T LastElement<T>(this List<T> list)
    {
        return list[list.Last()];
    }

    // Gets a random element from a list
    public static T RandomElement<T>(this List<T> list)
    {
        return list[UnityEngine.Random.Range(0, list.Count)];
    }

    #endregion

    #region ARRAY_EXTENSIONS

    // Returns last index of an Array
    public static int Last<T>(this T[] array)
    {
        return array.Length - 1;
    }
    // Returns the last element of an Array
    public static T LastElement<T>(this T[] array)
    {
        return array[array.Last()];
    }

    // Does this array contain this object
    public static bool Contains<T>(this T[] array, T item)
    {
        return Array.IndexOf(array, item) >= 0;
    }

    // Gets a random element from an array
    public static T RandomElement<T>(this T[] array)
    {
        return array[UnityEngine.Random.Range(0, array.Length)];
    }

    public static int IndexOf<T>(this T[] array, T item)
    {
        return Array.IndexOf(array, item);
    }

    // Swaps two indices of an array
    public static void Swap<T>(this T[] array, int a, int b)
    {
        T temp = array[a];
        array[a] = array[b];
        array[b] = temp;
    }

    public static void ChangeIndex<T>(this T[] array, int previousIndex, int newIndex)
    {
        if (array.Length < previousIndex + 1 || previousIndex < 0 || array.Length < newIndex + 1 || newIndex < 0)
        {
            throw new IndexOutOfRangeException();
		}

        T temp = array[previousIndex];

		if (newIndex < previousIndex)
        {
            for (int i = previousIndex + 1; i < newIndex; i++)
            {
                array[i - 1] = array[i];
            }
        }
        else
        {
            for (int i = newIndex; i > previousIndex; i--)
            {
                array[i] = array[i - 1];
            }
        }

        array[newIndex] = temp;
	}

    public static string[] ToStringArray(this object[] array)
    {
        string[] stringArray = new string[array.Length];
        for (int i = 0; i < stringArray.Length; i++)
        {
            if (array[i] == null)
            {
                stringArray[i] = "None";
            }
            else
            {
                stringArray[i] = array[i].ToString().Replace("ShopDrifter.", "");
            }
        }
        return stringArray;
    }

    #endregion

    #region SPRITE

    // Sets a new sprite on an image and preserves the original size
    public static Image SetAndPreserveAspec(this Image image, Sprite newSprite)
    {
        // Store the original bounds
        Rect originalBounds = image.rectTransform.rect;

        // Apply the sprite and update the size
        image.sprite = newSprite;
        image.SetNativeSize();

        // Get the new bounds and return it to the original aspect ratio
        Rect newBounds = image.rectTransform.rect;
        float diff = 0;
        if (originalBounds.width < originalBounds.height)
        {
            diff = newBounds.width / originalBounds.width;
        }
        else
        {
            diff = newBounds.height / originalBounds.height;
        }
        Vector2 size = image.rectTransform.sizeDelta;
        size.x /= diff;
        size.y /= diff;
        image.rectTransform.sizeDelta = size;

        return image;
    }

    #endregion

    #region CAMERA

    // Sets a Camera as the main camera
    public static void SetAsMain(this Camera camera)
    {
        camera.tag = "MainCamera";
        camera.gameObject.SetActive(true);
    }

    // Removes a camera as the main camera
    public static void RemoveAsMain(this Camera camera)
    {
        camera.tag = "Untagged";
        camera.gameObject.SetActive(false);
    }

    // Creates a mesh based on the view frustum
    public static Mesh MeshFromCameraFrustum(this Camera camera)
    {
        var frustumHeight = 2.0f * camera.farClipPlane * Mathf.Tan(camera.fieldOfView * 0.5f * Mathf.Deg2Rad);
        var frustumWidth = frustumHeight * camera.aspect;
        
        Vector3 topLeft = Vector3.forward * camera.farClipPlane - Vector3.right * frustumWidth * .5f + Vector3.up * frustumHeight * .5f;
        Vector3 topRight = Vector3.forward * camera.farClipPlane + Vector3.right * frustumWidth * .5f + Vector3.up * frustumHeight * .5f;
        Vector3 bottomleft = Vector3.forward * camera.farClipPlane - Vector3.right * frustumWidth * .5f - Vector3.up * frustumHeight * .5f;
        Vector3 bottomRight = Vector3.forward * camera.farClipPlane + Vector3.right * frustumWidth * .5f - Vector3.up * frustumHeight * .5f;
        
        Mesh newMesh = new Mesh();
        newMesh.SetVertices(new List<Vector3>()
        {
            Vector3.zero,
            topLeft,
            topRight,
            bottomleft,
            bottomRight
        });
        newMesh.SetTriangles(new int[]
        {
            0, 1, 2,
            0, 1, 3,
            0, 2, 4,
            0, 3, 4,
            1, 2, 3,
            2, 3, 4
        }, 0);

        return newMesh;
    }

    #endregion

    #region TIMESPAN

    // Formats a timespan the way I want it for UI display
    // TODO. How does this factor into localisation?
    public static string FormattedForDisplay(this TimeSpan timeSpan)
    {
        if(timeSpan.Days > 0)
        {
            return $"{timeSpan.Days}d, {timeSpan.Hours}h, {timeSpan.Minutes}m";
        }
        else if (timeSpan.Hours > 0)
        {
            return $"{timeSpan.Hours}h, {timeSpan.Minutes}m, {timeSpan.Seconds}s";
        }
        else if (timeSpan.Minutes > 0)
        {
            return $"{timeSpan.Minutes}m, {timeSpan.Seconds}s";
        }
        return $"{timeSpan.Seconds}s";
    }

    #endregion
    
    #region STRING

    // Removes the last character in a string
    public static string RemoveLast(this string str)
    {
        if(str.Length == 0)
        {
            return str;
        }
        return str.Remove(str.Length - 1);
    }

    // HTML Colour a string
    public static string Colour(this string str, Color colour)
    {
        return $"<color=#{ColorUtility.ToHtmlStringRGB(colour)}>{str}</color>";
    }

    public static string Colour(this string str, string hexColour)
    {
        if (ColorUtility.TryParseHtmlString(hexColour, out Color color))
        {
            return str.Colour(color);
        }
        return str;
    }

    // HTML Bold a string
    public static string Bold(this string str)
    {
        return $"<b>{str}</b>";
    }

    // HTML Italic a string
    public static string Italic(this string str)
    {
        return $"<i>{str}</i>";
    }

    public static string NewLine(this string str)
    {
        return $"{str}\n";
    }

    #endregion

    #region ANIMATOR

    // Sets a offset for the current animation
    public static void SetAnimationOffset(this Animator animator, float offset)
    {
        animator.CrossFade(animator.GetCurrentAnimatorStateInfo(0).shortNameHash, 0, 0, offset);
    }

    // Set a random offset on the current animation
    public static void RandomiseAnimationOffset(this Animator animator)
    {
        animator.SetAnimationOffset(UnityEngine.Random.Range(0f, 1f));
    }

    #endregion

    #region TEXT

    public static Vector2 AnchorForAlignment(this Text text)
    {
        switch (text.alignment)
        {
            case TextAnchor.MiddleLeft:
                return new Vector2(-.5f, 0);
            case TextAnchor.MiddleRight:
                return new Vector2(.5f, 0);
            case TextAnchor.LowerCenter:
                return new Vector2(0, -.5f);
            case TextAnchor.LowerLeft:
                return new Vector2(-.5f, -.5f);
            case TextAnchor.LowerRight:
                return new Vector2(.5f, -.5f);
            case TextAnchor.UpperCenter:
                return new Vector2(0, .5f);
            case TextAnchor.UpperLeft:
                return new Vector2(-.5f, .5f);
            case TextAnchor.UpperRight:
                return new Vector2(.5f, .5f);
        }
        return new Vector2(0, 0);
    }

    #endregion

    #region MASKABLE GRAPHIC

    public static void Skew(this MaskableGraphic graphic, VertexHelper vh, RectTransform rectTransform, Vector2 skew, Vector2 localSkew, Vector2 pivot)
    {
        var height = rectTransform.rect.height;
        var width = rectTransform.rect.width;

        skew.x *= Mathf.Deg2Rad;
        skew.y *= Mathf.Deg2Rad;
        //localSkew.x *= Mathf.Deg2Rad;
        //localSkew.y *= Mathf.Deg2Rad;

        UIVertex v = new UIVertex();
        for (int i = 0; i < vh.currentVertCount; i++)
        {
            Vector3 offset = new Vector3();
            vh.PopulateUIVertex(ref v, i);
            
            if (Mathf.Abs(skew.x) > 0)
            {
                float horizontalSkew = v.position.y - pivot.y * height;
                offset.x += Mathf.Tan(skew.x) * horizontalSkew;
            }

            if (Mathf.Abs(skew.y) > 0)
            {
                float verticalSkew = v.position.x - pivot.x * width;
                offset.y += Mathf.Tan(skew.y) * verticalSkew;
            }

            if(Mathf.Abs(localSkew.x) > 0)
            {
                float dir = v.position.y > 0f ? 1f : -1f;
                offset.x += Mathf.Cos(skew.y) * localSkew.x * dir;
                offset.y += Mathf.Sin(skew.y) * localSkew.x * dir;
            }

            if (Mathf.Abs(localSkew.y) > 0)
            {
                float dir = v.position.x > 0f ? 1f : -1f;
                offset.x -= Mathf.Cos(Mathf.PI * .5f + skew.x) * localSkew.y * dir;
                offset.y += Mathf.Sin(Mathf.PI * .5f + skew.x) * localSkew.y * dir;
            }

            v.position += offset;
            vh.SetUIVertex(v, i);
        }
    }

    #endregion
}
