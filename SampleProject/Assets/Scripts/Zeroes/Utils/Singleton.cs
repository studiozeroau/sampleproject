using System;
using UnityEngine;

// Generic singleton for regular classes
public abstract class Singleton<T> where T: new()
{
    private static T _instance;
    public static T Instance
    {
        get
        {
            if (_instance == null)
                _instance = new T();
            return _instance;
        }
    }
}

// Singleton for MonoBehaviours
public abstract class MonoSingleton<T> : MonoBehaviour where T : MonoBehaviour
{
    private static T _instance;
    public static T Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<T>();;
            return _instance;
        }
    }

    public virtual void Awake()
    {
        if (Instance != this)
        {
            Destroy(gameObject);
            DontDestroyOnLoad(gameObject);            
        }
    }
    
    public virtual void Start() { }

}