﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(InspectorExpandable))]
public class InspectorExpandableDrawer : PropertyDrawer
{
    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return EditorGUIUtility.singleLineHeight;
    }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        InspectorExpandable expandable = (attribute as InspectorExpandable);
        
        GUIStyle textureButton = new GUIStyle(GUI.skin.button)
        {
            border = new RectOffset(0, 0, 0, 0),
            margin = new RectOffset(0, 0, 0, 0),
            padding = new RectOffset(0, 0, 0, 0),
            fixedHeight = EditorGUIUtility.singleLineHeight,
            fixedWidth = EditorGUIUtility.singleLineHeight + 10
        };

        // Draw the property
        Rect propertyRect = position;
        propertyRect.width -= textureButton.fixedWidth * 1.5f;
        propertyRect.height = EditorGUIUtility.singleLineHeight;

        // Label
        string name = expandable.Label.Equals(string.Empty) ? property.displayName : expandable.Label;
        
        EditorGUI.PropertyField(propertyRect, property, new GUIContent(name));
        
        GUI.enabled = property.objectReferenceValue != null;

        Texture2D expandTexture = property.isExpanded ? EditorGUIUtility.FindTexture("CollabPush") : EditorGUIUtility.FindTexture("CollabPull");

        // Draw the expand button
        Rect buttonRect = new Rect(propertyRect.x + propertyRect.width + textureButton.fixedWidth * .5f, propertyRect.y, textureButton.fixedWidth, textureButton.fixedHeight);
        if (GUI.Button(buttonRect, expandTexture, textureButton))
        {
            property.isExpanded = !property.isExpanded;
        }
        GUI.enabled = true;

        // Draw the expanded object where applicable
        if(property != null && property.objectReferenceValue != null && property.isExpanded)
        {
            // Draw a nested inspector
            EditorGUI.indentLevel++;

            GUIStyle box = new GUIStyle(EditorStyles.helpBox);
                
            RectOffset margin = box.margin;
            margin.left = EditorGUI.indentLevel * (int)EditorGUIUtility.singleLineHeight;

            EditorGUILayout.BeginVertical(box);
            int cachedIndent = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;

            SerializedObject obj = new SerializedObject(property.objectReferenceValue);
            Editor editor = Editor.CreateEditor(obj.targetObject);
            editor.OnInspectorGUI();

            EditorGUI.indentLevel = cachedIndent;
            EditorGUILayout.EndVertical();
            EditorGUI.indentLevel--;
        }
    }
}