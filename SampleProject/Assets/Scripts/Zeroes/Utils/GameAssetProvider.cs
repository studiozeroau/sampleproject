﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

// Wrapper for multiple Object Pools
namespace Zeroes.Utils
{
	public class GameAssetProvider
	{
        // What order does the provider provide assets
        [Serializable]
        public enum ProviderOrder
        {
            Random = 0,
            Linear = 1
        }


		// The list of object pools
		private Dictionary<string, GameObjectPool> Pools;

		// Cache a list of the IDs for easy access
		private List<string> CachedIDs = new List<string>();

        // How does this provider provide assets? Default is Random
        private ProviderOrder Order;

        // Cache the most recent provider index
        private int MostRecentID = 0;
        
		public GameAssetProvider(ProviderOrder order = ProviderOrder.Random)
		{
            Order = order;
			Pools = new Dictionary<string, GameObjectPool>();
		}

		// Adds a pool to the provider list
		public void Add(string id, GameObjectPool newPool)
		{
			Pools.Add(id, newPool);
			CachedIDs.Add(id);
		}
		
		// Request a random item from the pools
		public virtual GameObject Request()
		{
			if (CachedIDs.Count == 0)
			{
				return null;
			}

            string id = string.Empty;
            switch(Order)
            {
                case ProviderOrder.Random:
                    id = CachedIDs.RandomElement();
                    break;

                case ProviderOrder.Linear:
                    id = CachedIDs[MostRecentID];
                    MostRecentID++;
                    if(MostRecentID == CachedIDs.Count)
                    {
                        MostRecentID = 0;
                    }
                    break;
            }

			return Request(id);
		}

		// Requests a specific asset pool
		public virtual GameObject Request(string id)
		{
			if (Pools.Count == 0)
			{
				return null;
			}
            
			return Pools[id].Request();
		}

		// Return an item to its respective pool
		public virtual void Return(GameObject obj)
		{
			Pools[obj.name].Return(obj);
		}

		// Does this asset provider contain the object?
		public virtual bool Contains(GameObject obj)
		{
			return CachedIDs.Contains(obj.name);
		}


	}
}