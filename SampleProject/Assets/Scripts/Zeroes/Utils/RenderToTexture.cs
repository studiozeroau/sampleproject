﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zeroes.UI;

[RequireComponent(typeof(Camera))]
public class RenderToTexture : MonoBehaviour
{
    // Scale the render down to a lower resolution if desired
    [SerializeField, Range(.05f, 1f)]
    private float RenderScale = 1f;

    // The camera rendering to the texture
    private Camera RenderCamera = null;
    
    // The texture this camera renders to
    private RenderTexture RenderTexture = null;

    public void Awake()
    {
        RenderCamera = GetComponent<Camera>();
        RenderCamera.clearFlags = CameraClearFlags.SolidColor;
        RenderCamera.backgroundColor = new Color(0, 0, 255, 0);
        RenderCamera.gameObject.SetActive(false);
    }

    public void BeginRender(RawImage target)
    {
        if(RenderTexture != null)
        {
            Debug.Log("Texture is already being rendered, check your order of operations!".Colour(Color.red));
            return;
        }
        RenderCamera.gameObject.SetActive(true);

        RectTransform targetRectTransform = target.GetComponent<RectTransform>();

        // Create a render texture the size of the desired target
        RenderTexture = new RenderTexture((int)(targetRectTransform.rect.width * RenderScale), (int)(targetRectTransform.rect.height * RenderScale), 24)
        {
            antiAliasing = 8,
            format = RenderTextureFormat.ARGB32,
            filterMode = FilterMode.Bilinear,
            name = $"{gameObject.name}_RenderTexture"
        };
        RenderCamera.targetTexture = RenderTexture;

        target.texture = RenderTexture;
    }

    public void EndRender()
    {
        RenderCamera.targetTexture = null;
        RenderCamera.gameObject.SetActive(false);

        if (RenderTexture != null)
        {
            RenderTexture.DiscardContents();
            Destroy(RenderTexture);
            RenderTexture = null;
        }
    }

    public void OnDestroy()
    {
        EndRender();
    }

}
