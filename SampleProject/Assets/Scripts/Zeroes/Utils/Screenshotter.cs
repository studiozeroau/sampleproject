﻿using System;
using UnityEngine;
using System.Collections;

public class Screenshotter : MonoBehaviour
{

    [Range(1, 4), SerializeField]
	private int ResolutionMultiplier = 1;
    
	private const string FileType = ".png";
    
    // Overload
	public IEnumerator TakeScreenshot(Camera camera, int width, int height, string path, string filename)
	{
        yield return StartCoroutine(DoScreenshot(camera, width, height, path, filename));
	}

    // Take Screenshot
	public IEnumerator DoScreenshot(Camera camera, int width, int height, string path, string filename)
	{
		string fullfilepath = $"{path}/{filename}{FileType}";

        // Create directory if it does not exist
        if (System.IO.Directory.Exists(path) == false)
        {
            System.IO.Directory.CreateDirectory(path);
        }

        // Remove old image if it exists
        if (System.IO.File.Exists(fullfilepath))
		{
			System.IO.File.Delete(fullfilepath);
		}
        
		QualitySettings.antiAliasing = 4;

        width *= ResolutionMultiplier;
        height *= ResolutionMultiplier;

		RenderTexture RT = new RenderTexture(width, height, 24)
		{
			antiAliasing = 8,
			format = RenderTextureFormat.ARGB32,
			filterMode = FilterMode.Bilinear
		};

		camera.targetTexture = RT;
		camera.clearFlags = CameraClearFlags.Depth;
		camera.Render();

		RenderTexture.active = RT;
		
		Texture2D output = new Texture2D(width, height, TextureFormat.ARGB32, false);

		output.ReadPixels(new Rect(0, 0, RT.width, RT.height), 0, 0);
		output.Apply();

		camera.targetTexture = null;
		RenderTexture.active = null;

		yield return new WaitForEndOfFrame();

		byte[] ImgBytes = ImageConversion.EncodeToPNG(output);
        
		System.IO.File.WriteAllBytes(fullfilepath, ImgBytes);
		
		QualitySettings.antiAliasing = 0;

		camera.clearFlags = CameraClearFlags.Skybox;

		System.Array.Clear(ImgBytes, 0, ImgBytes.Length);
		ImgBytes = null;

		if (output)
		{
			DestroyImmediate(output);
			output = null;
		}

		if (RT)
		{
			DestroyImmediate(RT);
			RT = null;
		}
	}

}
