﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.SceneManagement;

// GameObject specific pooling class
namespace Zeroes.Utils
{
	public class GameObjectPool : ObjectPool<GameObject>
	{
		// Container to wrap and orangise all game objects from the pool
		private Transform Container;

		// Cache the name of the object in the pool
		protected string PooledObject;

		public GameObjectPool(int size, GameObject asset, Scene scene) : base(size, null)
		{
			PooledObject = asset.name;

			// Create a container to wrap everything and keep the hierarchy clean
			Container = new GameObject().transform;
			Container.name = asset.name + " Pool";
			Container.transform.position = Vector3.one * 10000;
			Container.gameObject.SetScene(scene);

			// Build pools separately
			BuildPool(size, () =>
			{
				GameObject obj = GameObject.Instantiate(asset);
				obj.name = obj.name.Remove(obj.name.IndexOf("(Clone)"));
				obj.transform.SetParent(Container);
				obj.transform.localPosition = Vector3.zero;
				obj.SetActive(false);
				return obj;
			});
		}

		// Request an item from the pool if it exists
		// If pool is empty, we add a new one to it
		public override GameObject Request()
		{
			GameObject obj = base.Request();
			obj.SetActive(true);
			obj.transform.SetParent(null);
			return obj;
		}

		// Returns an item to the pool if the pool does not contain it already
		public override void Return(GameObject obj)
		{
			obj.transform.SetParent(Container);
			obj.transform.localPosition = Vector3.zero;
			obj.SetActive(false);
			base.Return(obj);
		}

		// Does this GameObject belong to this object pool?
		public virtual bool Contains(GameObject obj)
		{
			return obj.name.Equals(PooledObject);
		}
	}
}