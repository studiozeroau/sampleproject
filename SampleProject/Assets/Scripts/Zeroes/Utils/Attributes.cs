﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEditor;

// Used for drawing nested inspectors
public class InspectorExpandable : PropertyAttribute
{
    public string Label = string.Empty;

    public InspectorExpandable() { }

    public InspectorExpandable(string label)
    {
        Label = label;
    }
}

// Marks a field as read only in the editor
public class EditorReadOnly : PropertyAttribute { }

namespace Zeroes.Editor
{
    // Adds a button to a method
    public class Button : Attribute
    {
        public readonly string Label;
        
        public Button() { }
        
        public Button(string label)
        {
            Label = label;
        }
    }    
}
