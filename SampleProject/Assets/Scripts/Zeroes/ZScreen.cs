﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZScreen
{
    private static float WidthScaleFactor = 0;
    private static float HeightScaleFactor = 0;

    //Width and height of the screen in pixels
    public static float Width { private set; get; }
    public static float Height { private set; get; }

    //Width and height of the UI canvas
    //This is the co-ordinates system used for touch areas
    public static float CanvasWidth { private set; get; }
    public static float CanvasHeight { private set; get; }

    //Safe area of the screen in pixels
    public static Rect SafeArea { private set; get;  }
    public static float SafeAreaOffset_Left { private set; get; }
    public static float SafeAreaOffset_Right { private set; get; }
    public static float SafeAreaOffset_Top{ private set; get; }
    public static float SafeAreaOffset_Bottom { private set; get; }

    //Safe area of the screen in UI Canvas Units
    public static Rect CanvasSafeArea { private set; get; }
    public static float CanvasSafeAreaOffset_Left { private set; get; }
    public static float CanvasSafeAreaOffset_Right { private set; get; }
    public static float CanvasSafeAreaOffset_Top { private set; get; }
    public static float CanvasSafeAreaOffset_Bottom { private set; get; }

    public ZScreen(float baseCanvasWidth)
    {
        Width = Screen.width;
        Height = Screen.height;

        CanvasWidth = baseCanvasWidth;
        CanvasHeight = CanvasWidth * (Height / Width);

        WidthScaleFactor = baseCanvasWidth / Width;
        HeightScaleFactor = CanvasHeight / Height;

        //Store safe area values in pixels
        SafeArea = Screen.safeArea;
        SafeAreaOffset_Left = SafeArea.x;
        SafeAreaOffset_Right = Width - SafeArea.x - SafeArea.width;
        SafeAreaOffset_Bottom = SafeArea.y;
        SafeAreaOffset_Top = Height - SafeArea.y - SafeArea.height;

        //Store safe area values in canvas space
        CanvasSafeArea = new Rect(
            SafeArea.x * WidthScaleFactor,
            SafeArea.y * HeightScaleFactor,
            SafeArea.width * WidthScaleFactor,
            SafeArea.height * HeightScaleFactor);
        
        CanvasSafeAreaOffset_Left = SafeAreaOffset_Left * WidthScaleFactor;
        CanvasSafeAreaOffset_Right = SafeAreaOffset_Right * WidthScaleFactor;
        CanvasSafeAreaOffset_Bottom = SafeAreaOffset_Bottom * HeightScaleFactor;
        CanvasSafeAreaOffset_Top = SafeAreaOffset_Top * HeightScaleFactor;
    }

    /// <summary>
    /// Performs several methods combined as a shortcut to get a canvas pos from a raw screen pos
    /// </summary>
    public static Vector2 ScreenToCanvas(Vector2 pixelPosition)
    {
        return CanvasPositionFromNormalizedScreenPosition(NormalizedScreenPosition(pixelPosition));
    }

	/// <summary>
	/// Performs several methods combined as a shortcut to get a raw screen pos from a canvas position
	/// </summary>
	public static Vector2 CanvasToScreen(Vector2 canvasPosition)
    {
        return ScreenPositionFromNormalizedCanvasPosition(NormalizedCanvasPosition(canvasPosition));
    }

    /// <summary>
    /// Returns a normalized vector2 based on a raw pixel position
    /// </summary>
    public static Vector2 NormalizedScreenPosition(Vector2 pixelPosition)
    {
        pixelPosition.x /= Width;
        pixelPosition.y /= Height;

        return pixelPosition;
    }

	/// <summary>
	/// Returns a normalized vector2 based on a canvas position
	/// </summary>
    public static Vector2 NormalizedCanvasPosition(Vector2 canvasPosition)
    {
        canvasPosition.x /= CanvasWidth;
        canvasPosition.y /= CanvasHeight;

        return canvasPosition;
    }


    /// <summary>
    /// Returns a co-ordinate in canvas space from a normalized screen position
    /// </summary>
    public static Vector2 CanvasPositionFromNormalizedScreenPosition(Vector2 normalizedPosition)
    {
        normalizedPosition.x *= CanvasWidth;
        normalizedPosition.y *= CanvasHeight;

        return normalizedPosition;
    }

    /// <summary>
    /// Returns a co-ordinate in screen space from a normalized screen position
    /// </summary>
    public static Vector2 ScreenPositionFromNormalizedCanvasPosition(Vector2 normalizedPosition)
    {
        normalizedPosition.x *= Width;
        normalizedPosition.y *= Height;

        return normalizedPosition;
    }
}
