﻿using System;
using System.IO;

using UnityEngine;
using UnityEditor;
using UnityEditor.ProjectWindowCallback;
using System.Diagnostics;

namespace Zeroes.Editor
{
	public class Utilities
	{

		#region GAME OBJECTS

		// Creates and returns a new Game Object for Editor use
		public static GameObject NewGameObject()
		{
			GameObject newGameObject = new GameObject();
			newGameObject.transform.SetParent(Selection.activeTransform);
			if (newGameObject.transform.parent != null)
			{
				newGameObject.transform.localPosition = Vector3.zero;
			}
			newGameObject.transform.localScale = Vector3.one;
			Selection.activeObject = newGameObject.transform;
			Undo.RegisterCreatedObjectUndo(newGameObject, newGameObject.GetInstanceID().ToString());
			return newGameObject;
		}

		// Overload to create a new game object with a specified name
		public static GameObject NewGameObject(string name)
		{
			GameObject newGameObject = NewGameObject();
			newGameObject.name = name;
			return newGameObject;
		}

		#endregion



		#region UI GAME OBJECTS

		// Make a UI specific game object
		public static GameObject NewUIGameObject()
		{
			GameObject uiGameObject = NewGameObject();
			RectTransform rect = uiGameObject.AddComponent<RectTransform>();
			rect.anchoredPosition = Vector2.zero;
			uiGameObject.AddComponent<CanvasRenderer>();
			return uiGameObject;
		}

		// Make a UI specific game object with a name
		public static GameObject NewUIGameObject(string name)
		{
			GameObject uiGameObject = NewUIGameObject();
			uiGameObject.name = name;
			return uiGameObject;
		}

		#endregion

		// Gets the current path of the project window
		public static string GetCurrentProjectWindowPath()
		{
			string path = "Assets";
			foreach (UnityEngine.Object obj in Selection.GetFiltered(typeof(UnityEngine.Object), SelectionMode.Assets))
			{
				path = AssetDatabase.GetAssetPath(obj);
				if (File.Exists(path))
				{
					path = Path.GetDirectoryName(path);
					break;
				}
			}
			return path;
		}

		// Open the folder for Application.persistentDataPath
		[MenuItem("Zeroes/Open Persistent Data Path")]
		public static void OpenPersistentDataPath()
		{
			string path = Application.persistentDataPath;
			Process.Start(path);
		}

		// Delete all data in Application.persistentDataPath
		[MenuItem("Zeroes/Delete Persistent Data Path Files")]
		public static void DeletePersistentData()
        {
			string path = Application.persistentDataPath;
			if (EditorUtility.DisplayDialog("Are you sure?", $"Are you sure you would like to delete the files in the directory {path}", "Yes", "No"))
			{
				string[] files = Directory.GetFiles(path);
				foreach (var file in files)
				{
					try
					{
						File.Delete(file);
						Zebug.Log($"Successfully deleted file: {file}".Colour(Color.green));
					}
					catch
					{
						Zebug.Log($"Failed to delete file: {file}".Colour(Color.red));
					}
				}
			}
        }

	}

	// Generic EndNameEditActionCallback to avoid making one for every single editor action we want to make
	public class EndNameEditActionCallback : EndNameEditAction
    {

        public Action<int, string, string> OnComplete;
        public Action<int, string, string> OnCancelled;

        public override void Action(int instanceId, string pathName, string resourceFile)
        {
            OnComplete?.Invoke(instanceId, pathName, resourceFile);
        }

        public override void Cancelled(int instanceId, string pathName, string resourceFile)
        {
            OnCancelled?.Invoke(instanceId, pathName, resourceFile);
        }
    }
}
