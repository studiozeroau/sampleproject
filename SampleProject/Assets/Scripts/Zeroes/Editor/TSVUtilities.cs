﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Zeroes
{
	public static class TSVUtilities
	{

		public static Dictionary<string, string> ParseTSV(string tsv)
		{
			//Split up the TSV into the different rows
			string[] rows = tsv.Split(new[] { Environment.NewLine }, StringSplitOptions.None);

			Dictionary<string, string> content = new Dictionary<string, string>();

			for (int i = 0; i < rows.Length; i++)
			{
				string r = rows[i];
				string[] columns = r.Split('\t');

                // DO not add empty columns or keys that already exist
                if(columns[0].Equals(String.Empty) == false && content.ContainsKey(columns[0]) == false)
                {
                    content.Add(columns[0], columns[1]);
                }
			}

			return content;
		}

		//Parses a TSV file and returns JSON
		public static string ToJSON(string tsv)
		{
			Dictionary<string, string> content = ParseTSV(tsv);

			string json = JsonConvert.SerializeObject(content);

			return json;
		}

	}
}
