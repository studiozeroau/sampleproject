using UnityEditor;

namespace Zeroes.Editor
{
    public class SetDirtyContextMenu
    {
        [MenuItem("CONTEXT/Object/Set Dirty")]
        public static void SetGameObjectDirty()
        {
            if (Selection.activeObject != null)
            {
                EditorUtility.SetDirty(Selection.activeObject);
            }
        }
    }
}