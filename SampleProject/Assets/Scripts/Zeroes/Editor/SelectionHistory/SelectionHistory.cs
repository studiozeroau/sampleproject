﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEditor;

using Zeroes.Utils;
using Zeroes;

// Stores instance IDs for selections

[InitializeOnLoad]
public static class SelectionHistory
{

	// Directly mapped to the Session state for ease of use
	private const string kSelectionIndex = "kSelectionIndex";
	public static int SelectionIndex { private set; get; }

	// Directly mapped to the Session state for ease of use
	private const string kHistory = "kSelectionHistory";
	public static List<int> History { private set; get; }

	static SelectionHistory()
	{
		EditorApplication.update += Update;

		History = SessionState.GetIntArray(kHistory, new int[0]).ToList();
		SelectionIndex = SessionState.GetInt(kSelectionIndex, 0);
	}

	static void Update()
	{
		if (Selection.activeObject != null)
		{
			int instance = Selection.activeInstanceID;
			
			if (History.Count == 0 || History[SelectionIndex].Equals(instance) == false)
			{
				if (History.Count > SelectionIndex + 1)
				{
					History.RemoveRange(SelectionIndex + 1, History.Count - SelectionIndex - 1);
				}
				NewSelection(instance);
			}
		}
	}

	public static void NewSelection(int instance)
	{
		History.Add(instance);
		SelectionIndex = History.Count - 1;

		UpdateValues();
	}

	public static void SelectPrevious()
	{
		if (SelectionIndex > 0)
		{
			SelectionIndex--;
			Selection.activeObject = EditorUtility.InstanceIDToObject(History[SelectionIndex]);

			UpdateValues();
		}
	}

	public static void SelectNext()
	{
		if (SelectionIndex < History.Count - 1)
		{
			SelectionIndex++;
			Selection.activeObject = EditorUtility.InstanceIDToObject(History[SelectionIndex]);

			UpdateValues();
		}
	}

	public static void ClearHistory()
	{
		History.Clear();

		UpdateValues();
	}

	private static void UpdateValues()
	{
		SessionState.SetIntArray(kHistory, History.ToArray());
		SessionState.SetInt(kSelectionIndex, SelectionIndex);
	}

}