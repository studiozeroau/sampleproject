﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEditor;

using Zeroes.Utils;
using Zeroes;

public class SelectionHistoryWindow : EditorWindow
{

	[MenuItem("Zeroes/Selection History")]
	static void init()
	{
		SelectionHistoryWindow window = (SelectionHistoryWindow)GetWindow(typeof(SelectionHistoryWindow), false, "History");
		window.Show();
	}

	void OnGUI()
	{
		float size = EditorGUIUtility.singleLineHeight * 1.35f;
		minSize = new Vector2(minSize.x, size);

		GUIStyle button = new GUIStyle(GUI.skin.button);
		button.fixedWidth = size;
		button.fixedHeight = size;

		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.Space();

		GUI.enabled = SelectionHistory.SelectionIndex > 0;
		if (GUILayout.Button(EditorGUIUtility.FindTexture("tab_prev"), button))
		{
			SelectionHistory.SelectPrevious();
		}
		GUI.enabled = true;

		GUI.enabled = SelectionHistory.SelectionIndex < SelectionHistory.History.Count - 1;
		if (GUILayout.Button(EditorGUIUtility.FindTexture("tab_next"), button))
		{
			SelectionHistory.SelectNext();
		}
		GUI.enabled = true;

		if (GUILayout.Button(EditorGUIUtility.FindTexture("d_TreeEditor.Trash"), button))
		{
			SelectionHistory.ClearHistory();
			Repaint();
		}

		EditorGUILayout.EndHorizontal();
	}

	private void OnSelectionChange()
	{
		Repaint();
	}
}