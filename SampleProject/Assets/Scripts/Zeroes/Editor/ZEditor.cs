﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.Reflection;

using Zeroes.UI;

// Stolen from https://gist.github.com/t0chas/34afd1e4c9bc28649311

[CustomEditor(typeof(UnityEngine.Object), true, isFallback = true)]
[CanEditMultipleObjects]
public class ZEditor : Editor
{
	private const BindingFlags flags = BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic;
		
	public override void OnInspectorGUI()
	{
		Color cachedGuiColor = GUI.color;
		serializedObject.Update();
		var property = serializedObject.GetIterator();
		var next = property.NextVisible(true);
		if (next)
		{
			do
			{
				GUI.color = cachedGuiColor;
				this.HandleProperty(property);
			} while (property.NextVisible(false));
		}
		
		var methods = target.GetType().GetMethods(flags);
		foreach (MethodInfo method in methods)
		{
			HandleMethod(method);
		}
		
		serializedObject.ApplyModifiedProperties();
	}
	
	protected void HandleProperty(SerializedProperty property)
	{
		bool isdefaultScriptProperty = property.name.Equals("m_Script") && property.type.Equals("PPtr<MonoScript>") && property.propertyType == SerializedPropertyType.ObjectReference && property.propertyPath.Equals("m_Script");
		bool cachedGUIEnabled = GUI.enabled;
		if (isdefaultScriptProperty)
		{
			GUI.enabled = false;
		}
		if (property.isArray && property.propertyType != SerializedPropertyType.String)
		{
			Editors.DrawArray(property);
		}
		else
		{
			EditorGUILayout.PropertyField(property, property.isExpanded);
		}
		if (isdefaultScriptProperty)
		{
			GUI.enabled = cachedGUIEnabled;
		}
	}

	private void HandleMethod(MethodInfo methodInfo)
	{
		var buttonAttribute = methodInfo.GetCustomAttribute<Zeroes.Editor.Button>();
		if (buttonAttribute == null)
			return;
		
		if (GUILayout.Button(buttonAttribute.Label == string.Empty ? methodInfo.Name : buttonAttribute.Label))
		{
			methodInfo.Invoke(target, null);
		}
	}
}