﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEditor.Build.Reporting;

using Zeroes;
using Zeroes.Scene.Editor;

// Editor Icons
// https://unitylist.com/p/5c3/Unity-editor-icons

namespace UnityToolbarExtender.Examples
{
	static class ZeroesToolbarStyles
	{
		public static GUIStyle Dropdown
		{
			get
			{
				GUIStyle style = new GUIStyle(EditorStyles.popup);
				style.fixedHeight = 18;
				style.fontSize = 11;
				return style;
			}
		}
	}

	[InitializeOnLoad]
	static class ZeroesToolbar
	{
		static ZeroesToolbar()
		{
			ToolbarExtender.RightToolbarGUI.Add(OnRightToolbarGUI);
			ToolbarExtender.LeftToolbarGUI.Add(OnLeftToolbarGUI);
		}

		static void OnLeftToolbarGUI()
		{
			EditorGUI.BeginDisabledGroup(Application.isPlaying);

			EditorGUILayout.BeginVertical();

			GUILayout.Space(1);

			EditorGUILayout.BeginHorizontal();

			GUILayout.Space(50);

			BuildButton();

			BuildSettingsButton();

			PlayerSettingsButton();

			EditorGUILayout.EndHorizontal();

			EditorGUILayout.EndVertical();

			EditorGUI.EndDisabledGroup();
		}

		static void OnRightToolbarGUI()
		{
			EditorGUI.BeginDisabledGroup(Application.isPlaying);

			EditorGUILayout.BeginVertical();

			GUILayout.Space(1);

			EditorGUILayout.BeginHorizontal();

			SymbolSelector();

			if (SwitchBuildPlatform())
			{
				return;
			}

			if (SceneSelector())
			{
				return;
			}

			ConfigurationSelector();

			EditorGUILayout.EndHorizontal();

			EditorGUILayout.EndVertical();

            EditorGUI.EndDisabledGroup();
		}

		#region BUILD SYMBOLS

		public static readonly string[] Display = new string[] { "None", "DEBUG", "TEST", "RELEASE" };
		public static readonly string[] Symbols = new string[] { "", "DEBUGGING", "TEST", "RELEASE" };

		public static void SymbolSelector()
		{
			int index = 0;
			string currentSetting = PlayerSettings.GetScriptingDefineSymbolsForGroup(BuildTargetGroup.iOS);

			for (int i = 1; i < Symbols.Length; i++)
			{
				if (currentSetting.Equals(Symbols[i]))
				{
					index = i;
					break;
				}
			}

			index = EditorGUILayout.Popup(index, Display, ZeroesToolbarStyles.Dropdown, GUILayout.MaxWidth(100));
			
			if (!Symbols[index].Equals(currentSetting))
			{
				// Set symbols for both iOS and Android platforms
				PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.Standalone, Symbols[index]);
				PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.iOS, Symbols[index]);
				PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.Android, Symbols[index]);
			}
		}

		#endregion


        // Performs a build right from the toolbar
		public static void BuildButton()
		{

			if (GUILayout.Button(new GUIContent("Build"), GUILayout.MaxWidth(100)))
			{
                string extension = "";

                if (EditorUserBuildSettings.activeBuildTarget == BuildTarget.Android)
                {
                    // Prompt the user for what kind of build they want for Android
                    switch(EditorUtility.DisplayDialogComplex("Android Build", "Would you like to build an App Bundle (.aab) or APK (.apk)?", "App Bundle", "Cancel", "APK"))
                    {
                        case 0:
                            // App Bundle
                            extension = ".aab";
                            EditorUserBuildSettings.buildAppBundle = true;
                            break;
                        case 1:
                            // Cancel
                            return;
                        case 2:
                            // APK
                            extension = ".apk";
                            EditorUserBuildSettings.buildAppBundle = false;
                            break;
                    }
                }
                else if (EditorUserBuildSettings.activeBuildTarget == BuildTarget.iOS)
                {
                    // Does this require extra work?
                }
				
                // Final prompt because this could take a while
                if (EditorUtility.DisplayDialog($"Build {PlayerSettings.productName}?", $"Are you sure you would like to build {PlayerSettings.productName}? This may take a while.", "Yes", "No"))
				{
					BuildReport report = BuildScripts.BuildProject(BuildScripts.GetBuildPlayerOptions(extension));
                    BuildSummary summary = report.summary;

                    if (summary.result == BuildResult.Succeeded)
                    {
                        Debug.Log("Build succeeded: " + summary.totalSize + " bytes");
                    }

                    if (summary.result == BuildResult.Failed)
                    {
                        Debug.Log("Build failed");
                    }
                }
			}
		}

        // Shortcut to open up build settings
		public static void BuildSettingsButton()
        {
            GUIStyle textureButton = new GUIStyle(GUI.skin.button)
            {
                padding = new RectOffset(1, 1, 1, 1),
            };
            Texture2D icon = EditorGUIUtility.IconContent("d_TerrainInspector.TerrainToolSettings").image as Texture2D;
			if (GUILayout.Button(new GUIContent(icon), textureButton, GUILayout.MaxWidth(35), GUILayout.MaxHeight(18)))
			{
				EditorApplication.ExecuteMenuItem("File/Build Settings...");
			}
		}

        // Shortcut to open Player Settings
		public static void PlayerSettingsButton()
        {
            GUIStyle textureButton = new GUIStyle(GUI.skin.button)
            {
                padding = new RectOffset(1, 1, 1, 1),
            };
            Texture2D icon = null;
			switch (EditorUserBuildSettings.activeBuildTarget)
			{
				case BuildTarget.Android:
					icon = EditorGUIUtility.IconContent("d_BuildSettings.Android.Small").image as Texture2D;
					break;
				case BuildTarget.iOS:
					icon = EditorGUIUtility.IconContent("d_BuildSettings.iPhone.Small").image as Texture2D;
					break;
                case BuildTarget.StandaloneWindows:
                case BuildTarget.StandaloneOSX:
					icon = EditorGUIUtility.IconContent("BuildSettings.Standalone").image as Texture2D;
					break;
			}

			if (icon != null)
			{
				if (GUILayout.Button(new GUIContent(icon), textureButton, GUILayout.MaxWidth(35), GUILayout.MaxHeight(18)))
				{
#if UNITY_2019_1_OR_NEWER
					SettingsService.OpenProjectSettings("Project/Player");
				
#else
					EditorApplication.ExecuteMenuItem("Edit/Project Settings/Player");
#endif
				}
			}
		}

		// This method apparently messes with the layout stuff, hence why it returns a bool and returns in the method its called from
		public static bool SwitchBuildPlatform()
		{
			string[] platforms =
			{
				BuildTarget.iOS.ToString(),
				BuildTarget.Android.ToString(),
				BuildTarget.StandaloneOSX.ToString(),
                BuildTarget.StandaloneWindows.ToString()
			};

			int currentPlatform = Array.IndexOf(platforms, EditorUserBuildSettings.activeBuildTarget.ToString());
			
			if (currentPlatform > -1)
			{
				int newPlatform = EditorGUILayout.Popup(currentPlatform, platforms, ZeroesToolbarStyles.Dropdown, GUILayout.MaxWidth(100));
				
				if (currentPlatform != newPlatform)
				{
					BuildTarget newBuildTarget = BuildTarget.NoTarget;
					BuildTargetGroup newBuildTargetGroup = BuildTargetGroup.Unknown;

					if (Enum.TryParse<BuildTarget>(platforms[newPlatform], out newBuildTarget))
					{
						newBuildTargetGroup = BuildPipeline.GetBuildTargetGroup(newBuildTarget);
						if (newBuildTargetGroup != BuildTargetGroup.Unknown)
						{

							// Give the user a quick warning about the potential this will take ages
							if (EditorUtility.DisplayDialog("Change Build Platform?", $"Are you sure you would like to change your build platform from {platforms[currentPlatform]} to {platforms[newPlatform]}?", "Yes", "No"))
							{
								// Do the actual platform switch
								EditorUserBuildSettings.SwitchActiveBuildTarget(newBuildTargetGroup, newBuildTarget);
							}
							return true;
						}
					}

				}
			}
			return false;
		}

		public static bool SceneSelector()
        {
			UnityEngine.SceneManagement.Scene activeScene = UnityEngine.SceneManagement.SceneManager.GetActiveScene();

			// Show a sweet icon next to this element
			Texture2D icon = EditorGUIUtility.IconContent("SceneAsset Icon").image as Texture2D;
            GUIStyle iconStyle = new GUIStyle(GUI.skin.label)
            {
                padding = new RectOffset(1, 1, 1, 1),
            };
            GUILayout.Label(icon, iconStyle, GUILayout.MaxHeight(20), GUILayout.MaxWidth(20));

            string[] scenes = SceneUtilities.GetAllScenesInProject();
			string[] scenePaths = SceneUtilities.GetAllScenesInProject(true);

			int currentIndex = Array.IndexOf(scenes, activeScene.name);
             
			// The actual dropdown
			int sceneIndex = EditorGUILayout.Popup(currentIndex, scenes, ZeroesToolbarStyles.Dropdown, GUILayout.MinWidth(50), GUILayout.MaxWidth(150));

			// If the scenes don't match, open the new one 
			if (sceneIndex != currentIndex)
			{
				string scenePath = scenePaths[sceneIndex];

				if (activeScene.isDirty)
				{
					int choice = EditorUtility.DisplayDialogComplex("Scene(s) Have Been Modified", $"Do you want to save the changes you made in the scenes: {activeScene.path}\n\nYour changes will be lost if you don't save them.", "Save", "Cancel", "Don't Save");

					switch (choice)
					{
						// Save
						case 0:
							EditorSceneManager.SaveOpenScenes();
							break;

						// Cancel
						case 1:
							return true;

						// Don't Save
						case 2:
							break;
					}

					EditorSceneManager.OpenScene(scenePath);

					return true;
				}
				else
				{
					EditorSceneManager.OpenScene(scenePath);
				}
			}

			return false;
		}
        
        // Selects the current ProjectConfiguration file
        public static void ConfigurationSelector()
        {
            GUIStyle textureButton = new GUIStyle(GUI.skin.button)
            {
                padding = new RectOffset(1, 1, 1, 1),
            };
            Texture2D icon = EditorGUIUtility.IconContent("console.infoicon.sml").image as Texture2D;
            if (GUILayout.Button(new GUIContent(icon), textureButton, GUILayout.MaxWidth(35), GUILayout.MaxHeight(18)))
            {
				ProjectConfiguration configuration = ProjectConfiguration.Find();
                if(configuration != null)
                {
                    Selection.activeObject = EditorUtility.InstanceIDToObject(configuration.GetInstanceID());
                }
                else
                {
                    EditorUtility.DisplayDialog("No project Configuration!", "Zeroes Project requires a Project Configuration. Please make one, or extend ProjectConfiguration for the needs of the project.", "Ok", String.Empty);
                }
            }
        }

	}
}