﻿namespace Zeroes.Tween
{
    public partial class Tween
    {
        public struct Options
        {
            public float delay;
            public int repeat;
            public bool yoyo;
            public bool reverse;
            public EaseMethod ease;
            public TimeType timeType;
            public string tag;

            public Options(float delay = 0f, int repeat = 0, bool yoyo = false, bool reverse = false, EaseMethod ease = null, TimeType timeType = TimeType.Scaled, string tag = null)
            {
                this.delay = delay;
                this.repeat = repeat;
                this.yoyo = yoyo;
                this.reverse = reverse;
                this.ease = ease;
                this.timeType = timeType;
                this.tag = tag;
            }
        }
    }
}
