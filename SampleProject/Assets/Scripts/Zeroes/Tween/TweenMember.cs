﻿using System.Reflection;
using UnityEngine;

namespace Zeroes.Tween
{
    public partial class Tween
    {
        abstract class Member
        {
            public abstract void Reset(object target);
            public abstract void Update(object target, float t);
        }

        #region FIELDS

        class Field : Member
        {
            public FieldInfo info;

            public override void Reset(object target) { }
            public override void Update(object target, float t) { }
        }

        sealed class Vector2Field : Field
        {
            private Vector2 start;
            private Vector2 end;
            private Vector2 delta;

            public Vector2Field(FieldInfo info, Vector2 end)
            {
                this.info = info;
                this.end = end;
            }

            public Vector2Field(FieldInfo info, Vector2 end, object target)
            {
                this.info = info;
                this.end = end;
                Reset(target);
            }

            public override void Reset(object target)
            {
                start = (Vector2)info.GetValue(target);
                delta = end - start;
            }

            public override void Update(object target, float t)
            {
                info.SetValue(target, start + delta * t);
            }
        }

        sealed class Vector3Field : Field
        {
            private Vector3 start;
            private Vector3 end;
            private Vector3 delta;

            public Vector3Field(FieldInfo info, Vector3 end)
            {
                this.info = info;
                this.end = end;
            }

            public Vector3Field(FieldInfo info, Vector3 end, object target)
            {
                this.info = info;
                this.end = end;
                Reset(target);
            }

            public override void Reset(object target)
            {
                start = (Vector3)info.GetValue(target);
                delta = end - start;
            }

            public override void Update(object target, float t)
            {
                info.SetValue(target, start + delta * t);
            }
        }

        sealed class FloatField : Field
        {
            private float start;
            private float end;
            private float delta;

            public FloatField(FieldInfo info, float end)
            {
                this.info = info;
                this.end = end;
            }

            public FloatField(FieldInfo info, float end, object target)
            {
                this.info = info;
                this.end = end;
                Reset(target);
            }

            public override void Reset(object target)
            {
                start = (float)info.GetValue(target);
                delta = end - start;
            }

            public override void Update(object target, float t)
            {
                info.SetValue(target, start + delta * t);
            }
        }

        sealed class IntField : Field
        {
            private int start;
            private int end;
            private int delta;

            public IntField(FieldInfo info, int end)
            {
                this.info = info;
                this.end = end;
            }

            public IntField(FieldInfo info, int end, object target)
            {
                this.info = info;
                this.end = end;
                Reset(target);
            }

            public override void Reset(object target)
            {
                start = (int)info.GetValue(target);
                delta = end - start;
            }

            public override void Update(object target, float t)
            {
                info.SetValue(target, (int)(start + delta * t));
            }
        }

        sealed class ColorField : Field
        {
            private Color start;
            private Color end;
            private Color delta;

            public ColorField(FieldInfo info, Color end)
            {
                this.info = info;
                this.end = end;
            }

            public ColorField(FieldInfo info, Color end, object target)
            {
                this.info = info;
                this.end = end;
                Reset(target);
            }

            public override void Reset(object target)
            {
                start = (Color)info.GetValue(target);
                delta = end - start;
            }

            public override void Update(object target, float t)
            {
                info.SetValue(target, start + delta * t);
            }
        }

        sealed class QuaternionField : Field
        {
            private Quaternion start;
            private Quaternion end;

            public QuaternionField(FieldInfo info, Quaternion end)
            {
                this.info = info;
                this.end = end;
            }

            public QuaternionField(FieldInfo info, Quaternion end, object target)
            {
                this.info = info;
                this.end = end;
                Reset(target);
            }

            public override void Reset(object target)
            {
                start = (Quaternion)info.GetValue(target);
            }

            public override void Update(object target, float t)
            {
                info.SetValue(target, Quaternion.Lerp(start, end, t));
            }
        }


        #endregion



        #region PROPERTIES

        class Property : Member
        {
            public PropertyInfo info;

            public override void Reset(object target) { }
            public override void Update(object target, float t) { }
        }

        sealed class Vector2Property : Property
        {
            private Vector2 start;
            private Vector2 end;
            private Vector2 delta;

            public Vector2Property(PropertyInfo info, Vector2 end)
            {
                this.info = info;
                this.end = end;
            }

            public Vector2Property(PropertyInfo info, Vector2 end, object target)
            {
                this.info = info;
                this.end = end;
                Reset(target);
            }

            public override void Reset(object target)
            {
                start = (Vector2)info.GetValue(target);
                delta = end - start;
            }

            public override void Update(object target, float t)
            {
                info.SetValue(target, start + delta * t);
            }
        }

        sealed class Vector3Property : Property
        {
            private Vector3 start;
            private Vector3 end;
            private Vector3 delta;

            public Vector3Property(PropertyInfo info, Vector3 end)
            {
                this.info = info;
                this.end = end;
            }

            public Vector3Property(PropertyInfo info, Vector3 end, object target)
            {
                this.info = info;
                this.end = end;
                Reset(target);
            }

            public override void Reset(object target)
            {
                start = (Vector3)info.GetValue(target);
                delta = end - start;
            }

            public override void Update(object target, float t)
            {
                info.SetValue(target, start + delta * t);
            }
        }

        sealed class FloatProperty : Property
        {
            private float start;
            private float end;
            private float delta;

            public FloatProperty(PropertyInfo info, float end)
            {
                this.info = info;
                this.end = end;
            }

            public FloatProperty(PropertyInfo info, float end, object target)
            {
                this.info = info;
                this.end = end;
                Reset(target);
            }

            public override void Reset(object target)
            {
                start = (float)info.GetValue(target);
                delta = end - start;
            }

            public override void Update(object target, float t)
            {
                info.SetValue(target, start + delta * t);
            }
        }

        sealed class IntProperty : Property
        {
            private int start;
            private int end;
            private int delta;

            public IntProperty(PropertyInfo info, int end)
            {
                this.info = info;
                this.end = end;
            }

            public IntProperty(PropertyInfo info, int end, object target)
            {
                this.info = info;
                this.end = end;
                Reset(target);
            }

            public override void Reset(object target)
            {
                start = (int)info.GetValue(target);
                delta = end - start;
            }

            public override void Update(object target, float t)
            {
                info.SetValue(target, (int)(start + delta * t));
            }
        }

        sealed class ColorProperty : Property
        {
            private Color start;
            private Color end;
            private Color delta;

            public ColorProperty(PropertyInfo info, Color end)
            {
                this.info = info;
                this.end = end;
            }

            public ColorProperty(PropertyInfo info, Color end, object target)
            {
                this.info = info;
                this.end = end;
                Reset(target);
            }

            public override void Reset(object target)
            {
                start = (Color)info.GetValue(target);
                delta = end - start;
            }

            public override void Update(object target, float t)
            {
                info.SetValue(target, start + delta * t);
            }
        }

        sealed class QuaternionProperty : Property
        {
            private Quaternion start;
            private Quaternion end;
            
            public QuaternionProperty(PropertyInfo info, Quaternion end)
            {
                this.info = info;
                this.end = end;
            }

            public QuaternionProperty(PropertyInfo info, Quaternion end, object target)
            {
                this.info = info;
                this.end = end;
                Reset(target);
            }

            public override void Reset(object target)
            {
                start = (Quaternion)info.GetValue(target);
            }

            public override void Update(object target, float t)
            {
                info.SetValue(target, Quaternion.Lerp(start, end, t));
            }
        }

        #endregion

    }
}
