﻿using System;

using UnityEngine;

namespace Zeroes.Tween
{
	public static class Extensions
	{
		public static YieldInstruction YieldTween(this Tween tween)
		{
			return new WaitForSeconds(tween.duration);
		}
	}
}
