﻿using System.Reflection;
using System.Collections.Generic;
using System;

namespace Zeroes.Tween
{
    public delegate void TweenEventHandler(Tween tween);

    public partial class Tween
    {
        public event TweenEventHandler OnUpdate;
        public event TweenEventHandler OnRepeat;
        public event TweenEventHandler OnComplete;

        public object target { get; private set; }
        public float time { get; set; }
        public float duration { get; set; }
        public float delay { get; set; }
        public int repeat { get; set; }
        public bool yoyo { get; set; }
        public bool reverse { get; set; }
        public EaseMethod ease { get; set; }
        public TimeType timeType { get; set; }
        public string tag { get; set; }
        public bool paused { get; set; }
        public bool ended { get; private set; }

        List<Member> members;

        public Tween(object target, float duration, object members, Options options)
        {
            this.target = target;

            this.time = options.reverse ? duration : 0f;
            this.duration = duration;

            this.delay = options.delay;
            this.repeat = (options.repeat < 0) ? -1 : options.repeat;// Ensure initial repeat value for infinite repeat is -1 so we can use it in the Cancel method to calculate the complete value.
            this.yoyo = options.yoyo;
            this.reverse = options.reverse;
            this.ease = (options.ease != null) ? options.ease : Ease.Linear;
            this.timeType = options.timeType;
            this.tag = options.tag;
            this.paused = false;
            this.ended = false;

            this.members = new List<Member>();

            Type targetType = target.GetType();
            PropertyInfo[] memberProperties = members.GetType().GetProperties();

            foreach (PropertyInfo memberInfo in memberProperties)
            {
                var typeInfo = memberInfo.GetValue(members).GetType();

                PropertyInfo propertyInfo = targetType.GetProperty(memberInfo.Name);
                if (propertyInfo != null)
                {
                    if (typeInfo == typeof(UnityEngine.Vector2))
                    {
                        this.members.Add(new Vector2Property(propertyInfo, (UnityEngine.Vector2)memberInfo.GetValue(members), target));
                    }
                    else if (typeInfo == typeof(UnityEngine.Vector3))
                    {
                        this.members.Add(new Vector3Property(propertyInfo, (UnityEngine.Vector3)memberInfo.GetValue(members), target));
                    }
                    else if (typeInfo == typeof(float))
                    {
                        this.members.Add(new FloatProperty(propertyInfo, (float)memberInfo.GetValue(members), target));
                    }
                    else if (typeInfo == typeof(int))
                    {
                        this.members.Add(new IntProperty(propertyInfo, (int)memberInfo.GetValue(members), target));
                    }
                    else if (typeInfo == typeof(UnityEngine.Color))
                    {
                        this.members.Add(new ColorProperty(propertyInfo, (UnityEngine.Color)memberInfo.GetValue(members), target));
                    }
                    else if(typeInfo == typeof(UnityEngine.Quaternion))
                    {
                        this.members.Add(new QuaternionProperty(propertyInfo, (UnityEngine.Quaternion)memberInfo.GetValue(members), target));
                    }
                    else
                    {
                        throw new Exception($"Unsupported Type '{typeInfo}' for Property Tween");
                    }

                    continue;
                }

                FieldInfo fieldInfo = targetType.GetField(memberInfo.Name);
                if (fieldInfo != null)
                {
                    if (typeInfo == typeof(UnityEngine.Vector2))
                    {
                        this.members.Add(new Vector2Field(fieldInfo, (UnityEngine.Vector2)memberInfo.GetValue(members), target));
                    }
                    else if (typeInfo == typeof(UnityEngine.Vector3))
                    {
                        this.members.Add(new Vector3Field(fieldInfo, (UnityEngine.Vector3)memberInfo.GetValue(members), target));
                    }
                    else if (typeInfo == typeof(float))
                    {
                        this.members.Add(new FloatField(fieldInfo, (float)memberInfo.GetValue(members), target));
                    }
                    else if (typeInfo == typeof(int))
                    {
                        this.members.Add(new IntField(fieldInfo, (int)memberInfo.GetValue(members), target));
                    }
                    else if (typeInfo == typeof(UnityEngine.Color))
                    {
                        this.members.Add(new ColorField(fieldInfo, (UnityEngine.Color)memberInfo.GetValue(members), target));
                    }
                    else if(typeInfo == typeof(UnityEngine.Quaternion))
                    {
                        this.members.Add(new QuaternionField(fieldInfo, (UnityEngine.Quaternion)memberInfo.GetValue(members), target));
                    }
                    else
                    {
                        throw new Exception($"Unsupported Type '{typeInfo}' for Field Tween");
                    }

                    continue;
                }

                throw new Exception($"Member '{memberInfo.Name}' not found on object of type '{targetType}'!");
            }
        }

        /// <summary>
        /// Update this tween by the specified deltaTime.
        /// </summary>
        
        public void Update(float deltaTime)
        {
            if (paused || ended)
            {
                return;
            }

            if (delay > 0f)
            {
                delay -= deltaTime;

                if (delay > 0f)
                {
                    return;
                }

                foreach (Member member in members)
                {
                    member.Reset(target);
                }

                if (delay < 0f)
                {
                    deltaTime = -delay;
                }
                else
                {
                    return;
                }
            }

            bool complete = false;
            float repeatTime = 0f;

            if (reverse)
            {
                time -= deltaTime;

                if (time <= 0f)
                {
                    complete = true;
                    repeatTime = -time;
                    time = 0f;
                }
            }
            else
            {
                time += deltaTime;

                if (time >= duration)
                {
                    complete = true;
                    repeatTime = time - duration;
                    time = duration;
                }
            }

            UpdateMembers(ease(time, 0f, 1f, duration));

            OnUpdate?.Invoke(this);

            if (complete)
            {
                if (repeat != 0 || yoyo)
                {
                    OnRepeat?.Invoke(this);

                    repeat--;

                    if (yoyo)
                    {
                        reverse = !reverse;
                    }

                    if (reverse)
                    {
                        time = duration - repeatTime;
                    }
                    else
                    {
                        time = repeatTime;
                    }

                    UpdateMembers(ease(time, 0f, 1f, duration));

                    //TODO. Decide if OnComplete triggers from here too
                }
                else
                {
                    ended = true;

                    OnComplete?.Invoke(this);
                }
            }
        }

        private void UpdateMembers(float t)
        {
            foreach (Member member in members)
            {
                member.Update(target, t);
            }
        }


        /// <summary>
        /// Cancel this tween and optionally set each member to its end value.
        /// </summary>
        
        public void Cancel(bool complete)
        {
			ended = true;

			if (complete)
			{
				if (yoyo && repeat % 2 != 0)
				{
					UpdateMembers(reverse ? 1f : 0f);
				}
				else
				{
					UpdateMembers(reverse ? 0f : 1f);
				}
			}
        }

    }
}
