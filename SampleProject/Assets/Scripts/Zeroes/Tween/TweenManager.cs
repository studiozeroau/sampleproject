﻿using UnityEngine;
using System.Collections.Generic;

namespace Zeroes.Tween
{
    public class TweenManager : MonoBehaviour
    {

        const int ScaledTimeIndex = 0;
        const int UnscaledTimeIndex = 1;
        const int FixedScaledTimeIndex = 2;
        const int FixedUnscaledTimeIndex = 3;

        List<Tween>[] tweens;

        public bool paused;

        void Awake()
        {
            tweens = new List<Tween>[4];

            tweens[ScaledTimeIndex] = new List<Tween>();
            tweens[UnscaledTimeIndex] = new List<Tween>();
            tweens[FixedScaledTimeIndex] = new List<Tween>();
            tweens[FixedUnscaledTimeIndex] = new List<Tween>();
        }

        void Update()
        {
            if (!paused)
            {
                UpdateTween(ScaledTimeIndex, Time.deltaTime);
                UpdateTween(UnscaledTimeIndex, Time.unscaledDeltaTime);
            }
        }

        void FixedUpdate()
        {
            if (!paused)
            {
                UpdateTween(FixedScaledTimeIndex, Time.fixedDeltaTime);
                UpdateTween(FixedUnscaledTimeIndex, Time.fixedUnscaledDeltaTime);
            }
        }

        private void UpdateTween(int timeIndex, float delta)
        {
            for (int i = 0; i < tweens[timeIndex].Count;)
            {
                Tween tween = tweens[timeIndex][i];

                tween.Update(delta);

                if (tween.ended)
                {
                    tweens[timeIndex].RemoveAt(i);
                }
                else
                {
                    i++;
                }
            }
        }

        /// <summary>
        /// Create a tween of target with the specifiedduration, and members.
        /// </summary>

        public Tween TweenTo(object target, float duration, object members)
        {
            Debug.Assert(target != null);
            Debug.Assert(members != null);

            return AddTween(new Tween(target, duration, members, new Tween.Options()));
        }

        /// <summary>
        /// Create a tween of target with the specifiedduration, members, and options.
        /// </summary>
        
        public Tween TweenTo(object target, float duration, object members, Tween.Options options)
        {
            Debug.Assert(target != null);
            Debug.Assert(members != null);

            return AddTween(new Tween(target, duration, members, options));
        }

        /// <summary>
        /// Adds a tween to the update loop of this TweenManager
        /// </summary>
        
        public Tween AddTween(Tween tween)
        {
            switch (tween.timeType)
            {
                case TimeType.Scaled:
                    tweens[ScaledTimeIndex].Add(tween);
                    break;
                case TimeType.Unscaled:
                    tweens[UnscaledTimeIndex].Add(tween);
                    break;
                case TimeType.FixedScaled:
                    tweens[FixedScaledTimeIndex].Add(tween);
                    break;
                case TimeType.FixedUnscaled:
                    tweens[FixedUnscaledTimeIndex].Add(tween);
                    break;
            }

            return tween;
        }

		/// <summary>
		/// Addss multiple tweens to the update loop of this TweenManager
		/// </summary>

		public Tween[] AddTweens(Tween[] tweens)
		{
			foreach (Tween tween in tweens)
			{
				AddTween(tween);
			}
			return tweens;
		}

        /// <summary>
        /// Removes a tween from the Update loop of this TweenManager.
        /// </summary>
        
        public bool RemoveTween(Tween tween)
        {
            foreach (List<Tween> list in tweens)
            {
                int index = list.IndexOf(tween);

                if (index > -1)
                {
                    list.RemoveAt(index);

                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Pauses all tweens of the specified target.
        /// </summary>
        
        public void Pause(object target)
        {
            foreach (List<Tween> list in tweens)
            {
                foreach (Tween tween in list)
                {
                    if (tween.target == target)
                    {
                        tween.paused = true;
                    }
                }
            }
        }

        /// <summary>
        /// Pauses all tweens of the specified tag.
        /// </summary>

        public void Pause(string tag)
        {
            foreach (List<Tween> list in tweens)
            {
                foreach (Tween tween in list)
                {
                    if (tween.tag == tag)
                    {
                        tween.paused = true;
                    }
                }
            }
        }

        /// <summary>
        /// Pausess all tweens.
        /// Pauses all currently running tweens but any tweens added after this will run as normal.
        /// </summary>

        public void PauseAll()
        {
            foreach (List<Tween> list in tweens)
            {
                foreach (Tween tween in list)
                {
                    tween.paused = true;
                }
            }
        }

        /// <summary>
        /// Resumes all tweens of the specified target.
        /// </summary>
        
        public void Resume(object target)
        {
            foreach (List<Tween> list in tweens)
            {
                foreach (Tween tween in list)
                {
                    if (tween.target == target)
                    {
                        tween.paused = false;
                    }
                }
            }
        }

        /// <summary>
        /// Resumes all tweens of the specified tag.
        /// </summary>

        public void Resume(string tag)
        {
            foreach (List<Tween> list in tweens)
            {
                foreach (Tween tween in list)
                {
                    if (tween.tag == tag)
                    {
                        tween.paused = false;
                    }
                }
            }
        }

        /// <summary>
        /// Resumes all tweens.
        /// </summary>

        public void ResumeAll()
        {
            foreach (List<Tween> list in tweens)
            {
                foreach (Tween tween in list)
                {
                    tween.paused = false;
                }
            }
        }

        /// <summary>
        /// Cancels all tweens with the specified target and optionally set their members to their end values.
        /// </summary>
        
        public void Cancel(object target, bool complete = false)
        {
            foreach (List<Tween> list in tweens)
            {
                foreach (Tween tween in list)
                {
                    if (tween.target == target)
                    {
                        tween.Cancel(complete);
                    }
                }
            }
        }

        /// <summary>
        /// Cancels all tweens with the specified tag and optionally sets their members to their end values.
        /// </summary>
        
        public void Cancel(string tag, bool complete = false)
        {
            foreach (List<Tween> list in tweens)
            {
                foreach (Tween tween in list)
                {
                    if (tween.tag == tag)
                    {
                        tween.Cancel(complete);
                    }
                }
            }
        }

        /// <summary>
        /// Cancels all tweens and optionally set their members to their end values.
        /// </summary>
        
        public void CancelAll(bool complete = false)
        {
            foreach (List<Tween> list in tweens)
            {
                foreach (Tween tween in list)
                {
                    tween.Cancel(complete);
                }
            }
        }

		/// <summary>
		/// Return bool for whether TweenManager contains a tween for this object.
		/// </summary>
			
		public bool HasTween(object target)
		{
			foreach (List<Tween> list in tweens)
			{
				foreach (Tween tween in list)
				{
					if (tween.target == target)
					{
						return true;
					}
				}
			}
			return false;
		}
    }
}
