﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using Zeroes;
using Zeroes.Managers;

public class SampleGameManager : GameManager
{
    public override void InitialiseData()
    {
        base.InitialiseData();
        
        // Load up device settings
        string deviceDataPath = DataWriter.CreatePath("DeviceData.dat", Application.persistentDataPath);
        base.DeviceData = DataWriter.Read(deviceDataPath, new DeviceData());
        DeviceData.SetFilepath(deviceDataPath);
    }

    public override void Initialise()
    {
        base.Initialise();
        
        // If the user hasn't started the app, check their system language and set it to that
        // If not, use whatever the user has chosen as their language from the in-game settings
        // TODO. Move to base GameManager
        string lang = DeviceData.Language;
        if (string.IsNullOrEmpty(lang))
        {
            SystemLanguage systemLang = Application.systemLanguage;
            if (LocalisationManager.Configuration.IsLanguageSupported(systemLang))
            {
                lang = LocalisationManager.Configuration.CodeForSystemLanguage(systemLang);
            }
        }
        LocalisationManager.ChangeLanguage(lang);

        LocalisationManager.LoadContent("Sample");
    }
}
