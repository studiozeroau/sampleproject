﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using Zeroes;
using Zeroes.Managers;
using Zeroes.Tween;
using Zeroes.UI;
using Zeroes.Touch;


[Serializable]
public class ColourSet
{
    [SerializeField]
    private Color _BackgroundColour;
    public Color BackgroundColour => _BackgroundColour;

    [SerializeField]
    private Color _WaveColour;
    public Color WaveColour => _WaveColour;
}

public class SampleMenu : MonoBehaviour
{

    [Header("Content")]
    [SerializeField]
    private RectTransform ContentContainer;

    [SerializeField]
    private RectTransform Center;

    [SerializeField]
    private RectTransform Left;

    [SerializeField]
    private RectTransform Right;

    [SerializeField]
    private RectTransform Up;

    [SerializeField]
    private RectTransform Down;

    [SerializeField]
    private SampleModal Modal;

    [Header("Dynamic Visuals")]
    [SerializeField]
    private ZImage WaveTop;

    [SerializeField]
    private ZImage WaveBottom;

    [SerializeField]
    private ZImage Background;

    [SerializeField]
    private ColourSet[] ColourSets;

    [Header("Buttons")]
    [SerializeField]
    private Button UpButton;

    [SerializeField]
    private Button DownButton;
    
    [SerializeField]
    private Button LeftButton;

    [SerializeField]
    private Button RightButton;

    [SerializeField]
    private Button InfoButton;

    [SerializeField]
    private Button[] ReturnButtons;

    private TweenManager TweenManager;

    private void Awake()
    {
        TweenManager = gameObject.AddComponent<TweenManager>();
    }

    // Start is called before the first frame update
    void Start()
    {

        UpButton.OnRelease += () =>
        {
            GoToScreen(Up, ColourSets[1].WaveColour, ColourSets[1].BackgroundColour);
        };

        DownButton.OnRelease += () =>
        {
            GoToScreen(Down, ColourSets[2].WaveColour, ColourSets[2].BackgroundColour);
        };

        LeftButton.OnRelease += () =>
        {
            GoToScreen(Left, ColourSets[3].WaveColour, ColourSets[3].BackgroundColour);
        };

        RightButton.OnRelease += () =>
        {
            GoToScreen(Right, ColourSets[4].WaveColour, ColourSets[4].BackgroundColour);
        };

        InfoButton.OnRelease += () =>
        {
            Modal.Show();
        };

        foreach(Button button in ReturnButtons)
        {
            button.OnRelease += () =>
            {
                GoToScreen(Center, ColourSets[0].WaveColour, ColourSets[0].BackgroundColour);
            };
        }
    }

    private void GoToScreen(RectTransform rectTransform, Color waveColour, Color backgroundColour, float duration = .25f)
    {
        // Disable all touch for the duration of the tween
        GameManager.Instance.TouchManager.Enabled = false;

        // Cancel any potentially current Tweens
        TweenManager.Cancel(ContentContainer);
        TweenManager.Cancel(Background);
        TweenManager.Cancel(WaveTop);
        TweenManager.Cancel(WaveBottom);

        // Get proper new position
        Vector2 position = rectTransform.anchoredPosition;
        position.y = -position.y;
        position.x = -position.x;

        Tween.Options options = new Tween.Options(ease: Ease.SineEaseOut);

        // Tween to the next screen
        Tween screenTween = new Tween(ContentContainer, duration, new { anchoredPosition = position }, options);
        screenTween.OnComplete += (Tween tween) =>
        {
            GameManager.Instance.TouchManager.Enabled = true;
        };
        TweenManager.AddTween(screenTween);

        // Do all colour Tweens
        Tween bgTween = new Tween(Background, duration, new { color = backgroundColour }, options);
        Tween topWaveTween = new Tween(WaveTop, duration, new { color = waveColour }, options);
        Tween bottomWaveTween = new Tween(WaveBottom, duration, new { color = waveColour }, options);
        TweenManager.AddTween(bgTween);
        TweenManager.AddTween(topWaveTween);
        TweenManager.AddTween(bottomWaveTween);
    }

}
