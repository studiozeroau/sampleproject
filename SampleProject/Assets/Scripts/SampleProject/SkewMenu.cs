using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zeroes.Tween;

public class SkewMenu : MonoBehaviour
{

    [SerializeField] private TweenManager _tweenManager;
    [SerializeField] private UISkew _uiSkew;
    
    void Start()
    {
        float initialHorizontalSkew = _uiSkew.HorizontalAngle;
        float initialVerticalSkew = _uiSkew.VerticalAngle;
        
        Tween horizontalSkewTween = new Tween(_uiSkew, .75f, new {HorizontalAngle = -initialHorizontalSkew}, new Tween.Options(yoyo: true, ease: Ease.SineEaseIn));
        Tween verticalSkewTween = new Tween(_uiSkew, .3f, new {VerticalAngle = -initialVerticalSkew}, new Tween.Options(yoyo: true, ease: Ease.SineEaseIn));
        
        _tweenManager.AddTween(horizontalSkewTween);
        _tweenManager.AddTween(verticalSkewTween);
    }
}
