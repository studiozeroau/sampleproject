﻿using System;
using System.Collections;
using System.Collections.Generic;


using Zeroes.UI;
using Zeroes.Touch;
using Zeroes;

using UnityUI = UnityEngine.UI;
using UnityEngine;

public class RenderTextureMenu : MonoBehaviour
{
    [SerializeField]
    private RenderToTexture RenderToTexture;

    [SerializeField]
    private UnityUI.RawImage Target;

    [SerializeField]
    private UITouchArea TouchArea;

    [SerializeField]
    private Transform Mesh;

    private float Velocity = 0;

    public void Start()
    {
        RenderToTexture.BeginRender(Target);

        // Subscribe to touch events
        TouchArea.OnTouchDown += OnTouchDown;
        TouchArea.OnTouchMove += OnTouchMove;
        TouchArea.OnTouchUp += OnTouchUp;
        TouchArea.OnTouchCancel += OnTouchUp;
    }

    public void Update()
    {
        Mesh.Rotate(new Vector3(0, Velocity, 0));
        if(Mathf.Abs(Velocity) > .01f)
        {
            Velocity -= Mathf.Clamp(Velocity, -1, 1) * 5 * Time.deltaTime;
        }
        else
        {
            Velocity = 0;
        }
    }

    #region TOUCH

    private void OnTouchDown(TouchArea touchArea)
    {
        Velocity = 0;
    }

    private void OnTouchMove(TouchArea touchArea)
    {
        Mesh.Rotate(new Vector3(0, -touchArea.MoveDistance.x * .15f, 0));
    }

    private void OnTouchUp(TouchArea touchArea)
    {
        Velocity = -touchArea.MoveDistance.x * .05f;
    }

    #endregion

}
