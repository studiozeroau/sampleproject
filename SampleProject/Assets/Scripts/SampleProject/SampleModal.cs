﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using Zeroes.Managers;
using Zeroes.UI;
using Zeroes.Tween;

public class SampleModal : MonoBehaviour
{

    [SerializeField]
    private ZImage Background;

    [SerializeField]
    private RectTransform ModalWindow;

    [SerializeField]
    private Button CloseButton;

    private TweenManager TweenManager;

    public void Awake()
    {
        TweenManager = gameObject.AddComponent<TweenManager>();
    }

    public void Start()
    {
        Hide(false);

        CloseButton.OnRelease += () =>
        {
            Hide();
        };
    }

    // Show this modal window
    public void Show(bool animated = true, float duration = .25f)
    {
        // Isolate the Modal Layer
        GameManager.Instance.TouchManager.IsolateLayer("Modal");

        Color color = new Color(0, 0, 0, .6f);
        Vector2 position = new Vector2(0, -65);

        if (animated)
        {
            AnimateModal(color, position, duration);
            return;
        }

        // Do this instantly here
        Background.color = color;
        ModalWindow.anchoredPosition = position;
        GameManager.Instance.TouchManager.EnableAllLayers();

    }

    // Hide this modal window
    public void Hide(bool animated = true, float duration = .25f)
    {
        Color color = new Color(0, 0, 0, 0);
        Vector2 position = new Vector2(0, -ModalWindow.rect.height);

        if (animated)
        {
            AnimateModal(color, position, duration, () =>
            {
                GameManager.Instance.TouchManager.EnableAllLayers();
            });
            return;
        }

        // Do this instantly here
        Background.color = color;
        ModalWindow.anchoredPosition = position;
        GameManager.Instance.TouchManager.EnableAllLayers();
    }

    private void AnimateModal(Color backgroundColor, Vector2 position, float duration, Action andThen = null)
    {
        Tween.Options tweenOptions = new Tween.Options(ease: Ease.SineEaseOut);

        Tween fadeTween = new Tween(Background, duration, new { color = backgroundColor }, tweenOptions);
        Tween hideTween = new Tween(ModalWindow, duration, new { anchoredPosition = position }, tweenOptions);

        hideTween.OnComplete += (Tween tween) =>
        {
            andThen?.Invoke();
        };

        TweenManager.AddTween(fadeTween);
        TweenManager.AddTween(hideTween);
    }

}
